//
//  ForgetPasswordVC.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 3/27/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class ForgetPasswordVC: UIViewController {

    //MARK:
    //MARK: ---------------IBOutlet----------------
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var viewForChangePass: UIView!
    @IBOutlet weak var txtConPass: SkyFloatingLabelTextField!
    @IBOutlet weak var txtOldPass: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNewPass: SkyFloatingLabelTextField!
    var strComeFromScreen = String()
    //MARK:
    //MARK: ---------------Life Cycle-----------------
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewFromLocalisation()
        
        if strComeFromScreen == "ForgetPass" {
            viewForChangePass.isHidden = true
        }else{
            viewForChangePass.isHidden = false
        }
    
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillLayoutSubviews() {
        makeButtonCorner(radius: 20.0, color: UIColor.black, btn: btnLogin, borderWidth: 0.0)
        makeButtonCorner(radius: 20.0, color: UIColor.black, btn: btnSubmit, borderWidth: 0.0)

    }
    
    //MARK:
    //MARK: ---------------All IBAction-----------------
    
    @IBAction func actionOnSubmitChangePass(_ sender: Any) {
    
         if ((txtOldPass.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("ChangePass_OldPassAlert"), viewcontrol: self)
            
        }else if ((txtNewPass.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("ChangePass_NewPassAlert"), viewcontrol: self)
            
         }else if ((txtConPass.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_CPassword"), viewcontrol: self)
            
         }else if (txtNewPass.text! != txtConPass.text!){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_CPasswordNotRight"), viewcontrol: self)
         }else{
            if isInternetAvailable() {    //Check Network Condition
                call_ChangePasswordAPI()
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)
            }
            
            
        }
        
    }
    
    
    @IBAction func actionOnSubmit(_ sender: Any) {
        if ((txtEmail.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_EmailAddress"), viewcontrol: self)
            
        }else if !(validateEmail(email: txtEmail.text!)){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_ValidEmailAddress"), viewcontrol: self)
            
        }else{
            if isInternetAvailable() {    //Check Network Condition
               call_ForgetPasswordAPI()
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)
            }
        }
    }
    @IBAction func actionOnBack(_ sender: Any) {
        if strComeFromScreen == "ForgetPass" {
            self.navigationController?.popViewController(animated: true)
        }else{
            let mainVcIntial = kConstantObj.SetMainViewController(aStoryBoardID: "DashBoardVC")
            appDelegate.window?.rootViewController = mainVcIntial
        }
    }
    
    
    //MARK:
    //MARK: ---------------Extra Function's-----------------
    func configureViewFromLocalisation() {
   
        txtEmail.placeholder = Localization("Register_EmailAddress")
        btnLogin.setTitle(Localization("DashBoard_Submit"), for: .normal)
        txtOldPass.placeholder = Localization("ChangePass_OldPass")
        txtNewPass.placeholder = Localization("ChangePass_NewPass")
        txtConPass.placeholder = Localization("ChangePass_ConNewPass")
        btnSubmit.setTitle(Localization("DashBoard_Submit"), for: .normal)

    }
    
    //MARK:
    //MARK: ---------------API's Calling----------------
    func call_ForgetPasswordAPI() {
        let dict = NSMutableDictionary.init()
        
        if nsud.value(forKey: "AppLanguage") != nil{
            if nsud.value(forKey: "AppLanguage")as! String == "English"{
                dict.setValue("1", forKey: "language_preference")
            }else{
                dict.setValue("2", forKey: "language_preference")
            }
        }else{
            dict.setValue("1", forKey: "language_preference")
        }
        
        dict.setValue(txtEmail.text!, forKey: "email")//1
        print(dict)
        showLoader(strMsg: "", style: .dark)
        WebService.callAPIBYPOST(parameter: dict, url: API_ForgetPassword) { (responce, status) in
            DispatchQueue.main.async {
                dismissLoader()
            }
            if(status == "Suceess"){
                let dictResponce = responce.value(forKey: "data")as! NSDictionary
                print(dictResponce)
                if("\(String(describing: dictResponce.value(forKey: "error")!))" == "1"){
                    let alert = UIAlertController(title: Localization("UTHC_Alert"), message: (dictResponce.value(forKey: "data")as! String), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title:  Localization("UTHC_Ok"), style: UIAlertAction.Style.default, handler: { action in
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else if("\(String(describing: dictResponce.value(forKey: "error")!))" == "0"){
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: dictResponce.value(forKey: "data")as! String, viewcontrol: self)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)

            }
        }
    }
    func call_ChangePasswordAPI() {
        let dict = NSMutableDictionary.init()
        
        if nsud.value(forKey: "AppLanguage") != nil{
            if nsud.value(forKey: "AppLanguage")as! String == "English"{
                dict.setValue("1", forKey: "language_preference")
            }else{
                dict.setValue("2", forKey: "language_preference")
            }
        }else{
            dict.setValue("1", forKey: "language_preference")
        }
        let dictLoginData = nsud.value(forKey: "LogInData")as! NSDictionary

        dict.setValue(txtOldPass.text!, forKey: "old_password")//1
        dict.setValue(txtNewPass.text!, forKey: "new_password")//2
        
        let fullNameArr = (dictLoginData.value(forKey: "patient_id")as! NSString).components(separatedBy: "-")
        let surname = fullNameArr[2]
        dict.setValue(surname, forKey: "patient_id")//4
        dict.setValue("\(dictLoginData.value(forKey: "location_id")!)", forKey: "location_id")//4

        print(dict)
        showLoader(strMsg: "", style: .dark)
        WebService.callAPIBYPOST(parameter: dict, url: API_get_change_password) { (responce, status) in
            DispatchQueue.main.async {
                dismissLoader()
            }
            if(status == "Suceess"){
                let dictResponce = responce.value(forKey: "data")as! NSDictionary
                print(dictResponce)
                if("\(String(describing: dictResponce.value(forKey: "error")!))" == "1"){
                    let alert = UIAlertController(title: Localization("UTHC_Alert"), message: (dictResponce.value(forKey: "data")as! String), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title:  Localization("UTHC_Ok"), style: UIAlertAction.Style.default, handler: { action in
                        let mainVcIntial = kConstantObj.SetMainViewController(aStoryBoardID: "LoginVC")
                        appDelegate.window?.rootViewController = mainVcIntial
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if("\(String(describing: dictResponce.value(forKey: "error")!))" == "0"){
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: dictResponce.value(forKey: "data")as! String, viewcontrol: self)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)
            }
        }
    }
}
