//
//  SignupVC.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 2/12/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit


protocol refreshSignUp_Data : class{
    func refreshSignUpview(dict : NSDictionary ,tag : Int)
      func refreshSignUpviewWithString(str : String ,tag : Int)
}

class SignupVC: UIViewController {
    
    //MARK:
    //MARK: ---------------IBOutlet-----------------
   // @IBOutlet weak var scrollContain: UIScrollView!
    @IBOutlet weak var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmailAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtContactNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSecurityCode: SkyFloatingLabelTextField!

    @IBOutlet weak var btnAgreeTC: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnAgreeCheckBox: UIButton!
    @IBOutlet weak var txtDOB: SkyFloatingLabelTextField!
    

    @IBOutlet weak var txtEthnicity: SkyFloatingLabelTextField!

    
    var dictTermsConditions = NSMutableDictionary()
    var strStudyID = String()
    var strLocationID = String()
    var uuid = ""

}

     //MARK:
    //MARK: ---------------Life Cycle-----------------
    extension SignupVC{
        
        override func viewDidLoad() {
            super.viewDidLoad()
            configureViewFromLocalisation()
            uuid = UIDevice.current.identifierForVendor!.uuidString

        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            self.view.endEditing(true)
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        override func viewWillLayoutSubviews() {
           // scrollContain.contentSize = CGSize(width: scrollContain.contentSize.width, height: CGFloat(680))
            btnRegister.layer.cornerRadius = 20.0
        }

    }
    
    
  
    //MARK:
    //MARK: ---------------IBAction-----------------
    extension SignupVC{
    @IBAction func actionOnDOB(_ sender: UIButton) {
        
        let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.handleSignUpView = self
        
        if(sender.tag == 3){ // For Ethnicity
            
            let aryEthnicityOption = NSMutableArray()
            aryEthnicityOption.add(Localization("Register_Ethnicity_Option1"))
            aryEthnicityOption.add(Localization("Register_Ethnicity_Option2"))
            aryEthnicityOption.add(Localization("Register_Ethnicity_Option3"))
            aryEthnicityOption.add(Localization("Register_Ethnicity_Option4"))
            aryEthnicityOption.add(Localization("Register_Ethnicity_Option5"))
            aryEthnicityOption.add(Localization("Register_Ethnicity_Option6"))

            vc.arytbl = aryEthnicityOption
            vc.strTag = 25
            vc.strTitle = Localization("Register_SelectEthnicity")
        }else{
            vc.strTag = 9999
            vc.strTitle = Localization("Register_TC_Select_Date_Of_Birth")
        }
    
        self.present(vc, animated: false, completion: {})
    }
    
    
    @IBAction func actionOnBAck(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnAgreeTC(_ sender: UIButton) {
        if sender.tag == 1 {  // For T&C Radio
            if (sender.currentImage == #imageLiteral(resourceName: "redio_1")){
                sender.setImage(#imageLiteral(resourceName: "redio_2"), for: .normal)
            }else{
                sender.setImage(#imageLiteral(resourceName: "redio_1"), for: .normal)
            }
        }else if(sender.tag == 2){ // For T&C View
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TermsAndConditionVC") as! TermsAndConditionVC
            vc.strComeFrom = "Terms&Condition"
            self.navigationController?.pushViewController(vc, animated: true)
          
        }
    }
    
    @IBAction func actionOnRegister(_ sender: UIButton) {
        
        if ((txtFirstName.text?.count)! == 0){
            
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_FirstName"), viewcontrol: self)
            
        }else if ((txtLastName.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_LastName"), viewcontrol: self)
            
        }else if ((txtDOB.text?.count)! == 0) {
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_Age"), viewcontrol: self)
        }
        else if ((txtEthnicity.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_Ethnicity"), viewcontrol: self)
        }
        else if ((txtEmailAddress.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_EmailAddress"), viewcontrol: self)
            
        }else if !(validateEmail(email: txtEmailAddress.text!)){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_ValidEmailAddress"), viewcontrol: self)
            
        }else if ((txtContactNumber.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_ContactNumber"), viewcontrol: self)
            
        }
        else if ((txtContactNumber.text?.count)! < 10){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_ContactNumberValidation"), viewcontrol: self)
            
        }
        else if ((txtPassword.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_Password"), viewcontrol: self)
            
        }else if ((txtCPassword.text?.count)! == 0){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_CPassword"), viewcontrol: self)
            
        }else if (txtPassword.text! != txtCPassword.text!){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_CPasswordNotRight"), viewcontrol: self)
        }
        else if (btnAgreeCheckBox.currentImage == #imageLiteral(resourceName: "redio_1")){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_AgreeCondition"), viewcontrol: self)
        }
        else if (nsud.value(forKey: "SecurityCode") != nil){
            let strsecurityCode = "\(nsud.value(forKey: "SecurityCode")!)"
            if strsecurityCode.count != 0{
                if(txtSecurityCode.text != strsecurityCode){
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Register_Error_SecurityCode"), viewcontrol: self)
                }else{
                    self.hitSignUpApi()
                }
            }else{
                self.hitSignUpApi()
            }
        }
        else{
                  self.hitSignUpApi()
        }
    }
        func hitSignUpApi()  {
            if isInternetAvailable() {    //Check Network Condition
                call_RagistrationAPI()
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)
            }
        }
    
    @IBAction func actionOnMale_Female(_ sender: UIButton) {
        
        if sender.tag == 1 { // for male
            if btnMale.currentImage == #imageLiteral(resourceName: "redio_1") {
                btnMale.setImage(#imageLiteral(resourceName: "redio_2"), for: .normal)
                btnFemale.setImage(#imageLiteral(resourceName: "redio_1"), for: .normal)
                
            }else{
                btnMale.setImage(#imageLiteral(resourceName: "redio_1"), for: .normal)
                btnFemale.setImage(#imageLiteral(resourceName: "redio_2"), for: .normal)
            }
        }else if(sender.tag == 2){ // For Female
            if btnFemale.currentImage == #imageLiteral(resourceName: "redio_1") {
                btnFemale.setImage(#imageLiteral(resourceName: "redio_2"), for: .normal)
                btnMale.setImage(#imageLiteral(resourceName: "redio_1"), for: .normal)
            }else{
                btnFemale.setImage(#imageLiteral(resourceName: "redio_1"), for: .normal)
                btnMale.setImage(#imageLiteral(resourceName: "redio_2"), for: .normal)
                
            }
        }
        
        
    }
        
        
}
    
    
    //MARK:
    //MARK: ---------------API's Calling----------------
extension SignupVC{

    func call_RagistrationAPI() {
        let dict = NSMutableDictionary.init()
        
        if(nsud.value(forKey: "AppLanguage")as! String=="English"){  //1
            dict.setValue("1", forKey: "language_preference")
        }else{
            dict.setValue("2", forKey: "language_preference")
        }
        
        dict.setValue(strStudyID, forKey: "study_id") //2
        dict.setValue(strLocationID, forKey: "location_id") //2

        dict.setValue(txtFirstName.text!, forKey: "first_name")//3
        dict.setValue(txtLastName.text!, forKey: "last_name")//4
        dict.setValue(txtDOB.text!, forKey: "dob")//5
        if btnMale.currentImage == #imageLiteral(resourceName: "redio_2") {//6
            dict.setValue("1", forKey: "gender")
        }else{
            dict.setValue("2", forKey: "gender")
        }
        dict.setValue(txtEmailAddress.text!, forKey: "email")//7
        dict.setValue(txtContactNumber.text!, forKey: "contact_number")//8
        dict.setValue(txtPassword.text!, forKey: "login_password")//9
        dict.setValue(txtEthnicity.text!, forKey: "ethnicity")//10
        
        dict.setValue("\(UIDevice.current.systemVersion)", forKey: "device_version")
        dict.setValue("\(uuid)", forKey: "ip_address")
        dict.setValue("\(UIDevice().type)", forKey: "model_name")
        dict.setValue("registration", forKey: "request")
        
        print(dict)
        showLoader(strMsg: "", style: .dark)
        WebService.callAPIBYPOST(parameter: dict, url: API_Registration) { (responce, status) in
            dismissLoader()
            if(status == "Suceess"){
                let dictResponce = responce.value(forKey: "data")as! NSDictionary
                self.dictTermsConditions = NSMutableDictionary()
                if("\(String(describing: dictResponce.value(forKey: "error")!))" == "1"){
                    let dict = dictResponce.value(forKey: "data")as! NSDictionary
                    let registration = dict.value(forKey: "registration")as! NSDictionary
                    nsud.set("YES", forKey: "RegistrationStatus")
                    nsud.set(registration, forKey: "RegistrationData")
                    nsud.set("\(String(describing: registration.value(forKey: "patient_id")!))", forKey: "patient_id")
                    nsud.synchronize()
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "UniqueIDVC") as! UniqueIDVC
                    vc.strpatient_id = "\(String(describing: registration.value(forKey: "patient_id")!))"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else if("\(String(describing: dictResponce.value(forKey: "error")!))" == "0"){
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: dictResponce.value(forKey: "data")as! String, viewcontrol: self)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)
            }
        }
    }
}
    
    //MARK:
    //MARK: ---------------Extra Function's-----------------
extension SignupVC{

    func configureViewFromLocalisation() {
        
        txtFirstName.placeholder = Localization("Register_FirstName")
        txtLastName.placeholder = Localization("Register_LastName")
        txtDOB.placeholder = Localization("Register_Age")
        txtEmailAddress.placeholder = Localization("Register_EmailAddress")
        txtContactNumber.placeholder = Localization("Register_ContactNumber")
        txtPassword.placeholder = Localization("Register_Password")
        txtCPassword.placeholder = Localization("Register_CPassword")
        txtEthnicity.placeholder = Localization("Register_Ethnicity")
        btnAgreeTC.setTitle(Localization(""), for: .normal)
        
        let yourAttributes : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "282828"),
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
   
        let attributeString = NSMutableAttributedString(string: Localization("Register_AgreeTC"),
                                                        attributes: yourAttributes)
      
        btnAgreeTC.setAttributedTitle(attributeString, for: .normal)
        btnRegister.setTitle(Localization("Register_Register"), for: .normal)
        btnMale.setTitle(Localization("Register_Male"), for: .normal)
        btnFemale.setTitle(Localization("Register_FeMale"), for: .normal)
        
    }
}

//MARK:
//MARK: -----------TEXT FILED DELEGATE METHOD----------

extension SignupVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtFirstName || textField == txtLastName )
        {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 20)
        }
        if(textField == txtPassword ||  textField == txtCPassword)
        {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 15)
        }
        
        if(textField == txtEmailAddress)
        {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 30)
        }
        if(textField == txtContactNumber)
        {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 12)
        }
        return true
    }
    
 //---------For Change KeyBoard Language-------
//    override var textInputMode: UITextInputMode? {
//        for tim in UITextInputMode.activeInputModes {
//            print(UITextInputMode.activeInputModes)
//            if nsud.value(forKey: "AppLanguage") != nil{
//                if nsud.value(forKey: "AppLanguage")as! String == "English"{
//                    if tim.primaryLanguage!.contains("en") {
//                        return tim
//                    }
//                }else if( nsud.value(forKey: "AppLanguage")as! String == "Spanish"){
//                    if tim.primaryLanguage!.contains("es") {
//                        return tim
//                    }
//                }
//            }
//        }
//        return super.textInputMode
//    }
    
}

//MARK:-
//MARK:- ----------Refresh Delegate Methods----------
extension SignupVC: refreshSignUp_Data {
    func refreshSignUpviewWithString(str: String, tag: Int) {
        if(tag == 25 ){
            txtEthnicity.text = str
         
        }
    }
    
    func refreshSignUpview(dict: NSDictionary, tag: Int) {
        if tag == 9999 {  // For DOB
            txtDOB.text = "\(String(describing: dict.value(forKey: "date")!))"
        }else if(tag == 25 ){
            
        }
    }
}
