//
//  LoginVC.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 2/12/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit


//MARK:
//MARK: ---------------IBOutlet
class LoginVC: UIViewController {
    
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnRememberMe: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgetPass: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnCantAccess: UIButton!
    @IBOutlet weak var widthBtnRegister: NSLayoutConstraint!
    
    var window: UIWindow?
    
}

//MARK:
//MARK: ---------------LifeCycle
extension LoginVC{
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewFromLocalisation()
        btnLogin.titleLabel?.adjustsFontSizeToFitWidth = true
        btnLogin.titleLabel?.minimumScaleFactor = 2.0
       btnRememberMe.setImage(#imageLiteral(resourceName: "redio_1"), for: .normal)

 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        DispatchQueue.main.async {
            dismissLoader()
        }
        
    }
    override func viewWillLayoutSubviews() {
        makeButtonCorner(radius: 20.0, color: UIColor.black, btn: btnLogin, borderWidth: 0.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
        if (nsud.value(forKey: "patient_id") != nil) {
            txtEmail.text = "\(String(describing: nsud.value(forKey: "patient_id")!))"
        }
        configureViewFromLocalisation()
        
    }
}

//MARK:
//MARK: ---------------All IBAction
extension LoginVC{
    
    @IBAction func actionOnRememberMe(_ sender: UIButton) {
        if sender.currentImage == #imageLiteral(resourceName: "redio_1") {
            sender.setImage(#imageLiteral(resourceName: "redio_2"), for: .normal)
        }else{
            sender.setImage(#imageLiteral(resourceName: "redio_1"), for: .normal)
        }
    }
    
    @IBAction func actionOnLogin_Forget_Register(_ sender: UIButton) {
        self.view.endEditing(true)
        //----------------Login----------------//
        if sender.tag == 0 {
            if ((txtEmail.text?.count)! == 0){
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Login_Error_PatientID"), viewcontrol: self)
            }  else if ((txtPassword.text?.count)! == 0){
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Login_Error_Password"), viewcontrol: self)
            } else{
                if isInternetAvailable() {    //Check Network Condition
                    API_CallLogin()
                }else{
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)
                }
            }
        }
            //----------------Ragistration
        else if (sender.tag == 1){
              API_For_Insert_request_data()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LanguageSelectionVC") as! LanguageSelectionVC
            vc.strComeFrom = "LanguageSelection"
            self.navigationController?.pushViewController(vc, animated: true)
        }
            //----------------Forget Password
        else if (sender.tag == 2){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ForgetPasswordVC") as! ForgetPasswordVC
            vc.strComeFromScreen = "ForgetPass"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:
//MARK: --------------API's Calling
extension LoginVC{

    func API_CallLogin()  {
        let dictLoginData  = NSMutableDictionary()
        dictLoginData.setValue(txtEmail.text, forKey: "patient_id")
        dictLoginData.setValue(txtPassword.text, forKey: "password")
        
        showLoader(strMsg: "", style: .dark)
        WebService.callAPIBYPOST(parameter: dictLoginData, url: API_Login) { (responce, status) in
            DispatchQueue.main.async {
                dismissLoader()
            }
            if(status == "Suceess"){
                let dictResponce = responce.value(forKey: "data")as! NSDictionary
                if("\(String(describing: dictResponce.value(forKey: "error")!))" == "1"){
                    let dict = dictResponce.value(forKey: "data")as! NSDictionary
                    let registration = dict.value(forKey: "registration")as! NSDictionary
                    self.languageSutup(language: registration.value(forKey: "language_preference")as! String)
                    
                    if self.btnRememberMe.currentImage == #imageLiteral(resourceName: "redio_2"){
                        nsud.set("YES", forKey: "RememberMeStatus")
                    }else{
                        nsud.set("NO", forKey: "RememberMeStatus")
                    }
                    
                    nsud.set("\(String(describing: registration.value(forKey: "patient_id")!))", forKey: "patient_id")
                    nsud.set("\(String(describing: self.txtPassword.text!))", forKey: "password")

                    nsud.set("YES", forKey: "LoginStatus")
                    nsud.set(registration, forKey: "LogInData")
                    nsud.synchronize()
                    
                    if ("\(String(describing: registration.value(forKey: "is_pre_analysis_complete")!))" == "1"){
                        nsud.set("5", forKey: "AnswerStatus")
                        let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "DashBoardVC")
                        appDelegate.window?.rootViewController = mainVcIntial
                    }else{
                        nsud.set("0", forKey: "AnswerStatus")
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "PreTreatMentNotRelatedTooth_1VC") as! PreTreatMentNotRelatedTooth_1VC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else if("\(String(describing: dictResponce.value(forKey: "error")!))" == "0"){
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: dictResponce.value(forKey: "data")as! String, viewcontrol: self)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)
            }
        }
    }
    func API_For_Insert_request_data()  {
        let dict  = NSMutableDictionary()
        dict.setValue("\(UIDevice.current.systemVersion)", forKey: "device_version")
        dict.setValue("\(UIDevice.current.identifierForVendor!.uuidString)", forKey: "ip_address")
        dict.setValue("\(UIDevice().type)", forKey: "model_name")
        dict.setValue("RegisterNow", forKey: "request")
        print(dict)
       // showLoader(strMsg: "", style: .prominent)
        WebService.callAPIBYPOST(parameter: dict, url: API_Insert_request_data) { (responce, status) in
            print("--------------------------------\(responce)")
        //    dismissLoader()
            if(status == "Suceess"){
                let dictResponce = responce.value(forKey: "data")as! NSDictionary
                if("\(String(describing: dictResponce.value(forKey: "error")!))" == "1"){
                  
                }
                else if("\(String(describing: dictResponce.value(forKey: "error")!))" == "0"){
                   // showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: responce.value(forKey: "data")as! String, viewcontrol: self)
                }
            }else{
                self.API_For_Insert_request_data()
            }
        }
    }
}

//MARK:
//MARK: -----------Extra Function

extension LoginVC{
    
    func configureViewFromLocalisation() {
        txtEmail.placeholder = Localization("Login_PatientID")
        txtPassword.placeholder = Localization("Login_Password")
        btnRememberMe.setTitle(Localization("Login_RememberMe"), for: .normal)
        btnLogin.setTitle(Localization("Login_Login"), for: .normal)
        btnCantAccess.setTitle(Localization("Login_CantAccess"), for: .normal)
        let yourAttributes : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "282828"),
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: Localization("Login_RegisterNow"),
                                                        attributes: yourAttributes)
        btnRegister.setAttributedTitle(attributeString, for: .normal)
        let attributeString1 = NSMutableAttributedString(string:Localization("Login_ForgotPass"),
                                                         attributes: yourAttributes)
        btnForgetPass.setAttributedTitle(attributeString1, for: .normal)
        
        
        
        if ((nsud.value(forKey: "AppLanguage")) != nil){
            let strAppLanguage = (nsud.value(forKey: "AppLanguage"))!
            
            if (strAppLanguage as! String  == "English"){
                widthBtnRegister.constant = 0.0
            }else if(strAppLanguage as! String  == "Spanish"){
                widthBtnRegister.constant = -30.0
                
            }
        }
        
        
    }
    
    func languageSutup(language : String) {
        let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
        if language == "2"{
            if SetLanguage(arrayLanguages[0]) {
                nsud.setValue("Spanish", forKey: "AppLanguage")
            }
        }else{
            if SetLanguage(arrayLanguages[1]) {
                nsud.setValue("English", forKey: "AppLanguage")
            }
        }
    }
}

//MARK:
//MARK: -----------Text Filed Delegate Method

extension LoginVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtEmail)
        {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 30)
        }
        if(textField == txtPassword)
        {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 15)
        }
        return true
    }
    
    
    //---------For Change KeyBoard Language-------
    //
    //    override var textInputMode: UITextInputMode? {
    //        for tim in UITextInputMode.activeInputModes {
    //            print(UITextInputMode.activeInputModes)
    //            if nsud.value(forKey: "AppLanguage") != nil{
    //                if nsud.value(forKey: "AppLanguage")as! String != "English"{
    //                    if tim.primaryLanguage!.contains("es") {
    //                        return tim
    //                    }
    //                }else{
    //                    return super.textInputMode
    //
    //                }
    //            }
    //
    //        }
    //        return super.textInputMode
    //    }
}
