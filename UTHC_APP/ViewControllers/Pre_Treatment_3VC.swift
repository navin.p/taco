//
//  Pre_Treatment_3VC.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 3/30/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class Pre_Treatment_3VC: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnContinue: UIButton!
    
    //MARK:
    //MARK: ---------------Life Cycle----------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewFromLocalisation()
        makeButtonCorner(radius: 15.0, color: UIColor.clear, btn: btnContinue, borderWidth: 0.0)       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:
    //MARK: ---------------All IBAction's-----------------
    @IBAction func action_Continue(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PreTreatMentRelatedToothVC") as! PreTreatMentRelatedToothVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:
    //MARK: ---------------Extra Function-----------------
    func configureViewFromLocalisation() {
        lblTitle.text = Localization("UTHC_AppName")
        lblSubTitle.text = Localization("Pre-Treatment Analysis3_SubTitle")
        btnContinue.setTitle(Localization("UniqueID_Continue"), for: .normal)
    }
}
