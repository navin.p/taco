//
//  BodyPartVC.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 3/21/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class BodyPartVC: UIViewController {

    @IBOutlet weak var lbl_HeaderTitle: UILabel!
    @IBOutlet weak var lbl_SubTitle: UILabel!
    @IBOutlet weak var view_FrontImage: UIView!
    @IBOutlet weak var viewBackImage: UIView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnFlip: UIButton!

    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    @IBOutlet weak var lbl7: UILabel!
    @IBOutlet weak var lbl8: UILabel!
    @IBOutlet weak var lbl9: UILabel!
    @IBOutlet weak var lbl10: UILabel!
    @IBOutlet weak var lbl11: UILabel!

    @IBOutlet weak var lblBacknack: UILabel!
    @IBOutlet weak var lblBackhead: UILabel!

    @IBOutlet weak var lblBackUpper: UILabel!
    @IBOutlet weak var lblBacklower: UILabel!
    @IBOutlet weak var lblbuttocks: UILabel!

    @IBOutlet weak var lbladbomen: UILabel!
    @IBOutlet weak var btnFrontBody: UIButton!
    @IBOutlet weak var lblFrontBodyUnderLine: UILabel!
    @IBOutlet weak var btnBackBody: UIButton!
    @IBOutlet weak var lblSecondBodyUnderLine: UILabel!
    
    let arySelected = NSMutableArray()
    
    //MARK:
    //MARK: ---------------Life Cycle--------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewFromLocalisation()
       self.viewBackImage.isHidden = true
        btnFlip.setImage(#imageLiteral(resourceName: "flip_1"), for: .normal)
        makeButtonCorner(radius: 15.0, color: UIColor.black, btn: btnContinue, borderWidth: 0.0)
        self.viewBackImage.isHidden = true
        UIView.transition(with: view, duration: 0.5, options: .autoreverse, animations: {
            self.view_FrontImage.isHidden = false
        })
        btnFrontBody.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        btnBackBody.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.lblFrontBodyUnderLine.backgroundColor = PrimeryTheamColor
        self.lblSecondBodyUnderLine.backgroundColor = UIColor.lightGray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        for btn: Any in viewBackImage.subviews {
            if (btn is UIButton) {
                    (btn as AnyObject).setImage(UIImage(named: "check_box_1"), for: .normal)
            }
        }
        for btn: Any in view_FrontImage.subviews {
                   if (btn is UIButton) {
                           (btn as AnyObject).setImage(UIImage(named: "check_box_1"), for: .normal)
                   }
               }
    }
    
    //MARK:
    //MARK: ---------------All IBAction's----------------
    
    @IBAction func actionOnFlipButton(_ sender: UIButton) {
        
        if sender.tag == 1 { //For Front
            self.viewBackImage.isHidden = true
            UIView.transition(with: view, duration: 0.5, options: .autoreverse, animations: {
                self.view_FrontImage.isHidden = false
            })
            btnFrontBody.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            btnBackBody.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            self.lblFrontBodyUnderLine.backgroundColor = PrimeryTheamColor
            self.lblSecondBodyUnderLine.backgroundColor = UIColor.lightGray
        }else{  // For Back
            self.view_FrontImage.isHidden = true
            UIView.transition(with: view, duration: 0.5, options: .autoreverse, animations: {
                self.viewBackImage.isHidden = false
            })
            btnBackBody.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            btnFrontBody.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            self.lblSecondBodyUnderLine.backgroundColor = PrimeryTheamColor
            self.lblFrontBodyUnderLine.backgroundColor = UIColor.lightGray
        }
    }
    
    @IBAction func actionOnContinueButton(_ sender: Any) {
        
        if arySelected.count != 0 {
            print(arySelected)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PreTreatMentNotrelatedTooth_2VC") as! PreTreatMentNotrelatedTooth_2VC
            vc.aryselectedBodyParts = arySelected
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("BodyPart_Alert"), viewcontrol: self)

            
        }
        
   
    }
    
    
    @IBAction func actionOnBackImage_CheckBox(_ sender: UIButton) {
        
        var lbltitle = ""
        
        for label: Any in viewBackImage.subviews {
            if (label is UILabel) {
                if (label as AnyObject).tag == sender.tag{
                    lbltitle = (label as! UILabel).text!
                }
            }
        }
        if sender.currentImage == #imageLiteral(resourceName: "check_box_1") {
            if arySelected.contains(lbltitle) {
               arySelected.remove(lbltitle)
            }else{
                arySelected.add(lbltitle)
            }
            sender.setImage(#imageLiteral(resourceName: "check_box_2"), for: .normal)
        }else{
            if arySelected.contains(lbltitle) {
                arySelected.remove(lbltitle)
              //  arySelected.add(lbltitle)
            }else{
                arySelected.add(lbltitle)
            }
            sender.setImage(#imageLiteral(resourceName: "check_box_1"), for: .normal)
        }
    }
    
    @IBAction func actionOnFrontImage_CheckBox(_ sender: UIButton) {
        var lbltitle = ""
        for label: Any in view_FrontImage.subviews {
            if (label is UILabel) {
                if (label as AnyObject).tag == sender.tag{
                    lbltitle = (label as! UILabel).text!
                }
            }
        }
 
        
        if sender.currentImage == UIImage(named: "check_box_1"){
            if arySelected.contains(lbltitle) {
                arySelected.remove(lbltitle)
            }else{
                arySelected.add(lbltitle)
            }
            sender.setImage(UIImage(named: "check_box_2"), for: .normal)
        }else{
            if arySelected.contains(lbltitle) {
                arySelected.remove(lbltitle)
            }else{
                arySelected.add(lbltitle)
            }
            sender.setImage(UIImage(named: "check_box_1"), for: .normal)
        }
    }
    
    
    //MARK:
    //MARK: ---------------Extra Function's-----------------
    
    func configureViewFromLocalisation() {
        
        lbl1.text = Localization("BodyPart_1")
        lbl2.text = Localization("BodyPart_2")
        lbl3.text = Localization("BodyPart_3")
        lbl4.text = Localization("BodyPart_4")
        lbl5.text = Localization("BodyPart_5")
        lbl6.text = Localization("BodyPart_6")
        lbl7.text = Localization("BodyPart_7")
        lbl8.text = Localization("BodyPart_8")
        lbl9.text = Localization("BodyPart_9")
        lbl10.text = Localization("BodyPart_10")
        lbl11.text = Localization("BodyPart_11")

        lblBackhead.text = Localization("BodyPart_HeadBack")
        lblBacknack.text = Localization("BodyPart_NeckBack")
        lblBackUpper.text = Localization("BodyPart_Upperback")
        lblBacklower.text = Localization("BodyPart_Lowerback")
        lblbuttocks.text = Localization("BodyPart_Buttocks")
        
        btnContinue.setTitle(Localization("BodyPart_Continue"), for: .normal)
        lbl_HeaderTitle.text = Localization("BodyPart_Title")
        lbl_SubTitle.text = Localization("BodyPart_SubTitle")
        btnBackBody.setTitle(Localization("BodyPart_Back"), for: .normal)
        btnFrontBody.setTitle(Localization("BodyPart_Front"), for: .normal)

    }
}
