//
//  EmailVC.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 4/25/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class EmailVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtMessage: KMPlaceholderTextView!
    
    @IBOutlet weak var txtTo: UITextField!
    
    @IBOutlet weak var txtSubject: UITextField!
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var scrollContain: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let dictAdmin = nsud.value(forKey: "AdminInfo")as! NSDictionary
        txtTo.text = dictAdmin.value(forKey: "email")as?String
        txtSubject.text =  Localization("Email_Emergency")
        configureViewFromLocalisation()
        scrollContain.contentSize = CGSize(width: scrollContain.contentSize.width, height: CGFloat(600))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    //MARK:- --------All IBAction's----------
    
    @IBAction func actionOnBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnSubmit(_ sender: Any) {
        
        if txtTo.text?.count == 0 {
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)

        }else if (txtSubject.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)

        }else if (txtMessage.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Email_Alert_Msg"), viewcontrol: self)

        }else{
        
        if isInternetAvailable() {    //Check Network Condition
            callAPI_SensMail()
            
        }else{
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)
            }
            
        }
    }
    //MARK:-
    //MARK:- ---------Extra Function----------
    func configureViewFromLocalisation() {
        //--------------------Email Screen-------------------------
        lblTo.text =  Localization("Email_TO")
        lblSubject.text =  Localization("Email_Subect")
        makeButtonCorner(radius: 20.0, color: UIColor.black, btn: btnSend, borderWidth: 0.0)
        txtMessage.placeholder = Localization("Email_Message")
        btnSend.setTitle(Localization("Email_Send"), for: .normal)
        lblTitle.text = Localization("PostQuestion_Email")

    }
    //MARK:
    //MARK: ---------------API's Calling--------------
    @objc func callAPI_SensMail() {

        let dict = NSMutableDictionary.init()
        if nsud.value(forKey: "AppLanguage") != nil{
        if nsud.value(forKey: "AppLanguage")as! String == "English"{
        dict.setValue("1", forKey: "language_preference")
        }else{
        dict.setValue("2", forKey: "language_preference")
        }
        }else{
        dict.setValue("1", forKey: "language_preference")
        }
        
        let dictLoginData = nsud.value(forKey: "LogInData")as! NSDictionary
        let fullNameArr = (dictLoginData.value(forKey: "patient_id")as! NSString).components(separatedBy: "-")
        let surname = fullNameArr[2]
      
        dict.setValue(txtMessage.text, forKey: "message")//1
        dict.setValue(txtTo.text, forKey: "to")//2
        dict.setValue(txtSubject.text, forKey: "subject")//3
        dict.setValue(surname, forKey: "patient_id")//4
        dict.setValue("\(dictLoginData.value(forKey: "location_id")!)", forKey: "location_id")//5
 
        
        WebService.callAPI(parameter: dict, url:API_Patient_contact_email) { (responce, status) in
            FTIndicator.dismissProgress()
            print(responce)
            if(status == "Suceess"){

                if(responce.value(forKey: "error")as! String == "1"){

                    let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "DashBoardVC")
                    appDelegate.window?.rootViewController = mainVcIntial
                }
                else if(responce.value(forKey: "error")as! String == "0"){
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: responce.value(forKey: "data")as! String, viewcontrol: self)

                }else{
                    
                }
            }
        }
    }
}
