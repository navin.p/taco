//
//  DashBoardVC.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 2/14/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class DashBoardVC: UIViewController {
    
    
    //MARK:
    //MARK: ---------------IBOutlet-----------------
    @IBOutlet weak var tvForNotiList: UITableView!
    @IBOutlet weak var view_ForBottom: UIView!
    @IBOutlet weak var heightForViewBottom: NSLayoutConstraint!
    @IBOutlet weak var lbl_Static_HeaderTitle: UILabel!
    @IBOutlet weak var lbl_Static_1W: UILabel!
    @IBOutlet weak var lbl_Static_2W: UILabel!
    @IBOutlet weak var lbl_Static_3W: UILabel!
    @IBOutlet weak var lbl_Static_4W: UILabel!
    @IBOutlet weak var lbl_Static_5W: UILabel!
    @IBOutlet weak var lbl_Static_6W: UILabel!
    @IBOutlet weak var lbl_Static_7W: UILabel!
    @IBOutlet weak var lbl_Static_8W: UILabel!
    @IBOutlet weak var lbl_Static_treatmentfinish_Today: UILabel!
    @IBOutlet weak var btn_YES: UIButton!
    @IBOutlet weak var btn_NO: UIButton!
    @IBOutlet weak var btn_Submit: UIButton!
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var lblDataNotFound: UILabel!
    @IBOutlet weak var heightSliderView: NSLayoutConstraint!
    var refreshControl: UIRefreshControl!
    var aryTBL = NSMutableArray()
    let step: Float = 1
    var strFinishedTodayStatus = String()
    
    var dictQuestionData = NSMutableDictionary()

    //MARK:
    //MARK: ---------------LifeCycle-----------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = btn_Submit.backgroundColor
        refreshControl.addTarget(self, action: #selector(refreshtableView), for: .valueChanged)
        tvForNotiList.addSubview(refreshControl) 
        
        tvForNotiList.estimatedRowHeight = 85.0
        tvForNotiList.tableFooterView = UIView()
        configureViewFromLocalisation()
        makeButtonCorner(radius: 08.0, color: UIColor.black, btn: btn_YES, borderWidth: 1.0)
        makeButtonCorner(radius: 08.0, color: UIColor.black, btn: btn_NO, borderWidth: 1.0)
        makeButtonCorner(radius: 15.0, color: UIColor.black, btn: btn_Submit, borderWidth: 0.0)
        self.heightForViewBottom.constant = 0.0
        
        
        self.heightSliderView.constant = 0.0
        if isInternetAvailable() {
            callAPI_API_AddDevice()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setColorOnlabel(color: UIColor.black)
        
        FTIndicator.dismissProgress()
        if isInternetAvailable() {
            callAPI_GetNotification(str: "first")
            callAPI_AdminInfo()
            
        }else{
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)
        }
        // animateTable()
    }
    override func viewWillLayoutSubviews() {
        
    }
    //MARK:
    //MARK: ---------------All IBAction-----------------
    
    
    
    @IBAction func actionOnButtons(_ sender: UIButton) {
        if sender.tag == 0 {  // For YES
            strFinishedTodayStatus = (btn_YES.titleLabel?.text!)!
            btn_YES.backgroundColor = hexStringToUIColor(hex: "BD4701")
            btn_NO.backgroundColor = UIColor.white
            btn_YES.setTitleColor(UIColor.white, for: .normal)
            btn_NO.setTitleColor(UIColor.black, for: .normal)
            makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn_YES, borderWidth: 0.0)
            makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn_NO, borderWidth: 1.0)
            
        }else if sender.tag == 1{  // For NO
            strFinishedTodayStatus = (btn_NO.titleLabel?.text!)!
            btn_NO.backgroundColor = hexStringToUIColor(hex: "BD4701")
            btn_YES.backgroundColor = UIColor.white
            btn_NO.setTitleColor(UIColor.white, for: .normal)
            btn_YES.setTitleColor(UIColor.black, for: .normal)
            makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn_YES, borderWidth: 1.0)
            makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn_NO, borderWidth: 0.0)
            
        }else if(sender.tag == 2){ // For Submit
            if strFinishedTodayStatus.count != 0{
                API_Call_Save_PreTretmentQuestions()
            }
            
        }else if(sender.tag == 3){ // For Menu
            NotificationCenter.default.post(name: Notification.Name("UserLoggedIn"), object: nil)
            // self.setColorOnlabel(color: UIColor.clear)
            sideMenuVC.toggleMenu()
        }
    }
    
    
    //MARK:
    //MARK: ---------------Extra Function's-----------------
    
    @objc func refreshtableView() {
        if isInternetAvailable() {
            callAPI_GetNotification(str: "reload")
        }else{
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)
        }
        
    }
    func configureViewFromLocalisation() {
        //btn_NO.setTitle(Localization("DashBoard_No"), for: .normal)
        //btn_YES.setTitle(Localization("DashBoard_Yes"), for: .normal)
        btn_Submit.setTitle(Localization("DashBoard_Submit"), for: .normal)
        lbl_Static_1W.text = Localization("DashBoard_1W")
        lbl_Static_2W.text = Localization("DashBoard_2W")
        lbl_Static_3W.text = Localization("DashBoard_3W")
        lbl_Static_4W.text = Localization("DashBoard_4W")
        lbl_Static_5W.text = Localization("DashBoard_5W")
        lbl_Static_6W.text = Localization("DashBoard_6W")
        lbl_Static_7W.text = Localization("DashBoard_7W")
        lbl_Static_8W.text = Localization("DashBoard_8W")
        lbl_Static_HeaderTitle.text = Localization("UTHC_AppName")
        //  lbl_Static_treatmentfinish_Today.text = Localization("DashBoard_bottomViewTitle")
    }
    
    func setColorOnlabel(color: UIColor)  {
        lbl_Static_1W.textColor = color
        lbl_Static_2W.textColor = color
        lbl_Static_3W.textColor = color
        lbl_Static_4W.textColor = color
        lbl_Static_5W.textColor = color
        lbl_Static_6W.textColor = color
        lbl_Static_7W.textColor = color
        lbl_Static_8W.textColor = color
    }
    
    //MARK:
    //MARK: ---------------API's Calling--------------
    
    @objc func callAPI_API_AddDevice() {
        let dict = NSMutableDictionary.init()
        let dictLoginData = nsud.value(forKey: "LogInData")as! NSDictionary
        
        let fullNameArr = (dictLoginData.value(forKey: "patient_id")as! NSString).components(separatedBy: "-")
        let surname = fullNameArr[2]
        
        dict.setValue("\(dictLoginData.value(forKey: "location_id")!)", forKey: "location_id")//1
        dict.setValue(surname, forKey: "patient_id")//2
        dict.setValue(strApp_platform, forKey: "platform")//3
        
        if(nsud.value(forKey: "FCM_Token") != nil){
            dict.setValue("\(nsud.value(forKey: "FCM_Token")!)", forKey: "device_id")//4
            print(dict)
            WebService.callAPI(parameter: dict, url:API_AddDevice) { (responce, status) in
                print(responce)
            }
        }
    }
    
    
    
    @objc func callAPI_AdminInfo() {
        let dict = NSMutableDictionary.init()
        let dictLoginData = nsud.value(forKey: "LogInData")as! NSDictionary
        
        let fullNameArr = (dictLoginData.value(forKey: "patient_id")as! NSString).components(separatedBy: "-")
        let surname = fullNameArr[2]
        dict.setValue("\(surname)", forKey: "patient_id")//2
        dict.setValue("\(dictLoginData.value(forKey: "location_id")!)", forKey: "location_id")//1
        
        WebService.callAPI(parameter: dict, url:API_Admin_Information) { (responce, status) in
            print(responce)
            if(status == "Suceess"){
                if(responce.value(forKey: "error")as! String == "1"){
                    
                    //   let strFrequencyTime = "\((responce.value(forKey: "data")as! NSDictionary).value(forKey: "login_days")!)"
                    //  strFrequencyTime = "100"
                    //  if (Int(strFrequencyTime)! < FrequencyTime){
                    nsud.set(responce.value(forKey: "data")as! NSDictionary, forKey: "AdminInfo")
                    if ((((responce.value(forKey: "data")as! NSDictionary).value(forKey: "question")as! NSDictionary).value(forKey: "data")as! String) == "NO" || (((responce.value(forKey: "data")as! NSDictionary).value(forKey: "question")as! NSDictionary).value(forKey: "data")as! String) == "Inactive"){
                        self.heightForViewBottom.constant = 0.0
                    }else{
                        self.heightForViewBottom.constant = 150.0
                        let dictLogInData = (responce.value(forKey: "data")as! NSDictionary)
                        let questionDict = dictLogInData.value(forKey: "question")as! NSDictionary
                        self.dictQuestionData = NSMutableDictionary()
                        self.dictQuestionData = questionDict.mutableCopy()as! NSMutableDictionary
                        
                        let question_title = "\(questionDict.value(forKey: "title")!)"
                        self.lbl_Static_treatmentfinish_Today.text = question_title
                        let dictOption = (questionDict.value(forKey: "options")as! NSArray).object(at: 0)as! NSDictionary
                        if (dictOption.value(forKey: "options_list") as! NSArray).count == 2{
                            self.btn_YES.setTitle((dictOption.value(forKey: "options_list") as! NSArray).object(at: 0) as? String, for: .normal)
                            self.btn_NO.setTitle((dictOption.value(forKey: "options_list") as! NSArray).object(at: 1) as? String, for: .normal)
                            
                            self.btn_YES.backgroundColor = UIColor.white
                            makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: self.btn_YES, borderWidth: 1.0)
                            self.btn_YES.setTitleColor(UIColor.black, for: .normal)
                            
                            self.btn_NO.backgroundColor = UIColor.white
                            makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: self.btn_NO, borderWidth: 1.0)
                            self.btn_NO.setTitleColor(UIColor.black, for: .normal)
                        }
                    }
                    //      }
                    //                    else{
                    //
                    //                        let alert = UIAlertController(title: Localization("UTHC_Alert"), message:  Localization("UTHC_SessionExpire"), preferredStyle: UIAlertControllerStyle.alert)
                    //                        // add the actions (buttons)
                    //
                    //                        alert.addAction(UIAlertAction (title: Localization("UTHC_Ok"), style: .default, handler: { (nil) in
                    //                                    UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                    //                            let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
                    //                            if SetLanguage(arrayLanguages[1]) {
                    //                                nsud.setValue("English", forKey: "AppLanguage")
                    //                            }
                    //                            nsud.setValue("English", forKey: "AppLanguage")
                    //                            let mainVcIntial = kConstantObj.SetMainViewController(aStoryBoardID: "LoginVC")
                    //                               appDelegate.window?.rootViewController = mainVcIntial
                    //                        }))
                    //                        self.present(alert, animated: true, completion: nil)
                    //
                    //                    }
                    
                }
                else if(responce.value(forKey: "error")as! String == "0"){
                    
                }else{
                    
                }
            }
        }
    }
    
    @objc func callAPI_GetNotification(str:NSString) {
        let dict = NSMutableDictionary.init()
        if nsud.value(forKey: "AppLanguage") != nil{
            if nsud.value(forKey: "AppLanguage")as! String == "English"{
                dict.setValue("1", forKey: "language_preference")
            }else{
                dict.setValue("2", forKey: "language_preference")
            }
        }else{
            dict.setValue("1", forKey: "language_preference")
        }
        
        let dictLoginData = nsud.value(forKey: "LogInData")as! NSDictionary
        let fullNameArr = (dictLoginData.value(forKey: "patient_id")as! NSString).components(separatedBy: "-")
        let surname = fullNameArr[2]
        dict.setValue(surname, forKey: "patient_id")//3
        dict.setValue("\(dictLoginData.value(forKey: "location_id")!)", forKey: "location_id")//4
        if str != "reload" || aryTBL.count == 0 {
            showLoader(strMsg: "", style: .extraLight)
        }
        //API_get_survey_notification
        WebService.callAPI(parameter: dict, url:API_get_survey_notification) { (responce, status) in
            FTIndicator.dismissProgress()
            self.refreshControl.endRefreshing()
            print(responce)
            if(status == "Suceess"){

                if(responce.value(forKey: "login_status")as! String == "1"){
                    
                    if(responce.value(forKey: "error")as! String == "1"){
                        self.aryTBL = NSMutableArray()
                        let dictDat = (responce.value(forKey: "data")as! NSDictionary)
                        if dictDat["notification"] != nil {
                            self.aryTBL = ((responce.value(forKey: "data")as! NSDictionary).value(forKey: "notification")as!NSArray).mutableCopy()as! NSMutableArray
                        }
                        
                        if self.aryTBL.count == 0{
                            self.tvForNotiList.isHidden = true
                        }else{
                            self.tvForNotiList.isHidden = false
                        }
                        self.tvForNotiList.reloadData()
                        var strValue = ((responce.value(forKey: "data")as! NSDictionary).value(forKey: "status_bar")as!String)
                        self.heightSliderView.constant = 0.0
                        if (strValue.count != 0) {
                            if (strValue.removeLast() == "W"){
                                let result = String(((responce.value(forKey: "data")as! NSDictionary).value(forKey: "status_bar")as!String).dropLast(1))
                                if (Float(result)! > 8 )  {
                                    self.slider.value = Float(8.0)
                                    DispatchQueue.main.async {
                                        self.setColorOnlabel(color: UIColor.black)
                                    }
                                    self.heightSliderView.constant = 80.0
                                    
                                }else{
                                    self.slider.value = Float(result)!
                                    DispatchQueue.main.async {
                                        self.setColorOnlabel(color: UIColor.black)
                                    }
                                    self.heightSliderView.constant = 80.0
                                    
                                }
                                
                            }else{
                                self.heightSliderView.constant = 80.0
                                self.slider.value = Float(0.0)
                            }
                        }
                    }
                    else if(responce.value(forKey: "error")as! String == "0"){
                        self.lblDataNotFound.text = responce.value(forKey: "data")as? String
                        //  showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: responce.value(forKey: "data")as! String, viewcontrol: self)
                        self.heightSliderView.constant = 0.0
                        self.tvForNotiList.isHidden = true
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)
                        
                    }
                }else{
                    let alertController = UIAlertController(title: Localization("UTHC_Alert"), message: Localization("UTHC_ErrorAccountDelete"), preferredStyle: .alert)
                    // Create the actions
                    let okAction = UIAlertAction(title: Localization("UTHC_Ok"), style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        let p_ID = "\(String(describing: nsud.value(forKey: "patient_id")!))"
                        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        nsud.setValue(p_ID, forKey: "patient_id")
                        let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "LoginVC")
                        appDelegate.window?.rootViewController = mainVcIntial
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }else{
                let alertController = UIAlertController(title: Localization("UTHC_Alert"), message: Localization("UTHC_OtherError"), preferredStyle: .alert)
                // Create the actions
                let okAction = UIAlertAction(title: Localization("UTHC_TryAgain"), style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    self.callAPI_GetNotification(str: str)
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
    }
    
    
    
    //MARK:
    //MARK: ---------------API Calling-----------------
    
    func API_Call_Save_PreTretmentQuestions()  {
        let dict = NSMutableDictionary.init()
        
        let dictLogInData = nsud.value(forKey: "LogInData")as! NSDictionary
        var strPtientID = "\(String(describing: dictLogInData.value(forKey: "patient_id")!))"
        let fullNameArr = strPtientID.components(separatedBy: "-")
        strPtientID = fullNameArr[2]
        
        dict.setValue(strPtientID, forKey: "patient_id")
        dict.setValue("1", forKey: "is_pre_analysis_complete")
        dict.setValue("\(dictLogInData.value(forKey: "location_id")!)", forKey: "location_id")//4
        
        
        let aryoptions = dictQuestionData.value(forKey: "options")as! NSArray
        var dictOptionList = NSDictionary()
        if (aryoptions.count != 0){
            dictOptionList = aryoptions.object(at: 0)as! NSDictionary
        }
        
        let option_type = "\(dictQuestionData.value(forKey: "option_type")!)"
        let question_id = "\(dictQuestionData.value(forKey: "id")!)"
        let question_title = "\(dictQuestionData.value(forKey: "title")!)"
        
        let question_option_id = "\(dictOptionList.value(forKey: "id")!)"
        let question_option_title = strFinishedTodayStatus
        let selected_option_type = "\(dictQuestionData.value(forKey: "selected_option_type")!)"
        
        
        let dictData1 = NSMutableDictionary()
        let dictSendData = NSMutableDictionary()
        dictSendData.setValue(question_id, forKey: "question_id")
        dictSendData.setValue(question_title, forKey: "question_title")
        dictSendData.setValue(question_option_id, forKey: "question_option_id")
        dictSendData.setValue(selected_option_type, forKey: "selected_option_type")
        dictSendData.setValue(option_type, forKey: "option_type")
        dictSendData.setValue(question_option_title, forKey: "question_option_title")
        dictData1.setValue(dictSendData, forKey:question_id)
        print(dict)
        print(dictData1)
        dict.setValue(jsontoString(fromobject: dictData1), forKey: "question")
        
        showLoader(strMsg: "", style: .dark)
        WebService.callAPI(parameter: dict, url: API_Save_Pre_Analysis) { (responce, status) in
            print(responce)
            DispatchQueue.main.async {
                dismissLoader()
            }
            if(status == "Suceess"){
                self.heightForViewBottom.constant = 0.0
                
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)
            }
        }
    }
    
}

//MARK:-
//MARK:- ----------UITableView Delegate Methods----------

extension DashBoardVC : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTBL.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = aryTBL.object(at: indexPath.row)as! NSDictionary
        if "\(String(describing: dict.value(forKey: "status")!))" == "Pending" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NotificationQuestionVC") as! NotificationQuestionVC
            vc.dictNotiData = (aryTBL.object(at: indexPath.row)as! NSDictionary).mutableCopy()as!NSMutableDictionary
            if (indexPath.row == self.aryTBL.count - 1){
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvForNotiList.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath as IndexPath) as! CommonTableCellVC
        let dict = aryTBL.object(at: indexPath.row)as! NSDictionary
        
        cell.lbl_NotiCell_title.text = "\(String(describing: dict.value(forKey: "title")!))"
        cell.lbl_NotiCell_status.text = "\(Localization("DashBoard_Status")) - \(String(describing: dict.value(forKey: "status")!))"
      
        if "\(String(describing: dict.value(forKey: "status")!))" == "Pending" ||  "\(String(describing: dict.value(forKey: "status")!))" == "Incomplete"{
            cell.btn_NotiCellstatusIcon.setImage(#imageLiteral(resourceName: "pending"), for: .normal)
            cell.btn_NotiCellstatusIcon.tintColor =  UIColor.darkGray
            cell.lbl_NotiCell_status.textColor =  UIColor.black
            if "\(String(describing: dict.value(forKey: "status")!))" == "Pending"{
                cell.contentView.alpha = 1.0
            }else{
                cell.contentView.alpha = 0.5
            }
        }else{
            cell.btn_NotiCellstatusIcon.setImage(#imageLiteral(resourceName: "complete"), for: .normal)
            cell.btn_NotiCellstatusIcon.tintColor =  hexStringToUIColor(hex: "37B207")
            cell.lbl_NotiCell_status.textColor =  hexStringToUIColor(hex: "37B207")
            cell.contentView.alpha = 0.5
        }
    
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
