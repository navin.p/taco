
//
//  LanguageSelectionVC.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 2/14/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
//MARK: ---------------Protocol-----------------
protocol refreshLanguage_Data : class{
    func refreshlanguageview(dict : NSDictionary ,tag : Int)
}

//MARK: ---------------Class
class LanguageSelectionVC: UIViewController {
    
    //MARK:
    //MARK: ---------------IBOutlet
    
    @IBOutlet weak var lbl_Static_SelectLanguage: UILabel!
    @IBOutlet weak var lbl_static_StudyID: UILabel!
    @IBOutlet weak var lbl_Static_locationID: UILabel!
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var btnSpanish: UIButton!
    @IBOutlet weak var lblSelect: UILabel!
    
    @IBOutlet weak var lblSelectLocation: UILabel!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var viewSelectPID: UIView!
    @IBOutlet weak var viewSelectLID: UIView!
    
    
    
    var aryForStudyID = NSMutableArray()
    var aryForLocationID = NSMutableArray()
    
    var strComeFrom = String()
    var strSelectionStatus = String()
    var uuid = ""

    
    //MARK:
    //MARK: ---------------LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblSelect.tag = 0
        uuid = UIDevice.current.identifierForVendor!.uuidString

    
        if nsud.value(forKey: "AppLanguage") != nil{
            if nsud.value(forKey: "AppLanguage")as! String == "Spanish"{
                btnSpanish.setTitleColor(UIColor.white, for: .normal)
                btnSpanish.backgroundColor = PrimeryTheamColor
                makeButtonCorner(radius: 15.0, color: UIColor.white, btn: btnSpanish, borderWidth: 0.0)
                btnEnglish.setTitleColor(UIColor.black, for: .normal)
                btnEnglish.backgroundColor = UIColor.white
                makeButtonCorner(radius: 15.0, color: UIColor.black, btn: btnEnglish, borderWidth: 1.0)
            }else{
                btnEnglish.setTitleColor(UIColor.white, for: .normal)
                btnEnglish.backgroundColor = PrimeryTheamColor
                makeButtonCorner(radius: 15.0, color: UIColor.white, btn: btnEnglish, borderWidth: 0.0)
                
                btnSpanish.setTitleColor(UIColor.black, for: .normal)
                btnSpanish.backgroundColor = UIColor.white
                makeButtonCorner(radius: 15.0, color: UIColor.black, btn: btnSpanish, borderWidth: 1.0)
            }
        }else{
            let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()

            nsud.setValue("English", forKey: "AppLanguage")
            if SetLanguage(arrayLanguages[1]) {
            }
            btnEnglish.setTitleColor(UIColor.white, for: .normal)
            btnEnglish.backgroundColor = PrimeryTheamColor
            makeButtonCorner(radius: 15.0, color: UIColor.white, btn: btnEnglish, borderWidth: 0.0)
            
            btnSpanish.setTitleColor(UIColor.black, for: .normal)
            btnSpanish.backgroundColor = UIColor.white
            makeButtonCorner(radius: 15.0, color: UIColor.black, btn: btnSpanish, borderWidth: 1.0)
        }
      
        makeButtonCorner(radius: 15.0, color: UIColor.black, btn: btnContinue, borderWidth: 0.0)
        
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        configureViewFromLocalisation()
        if strComeFrom == "LanguageSelection" {
            lbl_static_StudyID.isHidden = false
            viewSelectPID.isHidden = false
            viewSelectLID.isHidden = false
            lbl_Static_locationID.isHidden = false
            if isInternetAvailable() {    //Check Network Condition
                if(aryForLocationID.count == 0){
                    API_CallLoaction(tag: 0)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)
            }
        }else{
            lbl_static_StudyID.isHidden = true
            viewSelectPID.isHidden = true
            viewSelectLID.isHidden = true
            lbl_Static_locationID.isHidden = true
        }
    }

    //MARK:
    //MARK: ---------------IBAction's
    @IBAction func actionOnLanguageChangeButton(_ sender: UIButton) {
        
        let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
        
        
        if sender.tag == 1 {    // For English Language
            
            if strComeFrom == "LanguageSelection" { // For Selction
                
                if SetLanguage(arrayLanguages[1]) {
                    nsud.setValue("English", forKey: "AppLanguage")
                    btnEnglish.setTitleColor(UIColor.white, for: .normal)
                    btnEnglish.backgroundColor = PrimeryTheamColor
                    makeButtonCorner(radius: 15.0, color: UIColor.white, btn: btnEnglish, borderWidth: 0.0)
                    
                    btnSpanish.setTitleColor(UIColor.black, for: .normal)
                    btnSpanish.backgroundColor = UIColor.white
                    makeButtonCorner(radius: 15.0, color: UIColor.black, btn: btnSpanish, borderWidth: 1.0)
                    
                    
                }
            }else{   // For Change Language
                
                strSelectionStatus = "1" // or English
                
                btnEnglish.setTitleColor(UIColor.white, for: .normal)
                btnEnglish.backgroundColor = PrimeryTheamColor
                makeButtonCorner(radius: 15.0, color: UIColor.white, btn: btnEnglish, borderWidth: 0.0)
                
                btnSpanish.setTitleColor(UIColor.black, for: .normal)
                btnSpanish.backgroundColor = UIColor.white
                makeButtonCorner(radius: 15.0, color: UIColor.black, btn: btnSpanish, borderWidth: 1.0)
            }
            
            
        }else if(sender.tag == 2){  // For Spanish Languge
            
            if strComeFrom == "LanguageSelection" { // For Selction
                if SetLanguage(arrayLanguages[0]) {
                    nsud.setValue("Spanish", forKey: "AppLanguage")
                    btnSpanish.setTitleColor(UIColor.white, for: .normal)
                    btnSpanish.backgroundColor = PrimeryTheamColor
                    makeButtonCorner(radius: 15.0, color: UIColor.white, btn: btnSpanish, borderWidth: 0.0)
                    
                    btnEnglish.setTitleColor(UIColor.black, for: .normal)
                    btnEnglish.backgroundColor = UIColor.white
                    makeButtonCorner(radius: 15.0, color: UIColor.black, btn: btnEnglish, borderWidth: 1.0)
                }
            }else{   // For Change Language
                strSelectionStatus = "2" // or Spanish
                btnSpanish.setTitleColor(UIColor.white, for: .normal)
                btnSpanish.backgroundColor = PrimeryTheamColor
                makeButtonCorner(radius: 15.0, color: UIColor.white, btn: btnSpanish, borderWidth: 0.0)
                
                btnEnglish.setTitleColor(UIColor.black, for: .normal)
                btnEnglish.backgroundColor = UIColor.white
                makeButtonCorner(radius: 15.0, color: UIColor.black, btn: btnEnglish, borderWidth: 1.0)
            }
        }
        lblSelectLocation.tag = 0
        lblSelect.tag = 0
        configureViewFromLocalisation()
    }
    
    
    @IBAction func actionOnSelectLocationID(_ sender: UIButton) {
        if aryForLocationID.count == 0 {
            if isInternetAvailable() {    //Check Network Condition
                API_CallLoaction(tag: 1)
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)
            }
        }else{
            self.openPopUpView(arySend: self.aryForLocationID, tag: 3, title: Localization("Language_SelectLocationID"))
        }
    }
    
    @IBAction func actionOnSelectStudyID(_ sender: UIButton) {
        
            if isInternetAvailable() {    //Check Network Condition
                if !(lblSelectLocation.tag == 0){
                    if(self.aryForStudyID.count != 0){
                        self.openPopUpView(arySend: self.aryForStudyID, tag: 1, title: Localization("Language_SelectStudyID"))
                    }else{
                        API_CallStudyID(tag: 1)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Language_SelectLocationID"), viewcontrol: self)
                    
                }
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)
            }
    }
    
    @IBAction func actionOnContinue(_ sender: UIButton) {
        
        // For Selction
        if strComeFrom == "LanguageSelection" {
           if lblSelectLocation.tag == 0 {
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Language_Error_LocationID"), viewcontrol: self)
            }else  if lblSelect.tag == 0 {
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("Language_Error_StudyID"), viewcontrol: self)
            }
            else{
            
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
            vc.strLocationID = "\(lblSelectLocation.tag)"
            vc.strStudyID = "\(lblSelect.tag)"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
         // For Change Language
        
        else{
            if isInternetAvailable() {    //Check Network Condition
                callAPIChangeLanguage()
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)
            }
        }
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        if strComeFrom == "LanguageSelection" {
            self.navigationController?.popViewController(animated: true)
        }else{
            let mainVcIntial = kConstantObj.SetMainViewController(aStoryBoardID: "DashBoardVC")
            appDelegate.window?.rootViewController = mainVcIntial
        }
    }
    
    
    //MARK:
    //MARK: --------------API's Calling
    
    func API_CallLoaction(tag : Int)  {
        let dict = NSMutableDictionary.init()
        if nsud.value(forKey: "AppLanguage") != nil{
            if nsud.value(forKey: "AppLanguage")as! String == "English"{
                dict.setValue("1", forKey: "language_preference")
            }else{
                dict.setValue("2", forKey: "language_preference")
            }
        }else{
            dict.setValue("1", forKey: "language_preference")
        }
        dict.setValue("\(UIDevice.current.systemVersion)", forKey: "device_version")
        dict.setValue("\(uuid)", forKey: "ip_address")
        dict.setValue("\(UIDevice().type)", forKey: "model_name")
        dict.setValue("admin_location_list", forKey: "request")
        print(dict)
      
        showLoader(strMsg: "", style: .prominent)
        
        WebService.callAPI(parameter: dict, url: API_Admin_location_list) { (responce, status) in
            print(responce)
            FTIndicator.dismissProgress()
            if(status == "Suceess"){
                if(responce.value(forKey: "error")as! String == "1"){
                    // Call From StudyID Button When Data Not load
                    self.aryForLocationID = NSMutableArray()
                    self.aryForLocationID = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    if tag == 1{
                        self.openPopUpView(arySend: self.aryForLocationID, tag: 3, title: Localization("Language_SelectLocationID"))
                    }
                }
            }else{
                    let alertController = UIAlertController(title: Localization("UTHC_Alert"), message: Localization("UTHC_OtherError"), preferredStyle: .alert)
                    // Create the actions
                let okAction = UIAlertAction(title: Localization("UTHC_TryAgain"), style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        self.API_CallLoaction(tag: tag)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
        }
    }
    
    func API_CallStudyID(tag : Int)  {
        let dict = NSMutableDictionary.init()
        
        if nsud.value(forKey: "AppLanguage") != nil{
            if nsud.value(forKey: "AppLanguage")as! String == "English"{
                dict.setValue("1", forKey: "language_preference")
            }else{
                dict.setValue("2", forKey: "language_preference")
            }
        }else{
            dict.setValue("1", forKey: "language_preference")
        }
        dict.setValue("\(lblSelectLocation.tag)", forKey: "location_id")
        
        
        dict.setValue("\(UIDevice.current.systemVersion)", forKey: "device_version")
        dict.setValue("\(uuid)", forKey: "ip_address")
        dict.setValue("\(UIDevice().type)", forKey: "model_name")
        dict.setValue("study_id_list", forKey: "request")
        print(dict)
        
        showLoader(strMsg: "", style: .prominent)
        
        WebService.callAPI(parameter: dict, url: API_StudyID) { (responce, status) in
            print(responce)
                dismissLoader()
            if(status == "Suceess"){
                if(responce.value(forKey: "error")as! String == "1"){
                    self.aryForStudyID = NSMutableArray()
                    self.aryForStudyID = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    // Call From StudyID Button When Data Not load
                    if tag == 1{
                        self.openPopUpView(arySend: self.aryForStudyID, tag: 1, title: Localization("Language_SelectStudyID"))
                    }
                } else if("\(String(describing: responce.value(forKey: "error")!))" == "0"){
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: responce.value(forKey: "data")as! String, viewcontrol: self)
                }
            }else{
                let alertController = UIAlertController(title: Localization("UTHC_Alert"), message: Localization("UTHC_OtherError"), preferredStyle: .alert)
                // Create the actions
                let okAction = UIAlertAction(title: Localization("UTHC_TryAgain"), style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    self.API_CallStudyID(tag: tag)
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func callAPIChangeLanguage() {
        let dict = NSMutableDictionary.init()
        
        if nsud.value(forKey: "AppLanguage") != nil{
            if nsud.value(forKey: "AppLanguage")as! String == "English"{
                dict.setValue("1", forKey: "old_language_preference")
            }else{
                dict.setValue("2", forKey: "old_language_preference")
            }
        }else{
            dict.setValue("1", forKey: "old_language_preference")
        }
        
        let dictLoginData = nsud.value(forKey: "LogInData")as! NSDictionary
        let fullNameArr = (dictLoginData.value(forKey: "patient_id")as! NSString).components(separatedBy: "-")
        let surname = fullNameArr[2]
        
        dict.setValue(surname, forKey: "patient_id")//4
        dict.setValue(strSelectionStatus, forKey: "new_language_preference")
        dict.setValue("\(dictLoginData.value(forKey: "location_id")!)", forKey: "location_id")//4
    
        showLoader(strMsg: "", style: .prominent)
        
        WebService.callAPI(parameter: dict, url: API_change_language_preference) { (responce, status) in
            print(responce)
                dismissLoader()
            if(status == "Suceess"){
                if("\(String(describing: responce.value(forKey: "error")!))" == "1"){
                    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
                    if (self.strSelectionStatus == "1"){
                        if SetLanguage(arrayLanguages[1]) {
                            nsud.setValue("English", forKey: "AppLanguage")
                        }
                    }else{
                        if SetLanguage(arrayLanguages[0]) {
                            nsud.setValue("Spanish", forKey: "AppLanguage")
                        }
                    }
                    
                    let alert = UIAlertController(title: Localization("UTHC_Alert"), message: Localization("Language_Error_LanguageChange"), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title:  Localization("UTHC_Ok"), style: UIAlertAction.Style.default, handler: { action in
                        let mainVcIntial = kConstantObj.SetMainViewController(aStoryBoardID: "DashBoardVC")
                        appDelegate.window?.rootViewController = mainVcIntial
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else if("\(String(describing: responce.value(forKey: "error")!))" == "0"){
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: responce.value(forKey: "data")as! String, viewcontrol: self)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)
            }
        }
    }
    
    //MARK:
    //MARK: ---------------Extra Function's
    
    func openPopUpView(arySend : NSMutableArray , tag : Int , title : String)  {
        let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.handlelanguageView = self
        vc.arytbl =  arySend
        vc.strTag = tag
        vc.strTitle = title
        self.present(vc, animated: false, completion: {})
    }
    
    
    func configureViewFromLocalisation() {
        btnEnglish.setTitle(Localization("Language_English"), for: .normal)
        btnSpanish.setTitle(Localization("Language_Spanish"), for: .normal)
        btnContinue.setTitle(Localization("Language_Continue"), for: .normal)
        lbl_Static_SelectLanguage.text = Localization("Language_SelectLanguage")
        
   

        lbl_static_StudyID.text = Localization("Language_SelectStudyID")
        lbl_Static_locationID.text = Localization("Language_SelectLocationID")
        
        if(lblSelect.tag == 0){
                 lblSelect.text = Localization("Language_Select")
        }
        if(lblSelectLocation.tag == 0){
            lblSelectLocation.text = Localization("Language_Select")
        }
    }
    
    func openActionSheet_AlertView()  {
        let alert = UIAlertController(title: Localization("UTHC_Alert"), message: Localization("UTHC_KeyBoardMessage"), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: Localization("UTHC_Setting"), style: UIAlertAction.Style.default, handler: { action in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string:"App-prefs:root=General&path=Keyboard")!)
            } else {
                UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
            }
        }))
        alert.addAction(UIAlertAction(title: Localization("UTHC_Cancel"), style: UIAlertAction.Style.default,handler: { action in
        }))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK:-
//MARK:- ----------Refresh Delegate Methods

extension LanguageSelectionVC: refreshLanguage_Data {
    func refreshlanguageview(dict: NSDictionary, tag: Int) {
        print(dict)

        if tag == 1 { // For study_id
            lblSelect.text = dict.value(forKey: "study_id")as? String
            lblSelect.tag = Int("\(String(describing: dict.value(forKey: "id")!))")!
            nsud.set("\(String(describing: dict.value(forKey: "security_code")!))", forKey: "SecurityCode")

        }else if ( tag == 3){
            
            lblSelectLocation.text = dict.value(forKey: "location_prefix")as? String
            lblSelectLocation.tag = Int("\(String(describing: dict.value(forKey: "id")!))")!
              lblSelect.text = "Select"
               lblSelect.tag = 0
            self.aryForStudyID = NSMutableArray()

        }
    }
}
