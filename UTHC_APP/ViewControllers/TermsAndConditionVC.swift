//
//  TermsAndConditionVC.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 3/29/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class TermsAndConditionVC: UIViewController {
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var heightImage: NSLayoutConstraint!
    
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var lblTitle: UILabel!
    var strComeFrom = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        if isInternetAvailable() {    //Check Network Condition
            call_TermsConditionAPI()
        }else{
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)
        }
        
        if strComeFrom != "Terms&Condition" {
            lblTitle.text = Localization( Localization("About_Title"))
            heightImage.constant = 150.0
            
        }else{
            lblTitle.text = Localization( Localization("Register_TC"))
            heightImage.constant = 0.0

        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func actionOnBackButton(_ sender: Any) {
        
        if strComeFrom != "Terms&Condition" {
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "DashBoardVC")
            appd.window?.rootViewController = mainVcIntial
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    func call_TermsConditionAPI()  {
        let dict = NSMutableDictionary.init()
        if nsud.value(forKey: "AppLanguage") != nil{
            if nsud.value(forKey: "AppLanguage")as! String == "English"{
                dict.setValue("1", forKey: "language_preference")
            }else{
                dict.setValue("2", forKey: "language_preference")
            }
        }else{
            dict.setValue("1", forKey: "language_preference")
        }
        showLoader(strMsg: "", style: .extraLight)
        var strUrl = ""
        if strComeFrom != "Terms&Condition" {
            strUrl = API_AboutApp
            let dictLoginData = nsud.value(forKey: "LogInData")as! NSDictionary
            dict.setValue("\(dictLoginData.value(forKey: "location_id")!)", forKey: "location_id")//4
        }else{
            strUrl = API_Terms_Condition
        }
        WebService.callAPI(parameter: dict, url: strUrl) { (responce, status) in
            DispatchQueue.main.async {
                dismissLoader()
            }
            print(responce)
            if(status == "Suceess"){
                var dictTermsConditions = NSMutableDictionary()
                if(responce.value(forKey: "error")as! String == "1"){
                    dictTermsConditions = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if dictTermsConditions.count != 0{
                        var strTermsConditions = (dictTermsConditions.value(forKey: "text")as!String)
                        strTermsConditions = strTermsConditions.replacingOccurrences(of: "\\n", with: "\n")
                        strTermsConditions = strTermsConditions.replacingOccurrences(of: "\\r", with: "\r")
                        self.txtView.text = strTermsConditions
                        self.heightImage.constant = 0.0
                        if self.strComeFrom != "Terms&Condition" {
                            if !("\(dictTermsConditions.value(forKey: "image")!)" == ""){
                                self.imgView.setImageWith(URL(string: (dictTermsConditions.value(forKey: "image")as!String)), placeholderImage: UIImage(), options:.init(rawValue: 0), usingActivityIndicatorStyle: .gray)
                                self.heightImage.constant = 150.0
                            }
                        }
                    }
                }      else if(responce.value(forKey: "error")as! String == "0"){
                  responce.value(forKey: "data")as! String
                    
                      showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: responce.value(forKey: "data")as! String, viewcontrol: self)
                    
                }
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)
            }
        }
    }
}
