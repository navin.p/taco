//
//  PreTreatMentNotRelatedTooth_1VC.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 3/28/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData

//MARK: ---------------Protocol-----------------
protocol refreshPre_TretmentQuestion1_Data : class{
    func refreshview(str : String ,tag : Int)
}


class PreTreatMentNotRelatedTooth_1VC: UIViewController {
    
    var  aryForPreTrementQuestionList = NSMutableArray()  // For All Data
    var dictAllQuestion = NSMutableDictionary()
    var  aryForPreTre_NotRelatedTooth = NSMutableArray()  // Only NotRelated tooth
    var  aryForWellness = NSMutableArray()
    var  aryForLastScreen = NSMutableArray()
    var  aryForReletedtoTooth = NSMutableArray()
    var  aryForTvList = NSMutableArray()
    var tagForDropDown = Int()
    var strBackPainTag = Int()
    
    @IBOutlet weak var tvForList: UITableView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblHeaderSubTitle: UILabel!
    @IBOutlet weak var btnContinue: UIButton!
    
    
    //MARK:
    //MARK: --------------Life Cycle----------------
    override func viewDidLoad() {
        super.viewDidLoad()
        tvForList.tableFooterView = UIView()
        tvForList.estimatedRowHeight = 95.0
        configureViewFromLocalisation()
        makeButtonCorner(radius: 15.0, color: UIColor.clear, btn: btnContinue, borderWidth: 0.0)
        API_CallPreTretmentQuestionList()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    //MARK:
    //MARK: --------------API's Calling-----------------
    
    func API_CallPreTretmentQuestionList()  {
        let dict = NSMutableDictionary.init()
        if nsud.value(forKey: "AppLanguage") != nil{
            if nsud.value(forKey: "AppLanguage")as! String == "English"{
                dict.setValue("1", forKey: "language_preference")
            }else{
                dict.setValue("2", forKey: "language_preference")
            }
        }else{
            dict.setValue("1", forKey: "language_preference")
        }
        let dictLogInData = nsud.value(forKey: "LogInData")as! NSDictionary
        dict.setValue("\(dictLogInData.value(forKey: "location_id")!)", forKey: "location_id")   //1
        
        let fullNameArr = (dictLogInData.value(forKey: "patient_id")as! NSString).components(separatedBy: "-")
        let surname = fullNameArr[2]
        dict.setValue(surname, forKey: "patient_id")//2
        print(dict)
        showLoader(strMsg: "", style: .dark)
        WebService.callAPI(parameter: dict, url: API_Pre_Survey_Question) { (responce, status) in
            print(responce)
            DispatchQueue.main.async {
                dismissLoader()
            }
            if(status == "Suceess"){
                
                if(responce.value(forKey: "login_status")as! String == "1"){
                    if(responce.value(forKey: "error")as! String == "1"){
                        deleteAllRecords(strEntity:"PreTretmentQuestion")
                        saveDataInLocalDictionary(strEntity: "PreTretmentQuestion", strKey: "question", data: (responce ).mutableCopy() as! NSMutableDictionary)
                        self.filterDataAccordingtoType()
                    } else if("\(String(describing: responce.value(forKey: "error")!))" == "0"){
                        //  showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: responce.value(forKey: "data")as! String, viewcontrol: self)
                        let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "DashBoardVC")
                        appDelegate.window?.rootViewController = mainVcIntial
                        
                    }else{
                               showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)
                    }
                }else{
                    let alertController = UIAlertController(title: Localization("UTHC_Alert"), message: Localization("UTHC_ErrorAccountDelete"), preferredStyle: .alert)
                    
                    // Create the actions
                    let okAction = UIAlertAction(title: Localization("UTHC_Ok"), style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        let p_ID = "\(String(describing: nsud.value(forKey: "patient_id")!))"
                        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        nsud.setValue(p_ID, forKey: "patient_id")
                        let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "LoginVC")
                        appDelegate.window?.rootViewController = mainVcIntial
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)

                }
            }else{
                let alertController = UIAlertController(title: Localization("UTHC_Alert"), message: Localization("UTHC_OtherError"), preferredStyle: .alert)
                // Create the actions
                let okAction = UIAlertAction(title: Localization("UTHC_TryAgain"), style: UIAlertAction.Style.default) {
                    UIAlertAction in
                      self.API_CallPreTretmentQuestionList()
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
                
              
        }
    }
    
    func API_Call_Save_PreTretmentQuestions(aryData : NSMutableDictionary)  {
        let dict = NSMutableDictionary.init()
        if nsud.value(forKey: "AppLanguage") != nil{
            if nsud.value(forKey: "AppLanguage")as! String == "English"{
                dict.setValue("1", forKey: "language_preference")
            }else{
                dict.setValue("2", forKey: "language_preference")
            }
        }else{
            dict.setValue("1", forKey: "language_preference")
        }
        
        let dictLogInData = nsud.value(forKey: "LogInData")as! NSDictionary
        var strPtientID = "\(String(describing: dictLogInData.value(forKey: "patient_id")!))"
        let fullNameArr = strPtientID.components(separatedBy: "-")
        strPtientID = fullNameArr[2]
        
        dict.setValue(strPtientID, forKey: "patient_id")
        //For Check  next question available or not
        if (aryForWellness.count == 0 && aryForLastScreen.count == 0 && aryForReletedtoTooth.count == 0 ){
            dict.setValue("1", forKey: "is_pre_analysis_complete")
        }else{
            dict.setValue("0", forKey: "is_pre_analysis_complete")
        }
        dict.setValue(jsontoString(fromobject: aryData), forKey: "question")
        dict.setValue("\(dictLogInData.value(forKey: "location_id")!)", forKey: "location_id")//4
     
        
        print(dict)
        
        showLoader(strMsg: "", style: .dark)
        WebService.callAPI(parameter: dict, url: API_Save_Pre_Analysis) { (responce, status) in
            print(responce)
            DispatchQueue.main.async {
                dismissLoader()
            }
            if(status == "Suceess"){
                if(responce.value(forKey: "error")as! String == "1"){
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    if self.strBackPainTag == 1{  // For Body Pain Yes
                         // for chaeck body question Available or not
                        let aryTemp = getDataFromLocal(strEntity: "PreTretmentQuestion", strkey: "question")
                        let aryForPreTrementQuestionTemp = NSMutableArray()
                        if aryTemp.count > 0 {
                            for j in 0 ..< aryTemp.count {
                                var obj = NSManagedObject()
                                obj = aryTemp[j] as! NSManagedObject
                                aryForPreTrementQuestionTemp.add(obj.value(forKey: "question") ?? 0)
                            }
                        }
                        print(aryForPreTrementQuestionTemp)
                        var dictAllQuestion = NSMutableDictionary()

                        if aryForPreTrementQuestionTemp.count != 0 {
                            let dict = aryForPreTrementQuestionTemp.object(at: 0)as! NSDictionary
                            dictAllQuestion = NSMutableDictionary()
                            dictAllQuestion = (dict.value(forKey: "data") as! NSDictionary).mutableCopy()as! NSMutableDictionary
                        }
                        var aryForPreTre_NotRelatedTooth = NSMutableArray()
                        if dictAllQuestion.count != 0 {
                            aryForPreTre_NotRelatedTooth = NSMutableArray()
                            aryForPreTre_NotRelatedTooth = (dictAllQuestion.value(forKey: "pain_not_related_to_tooth")as! NSArray).mutableCopy() as! NSMutableArray
                            //wellness   pain_not_related_to_tooth
                        }
                       let aryForTvListtemp = NSMutableArray()
                        var strTemp = 0
                        for item in aryForPreTre_NotRelatedTooth {
                            let dictData = ((item as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            let dict = dictData.value(forKey: "question")as! NSDictionary
                            let str = "\(dict.value(forKey: "serial_no")!)"
                            dictData.setValue("", forKey: "answer")
                            dictData.setValue(NSMutableArray(), forKey: "Multianswer")
                            if !(Int(str)! < 4){
                                aryForTvListtemp.add(dictData)
                            }
                        }
                        
                        if (aryForTvListtemp.count != 0){
                                nsud.set("1", forKey: "AnswerStatus")
                            let vc = storyboard.instantiateViewController(withIdentifier: "BodyPartVC") as! BodyPartVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                                nsud.set("2", forKey: "AnswerStatus")
                            let vc = storyboard.instantiateViewController(withIdentifier: "Pre_Treatment_3VC") as! Pre_Treatment_3VC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    else{ // For Body Pain NO
                            nsud.set("2", forKey: "AnswerStatus")
                        let vc = storyboard.instantiateViewController(withIdentifier: "Pre_Treatment_3VC") as! Pre_Treatment_3VC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                } else if("\(String(describing: responce.value(forKey: "error")!))" == "0"){
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: responce.value(forKey: "data")as! String, viewcontrol: self)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)
            }
        }
    }
    
    //MARK:
    //MARK: --------------Extra Function---------------
    
    func configureViewFromLocalisation() {
        lblHeaderTitle.text = Localization("Pre-Treatment Analysis1_Title")
        lblHeaderSubTitle.text = Localization("Pre-Treatment Analysis1_SubTitle")
        btnContinue.setTitle(Localization("Pre-Treatment Analysis1_Continue"), for: .normal)
    }
    
    //MARK: Filter
    func filterDataAccordingtoType()  {
        DispatchQueue.main.async {
            dismissLoader()
        }
        let aryTemp = getDataFromLocal(strEntity: "PreTretmentQuestion", strkey: "question")
        aryForPreTrementQuestionList = NSMutableArray()
        let aryForPreTrementQuestionTemp = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryForPreTrementQuestionTemp.add(obj.value(forKey: "question") ?? 0)
            }
        }
        print(aryForPreTrementQuestionTemp)
        
        if aryForPreTrementQuestionTemp.count != 0 {
            let dict = aryForPreTrementQuestionTemp.object(at: 0)as! NSDictionary
            dictAllQuestion = NSMutableDictionary()
            dictAllQuestion = (dict.value(forKey: "data") as! NSDictionary).mutableCopy()as! NSMutableDictionary
        }
        
        if dictAllQuestion.count != 0 {
            aryForPreTre_NotRelatedTooth = NSMutableArray()
            aryForPreTre_NotRelatedTooth = (dictAllQuestion.value(forKey: "pain_not_related_to_tooth")as! NSArray).mutableCopy() as! NSMutableArray
            //wellness
            
            aryForReletedtoTooth = NSMutableArray()
            aryForReletedtoTooth = (dictAllQuestion.value(forKey: "pain_related_to_tooth")as! NSArray).mutableCopy() as! NSMutableArray
            aryForWellness = NSMutableArray()
            aryForWellness = (dictAllQuestion.value(forKey: "wellness")as! NSArray).mutableCopy() as! NSMutableArray
            aryForLastScreen = NSMutableArray()
            aryForLastScreen = (dictAllQuestion.value(forKey: "last_screen")as! NSArray).mutableCopy() as! NSMutableArray
        }
        aryForTvList = NSMutableArray()
        var temp = 0
        for item in aryForPreTre_NotRelatedTooth {
            let dictData = ((item as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
            let dict = dictData.value(forKey: "question")as! NSDictionary
            let str = "\(dict.value(forKey: "serial_no")!)"
            //--Set default value on slider question
            let strType = dict.value(forKey: "selected_option_type")as! String
            let stroption_type = dict.value(forKey: "option_type")as! String
            if(strType == "") && stroption_type == "1"{
                dictData.setValue("0.0", forKey: "answer")
            }else{
                dictData.setValue("", forKey: "answer")
            }
            dictData.setValue(NSMutableArray(), forKey: "Multianswer")
           
            if (Int(str)! < 4){
                if (str == "1"){
                    temp = 1
                }
                if temp == 1{
                    if (str != "2"){
                        aryForTvList.add(dictData)
                    }
                }else{
                    aryForTvList.add(dictData)

                    
                }
                
            }else{
                break
            }
        
        }
        
        // ----- For Hide show Screen According to DATA
        
        if aryForTvList.count == 0 {
            nsud.set("2", forKey: "AnswerStatus")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PreTreatMentRelatedToothVC") as! PreTreatMentRelatedToothVC
            self.navigationController?.pushViewController(vc, animated: false)
        }else{
            genrateDynemicView(aryData: aryForTvList)
            tvForList.reloadData()
        }
        
    }
    
    
    func underlineCreat(ract:CGRect) -> UILabel {
        let lbl = UILabel()
        lbl.frame = ract
        lbl.backgroundColor = UIColor.lightGray
        return lbl
    }
    
    //MARK: Dynemic
    
    func genrateDynemicView( aryData : NSMutableArray)  {
        
        for view in self.tvForList.subviews {
            view.removeFromSuperview()
        }
        var yAxix = 0.0
        
        for (index, item) in aryData.enumerated() {
            let dict = (item as AnyObject)as! NSDictionary
            let dictData = dict.mutableCopy()as! NSMutableDictionary
            let dictQuestion = dictData.value(forKey: "question")as! NSDictionary
            let strAnswer = "\(dictData.value(forKey: "answer")!)"
            let strType = dictQuestion.value(forKey: "selected_option_type")as! String
            let stroption_type = dictQuestion.value(forKey: "option_type")as! String
            
            
            
            //MARK:***** Yes_No_Button_View*****
            
            if(strType == "yes_no_button_view") && stroption_type == "0"{
                let lblTitleQuestionYesNo = UILabel()
                let btnYES = UIButton()
                let btnNO = UIButton()
                
                
                lblTitleQuestionYesNo.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitleQuestionYesNo.textColor = UIColor.black
                lblTitleQuestionYesNo.numberOfLines = 0
                lblTitleQuestionYesNo.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitleQuestionYesNo.text!, view: self.view) )
                
                
                yAxix = Double(CGFloat(yAxix) + lblTitleQuestionYesNo.frame.size.height  + 10)
                
                btnYES.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: 120 , height: 30)
                btnYES.layer.cornerRadius = 4.0
                btnYES.layer.borderWidth = 1.0
                btnYES.layer.borderColor = UIColor.gray.cgColor
                btnYES.layer.backgroundColor = UIColor.white.cgColor
                
                
                let dictOption = (dictQuestion.value(forKey: "options")as! NSArray).object(at: 0)as! NSDictionary
                if (dictOption.value(forKey: "options_list") as! NSArray).count == 2{
                    btnYES.setTitle((dictOption.value(forKey: "options_list") as! NSArray).object(at: 0) as? String, for: .normal)
                    btnNO.setTitle((dictOption.value(forKey: "options_list") as! NSArray).object(at: 1) as? String, for: .normal)
                }
                
                
                btnNO.frame = CGRect(x: btnYES.frame.origin.x + 125, y: btnYES.frame.origin.y, width: 120 , height: 30)
                
                
                if strAnswer == btnYES.titleLabel?.text!{
                    btnYES.backgroundColor = PrimeryTheamColor
                    btnNO.backgroundColor = UIColor.white
                    btnYES.setTitleColor(UIColor.white, for: .normal)
                    btnNO.setTitleColor(UIColor.black, for: .normal)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btnYES, borderWidth: 0.0)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btnNO, borderWidth: 1.0)
                }else if(strAnswer == btnNO.titleLabel?.text!){
                    btnNO.backgroundColor = PrimeryTheamColor
                    btnYES.backgroundColor = UIColor.white
                    btnNO.setTitleColor(UIColor.white, for: .normal)
                    btnYES.setTitleColor(UIColor.black, for: .normal)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btnYES, borderWidth: 1.0)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btnNO, borderWidth: 0.0)
                }else{
                    btnNO.backgroundColor = UIColor.white
                    btnYES.backgroundColor = UIColor.white
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btnYES, borderWidth: 1.0)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btnNO, borderWidth: 1.0)
                    btnYES.setTitleColor(UIColor.black, for: .normal)
                    btnNO.setTitleColor(UIColor.black, for: .normal)
                    
                }
                
                btnNO.tag = index
                btnYES.tag = index
                btnNO.addTarget(self, action: #selector(pressButtonNO(_:)), for: .touchUpInside)
                btnYES.addTarget(self, action: #selector(pressButtonYES(_:)), for: .touchUpInside)
                
                self.tvForList.addSubview(lblTitleQuestionYesNo)
                self.tvForList.addSubview(btnYES)
                self.tvForList.addSubview(btnNO)
                
                yAxix = Double(CGFloat(yAxix) +  btnNO.frame.size.height + 25.0)
                
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width ), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
                
            }
                
                //MARK:***** Single_Select_Redio_Button*****
                
                
            else if(strType == "single_select_redio_button") && stroption_type == "0"{
                
                //---Question
                let lblTitleRadio = UILabel()
                lblTitleRadio.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitleRadio.textColor = UIColor.black
                lblTitleRadio.numberOfLines = 0
                lblTitleRadio.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitleRadio.text!, view: self.view) )
                
                yAxix = Double(CGFloat(yAxix) + lblTitleRadio.frame.size.height  + 05)
                
                self.tvForList.addSubview(lblTitleRadio)
                
                //CheckBox
                
                let aryorOption = dictQuestion.value(forKey: "options")as! NSArray
                if aryorOption.count != 0{
                    
                    let dict = aryorOption.object(at: 0)as! NSDictionary
                    let aryOptionlist = dict.value(forKey: "options_list")as! NSArray
                    if aryOptionlist.count != 0{
                        for item in aryOptionlist{
                            let lblTitile = UILabel()
                            let btnImg  = UIButton()
                            btnImg.tag = index
                            btnImg.addTarget(self, action: #selector(pressButtonRadio(_:)), for: .touchDown)
                            lblTitile.textColor = UIColor.black
                            lblTitile.text = (item as AnyObject)as? String
                            btnImg.setTitle(lblTitile.text, for: .normal)
                            lblTitile.frame = CGRect(x: lblTitleRadio.frame.origin.x+30.0, y: CGFloat(yAxix), width: self.view.frame.width - 50 , height: 30)
                            btnImg.frame = CGRect(x:  lblTitleRadio.frame.origin.x, y: CGFloat(yAxix + 3), width: 30 , height: 30)
                            btnImg.setImage(#imageLiteral(resourceName: "pending"), for: .normal)
                            if (strAnswer as String).count != 0 {
                                if (btnImg.titleLabel?.text! ==  strAnswer){
                                    btnImg.setImage(#imageLiteral(resourceName: "redio_2"), for: .normal)
                                }
                            }
                            yAxix = yAxix + 40.0
                            self.tvForList.addSubview(lblTitile)
                            self.tvForList.addSubview(btnImg)
                        }
                    }
                }
                //---------UnderLine
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
            }
                
                //MARK:***** Single_Select_Drop_Down*****
                
            else if(strType == "single_select_drop_down") && stroption_type == "0"{
                
                let lblTitlesingle_select_drop_down = UILabel()
                lblTitlesingle_select_drop_down.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitlesingle_select_drop_down.textColor = UIColor.black
                lblTitlesingle_select_drop_down.numberOfLines = 0
                lblTitlesingle_select_drop_down.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitlesingle_select_drop_down.text!, view: self.view) )
                
                yAxix = Double(CGFloat(yAxix) + lblTitlesingle_select_drop_down.frame.size.height  + 15)
                
                let lblSubTitle_select_drop_down = UILabel()
                lblSubTitle_select_drop_down.text = "\(dictQuestion.value(forKey: "title")!)"
                lblSubTitle_select_drop_down.textColor = UIColor.black
                lblSubTitle_select_drop_down.numberOfLines = 0
                lblSubTitle_select_drop_down.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 20 )
                lblSubTitle_select_drop_down.text = Localization("Pre-Treatment Analysis2_Select_Body_Part")
                lblSubTitle_select_drop_down.font = UIFont(name: (lblSubTitle_select_drop_down.font?.fontName)!, size: 14)
                
                yAxix = Double(CGFloat(yAxix) + lblSubTitle_select_drop_down.frame.size.height + 10)
                
                
                let btnDrop = UIButton()
                btnDrop.setBackgroundImage(#imageLiteral(resourceName: "dropdown_2"), for: .normal)
                btnDrop.addTarget(self, action: #selector(pressButtonDropDown(_:)), for: .touchUpInside)
                btnDrop.tag = index
                btnDrop.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 35 )
                
                let lblselectedText = UILabel()
                lblselectedText.textColor = UIColor.black
                lblselectedText.numberOfLines = 0
                lblselectedText.textAlignment = .center
                lblselectedText.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 40 , height: 30 )
                lblselectedText.font = UIFont(name: (lblselectedText.font?.fontName)!, size: 14)
                
                if strAnswer.count != 0{
                    lblselectedText.text = strAnswer
                    lblselectedText.textColor = UIColor.black
                }else{
                    lblselectedText.textColor = UIColor.darkGray
                    lblselectedText.text = Localization("Pre-Treatment Analysis2_Select")
                }
                yAxix = Double(CGFloat(yAxix) + btnDrop.frame.size.height + 25)
                
                self.tvForList.addSubview(lblTitlesingle_select_drop_down)
                self.tvForList.addSubview(lblSubTitle_select_drop_down)
                self.tvForList.addSubview(lblselectedText)
                self.tvForList.addSubview(btnDrop)
                
                
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
            }
                
                //MARK:***** Single_Select_Button_View*****
                
                
            else if(strType == "single_select_button_view") && stroption_type == "0"{
                
                //---Question
                let lblTitlesingle_single_select_button_view = UILabel()
                lblTitlesingle_single_select_button_view.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitlesingle_single_select_button_view.textColor = UIColor.black
                lblTitlesingle_single_select_button_view.numberOfLines = 0
                lblTitlesingle_single_select_button_view.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitlesingle_single_select_button_view.text!, view: self.view) )
                
                yAxix = Double(CGFloat(yAxix) + lblTitlesingle_single_select_button_view.frame.size.height  + 15)
                
                self.tvForList.addSubview(lblTitlesingle_single_select_button_view)
                
                //----Button
                
                let aryorOption = dictQuestion.value(forKey: "options")as! NSArray
                
                if aryorOption.count != 0{
                    let dict = aryorOption.object(at: 0)as! NSDictionary
                    let aryOptionlist = dict.value(forKey: "options_list")as! NSArray
                    if aryOptionlist.count != 0{
                        var strWidth = CGFloat(10.0)
                        for (index1, item) in aryOptionlist.enumerated(){
                            let btn = UIButton()
                            btn.titleLabel?.font = UIFont(name: (btn.titleLabel?.font.fontName)!, size: 12.0)
                            btn.frame = CGRect(x: strWidth, y: CGFloat(yAxix), width:  self.view.frame.width / 3 - 10 , height: 30 )
                            strWidth =  strWidth + btn.frame.size.width + 5.0
                            btn.setTitle("\(item)", for: .normal)
                            btn.tag = index
                            btn.addTarget(self, action: #selector(pressButtonSinglButtonDown(_:)), for: .touchUpInside)
                            btn.backgroundColor = UIColor.white
                            btn.setTitleColor(UIColor.black, for: .normal)
                            makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 1.0)
                            if strAnswer == btn.titleLabel?.text!{
                                btn.backgroundColor = PrimeryTheamColor
                                btn.setTitleColor(UIColor.white, for: .normal)
                                makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 0.0)
                                
                            }
                            if ((index1 + 1) % 3 == 0){
                                yAxix = yAxix + Double(btn.frame.size.height + 10.0)
                                strWidth = CGFloat(10.0)
                            }
                            self.tvForList.addSubview(btn)
                        }
                        
                    }
                }
                
                //----Underline
                yAxix = Double(CGFloat(yAxix) + 40)

                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) + 5)
                
            }
                //MARK:***** Multi_Select_Drop_Down*****
                
            else if(strType == "multi_select_drop_down") && stroption_type == "0"{
                
            }
                
                //MARK:***** Multi_Select_Check_Box*****
            else if(strType == "multi_select_check_box") && stroption_type == "0"{
                
                //----Question
                
                let lblTitlemulti_select_check_box = UILabel()
                lblTitlemulti_select_check_box.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitlemulti_select_check_box.textColor = UIColor.black
                lblTitlemulti_select_check_box.numberOfLines = 0
                lblTitlemulti_select_check_box.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitlemulti_select_check_box.text!, view: self.view))
                yAxix = Double(CGFloat(yAxix) + lblTitlemulti_select_check_box.frame.size.height  + 20)
                self.tvForList.addSubview(lblTitlemulti_select_check_box)
                
                //CheckBox
                
                let aryorOption = dictQuestion.value(forKey: "options")as! NSArray
                if aryorOption.count != 0{
                    
                    let dict = aryorOption.object(at: 0)as! NSDictionary
                    let aryOptionlist = dict.value(forKey: "options_list")as! NSArray
                    if aryOptionlist.count != 0{
                        for item in aryOptionlist{
                            let lblTitile = UILabel()
                            let btnImg  = UIButton()
                            btnImg.tag = index
                            btnImg.addTarget(self, action: #selector(pressButtonMultiCheckBox(_:)), for: .touchDown)
                            lblTitile.textColor = UIColor.black
                            lblTitile.text = (item as AnyObject)as? String
                            btnImg.setTitle(lblTitile.text, for: .normal)
                            lblTitile.frame = CGRect(x: lblTitlemulti_select_check_box.frame.origin.x+30.0, y: CGFloat(yAxix), width: self.view.frame.width - 50 , height: 30)
                            btnImg.frame = CGRect(x:  lblTitlemulti_select_check_box.frame.origin.x, y: CGFloat(yAxix), width: 30 , height: 30)
                            btnImg.setImage(#imageLiteral(resourceName: "check_box_1"), for: .normal)
                            
                            if (strAnswer as String).count != 0 {
                                let myStringArr = strAnswer.components(separatedBy: "::")
                                btnImg.setImage(#imageLiteral(resourceName: "check_box_1"), for: .normal)
                                for item in myStringArr{
                                    if (btnImg.titleLabel?.text! ==  item){
                                        btnImg.setImage(#imageLiteral(resourceName: "check_box_2"), for: .normal)
                                    }
                                }
                            }
                            
                            
                            yAxix = yAxix + 40.0
                            self.tvForList.addSubview(lblTitile)
                            self.tvForList.addSubview(btnImg)
                        }
                    }
                }
                //---------UnderLine
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
            }
                
                //MARK:*****Yes_No_Radio_Button*****
                
            else if(strType == "yes_no_radio_button") && stroption_type == "0"{
                let lblTitleQuestionYesNo = UILabel()
                lblTitleQuestionYesNo.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitleQuestionYesNo.textColor = UIColor.black
                lblTitleQuestionYesNo.numberOfLines = 0
                lblTitleQuestionYesNo.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitleQuestionYesNo.text!, view: self.view) )
                yAxix = Double(CGFloat(yAxix) + lblTitleQuestionYesNo.frame.size.height  + 10)
                self.tvForList.addSubview(lblTitleQuestionYesNo)
                
                //----Button
                let aryorOption = dictQuestion.value(forKey: "options")as! NSArray
                
                if aryorOption.count != 0{
                    let dict = aryorOption.object(at: 0)as! NSDictionary
                    let aryOptionlist = dict.value(forKey: "options_list")as! NSArray
                    if aryOptionlist.count != 0{
                        var strWidth = CGFloat(10.0)
                        
                        for (index1, item) in aryOptionlist.enumerated() {
                            let btn = UIButton()
                            btn.frame = CGRect(x:strWidth, y: CGFloat(yAxix), width: self.view.frame.width / 4 - 10 , height: 30 )
                            strWidth =  strWidth + btn.frame.size.width + 5.0
                            btn.setTitle("\(item)", for: .normal)
                            btn.tag = index
                            btn.addTarget(self, action: #selector(pressButtonSinglButtonDown(_:)), for: .touchUpInside)
                            btn.backgroundColor = UIColor.white
                            btn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left;
                            btn.setTitleColor(UIColor.black, for: .normal)
                            btn.setImage(#imageLiteral(resourceName: "redio_1"), for: .normal)
                            if strAnswer == btn.titleLabel?.text!{
                                btn.setImage(#imageLiteral(resourceName: "redio_2"), for: .normal)
                            }
                            if ((index1 + 1) % 4 == 0){
                                yAxix = yAxix + Double(btn.frame.size.height + 10.0)
                            }
                            let spacing = 15; // the amount of spacing to appear between image and title
                            
                            let insetAmount = spacing / 2
                            btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(-insetAmount), bottom: 0, right: CGFloat(insetAmount))
                            btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(insetAmount), bottom: 0, right: CGFloat(-insetAmount))
                            btn.contentEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(insetAmount), bottom: 0, right: CGFloat(insetAmount))
                            
                            self.tvForList.addSubview(btn)
                        }
                        yAxix = yAxix + Double(40.0)
                        
                    }
                }
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width ), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
                
            }
                //MARK:*****Textbox View*****
                
            else if(strType == "textbox") && stroption_type == "0"{
                let lblTitleQuestionYesNo = UILabel()
                lblTitleQuestionYesNo.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitleQuestionYesNo.textColor = UIColor.black
                lblTitleQuestionYesNo.numberOfLines = 0
                lblTitleQuestionYesNo.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitleQuestionYesNo.text!, view: self.view) )
                yAxix = Double(CGFloat(yAxix) + lblTitleQuestionYesNo.frame.size.height  + 10)
                self.tvForList.addSubview(lblTitleQuestionYesNo)
                
                //----Text
                let txt = UITextField()
                txt.layer.cornerRadius = 4.0
                txt.layer.borderColor = UIColor.gray.cgColor
                txt.layer.borderWidth = 1.0
                txt.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 35)
                txt.delegate = self
                txt.tag = index
                txt.text = strAnswer
                txt.placeholder = ""
                yAxix = Double(CGFloat(yAxix) + txt.frame.size.height  + 10)
                self.tvForList.addSubview(txt)
                
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width ), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
                
            }
                
                //MARK:*****Yes No Range View*****
                
            else if(strType == "yes_no_range_view") && stroption_type == "0"{
                //--------Question
                let lblTitleQuestionSlider = UILabel()
                lblTitleQuestionSlider.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitleQuestionSlider.textColor = UIColor.black
                lblTitleQuestionSlider.numberOfLines = 0
                lblTitleQuestionSlider.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitleQuestionSlider.text!, view: self.view))
                yAxix = Double(CGFloat(yAxix) + lblTitleQuestionSlider.frame.size.height  + 20)
                self.tvForList.addSubview(lblTitleQuestionSlider)
                
                let dictOption = (dictQuestion.value(forKey: "options")as! NSArray).object(at: 0)as! NSDictionary
                
                var strWidth = CGFloat(10.0)
                for (_, item) in (dictOption.value(forKey: "options_list") as! NSArray).enumerated() {
                    let btn = UIButton()
                    btn.titleLabel?.font = UIFont(name: (btn.titleLabel?.font.fontName)!, size: 12.0)
                    btn.frame = CGRect(x:strWidth, y: CGFloat(yAxix), width: 80 , height: 30 )
                    strWidth =  strWidth + btn.frame.size.width + 5.0
                    btn.setTitle("\(item)", for: .normal)
                    btn.tag = index
                    btn.backgroundColor = UIColor.white
                    btn.setTitleColor(UIColor.black, for: .normal)
                    btn.addTarget(self, action: #selector(pressButtonYESSlider(_:)), for: .touchUpInside)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 1.0)
                    
                    let answerArr = strAnswer.components(separatedBy: "::")
                    if answerArr.count == 2{
                        if answerArr[0] == btn.titleLabel?.text!{
                            btn.backgroundColor = hexStringToUIColor(hex: "BD4701")
                            btn.setTitleColor(UIColor.white, for: .normal)
                            makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 0.0)
                        }
                    }
                    self.tvForList.addSubview(btn)
                }
                yAxix = Double(CGFloat(yAxix) + 40.0)
                
                //--------Range Image
                let imgRange = UIImageView()
                imgRange.image = #imageLiteral(resourceName: "scale")
                imgRange.contentMode = .scaleToFill
                imgRange.frame = CGRect(x: 15.0, y: CGFloat(yAxix), width: self.view.frame.width - 30 , height: 30 )
                yAxix = Double(CGFloat(yAxix) + imgRange.frame.size.height  + 15)
                self.tvForList.addSubview(imgRange)
                
                //--------Slider
                let sliderRange = UISlider()
                sliderRange.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 30 )
                sliderRange.thumbTintColor = hexStringToUIColor(hex: "D75F00")
                sliderRange.minimumTrackTintColor = hexStringToUIColor(hex: "D75F00")
                sliderRange.maximumTrackTintColor = UIColor.lightGray
                yAxix = Double(CGFloat(yAxix) + sliderRange.frame.size.height  + 20)
                sliderRange.maximumValue = 10.0
                sliderRange.minimumValue = 0.0
                sliderRange.tag = index
                  
                sliderRange.addTarget(self, action: #selector(pressYesNoSliderChange(slider:event:)), for: .valueChanged)
                if strAnswer.count != 0{
                    let myStringArr = strAnswer.components(separatedBy: "::")
                    sliderRange.value = Float("\(myStringArr[1])")!
                }else{
                    sliderRange.value = 0.0
                }
                
                self.tvForList.addSubview(sliderRange)
                let lbl = UILabel()
                let trackRect: CGRect = sliderRange.trackRect(forBounds: sliderRange.bounds)
                let thumbRect: CGRect = sliderRange.thumbRect(forBounds: sliderRange.bounds, trackRect: trackRect, value: sliderRange.value)
                lbl.frame = CGRect(x: thumbRect.origin.x + sliderRange.frame.origin.x - 12.0 , y: sliderRange.frame.origin.y + 30, width: 60.0, height: 20.0)
                lbl.textColor = hexStringToUIColor(hex: "BD4701")
                lbl.text = "\(sliderRange.value)"
                lbl.font = UIFont(name: lbl.font.fontName, size: 12.0)
                lbl.textAlignment = .center
                self.tvForList.addSubview(lbl)
                yAxix = Double(CGFloat(yAxix) + 10.0)
                //---------emojiImage
                let emojiImage = UIImageView()
                emojiImage.image = #imageLiteral(resourceName: "smiles")
                emojiImage.contentMode = .scaleAspectFill
                
                emojiImage.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 70 )
                //    yAxix = Double(CGFloat(yAxix) + emojiImage.frame.size.height  + 20)
                //   self.tvForList.addSubview(emojiImage)
                
                //---------UnderLine
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
            }
                
                
                
                
                
                
                //MARK:***** SliderView*****
                
            else if(strType == "") && stroption_type == "1"{
                
                //--------Question
                
                let lblTitleQuestionSlider = UILabel()
                lblTitleQuestionSlider.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitleQuestionSlider.textColor = UIColor.black
                lblTitleQuestionSlider.numberOfLines = 0
                lblTitleQuestionSlider.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitleQuestionSlider.text!, view: self.view))
                yAxix = Double(CGFloat(yAxix) + lblTitleQuestionSlider.frame.size.height  + 20)
                self.tvForList.addSubview(lblTitleQuestionSlider)
                
                //--------Range Image
                let imgRange = UIImageView()
                imgRange.image = #imageLiteral(resourceName: "scale")
                imgRange.contentMode = .scaleToFill
                
                imgRange.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 30 )
                yAxix = Double(CGFloat(yAxix) + imgRange.frame.size.height  + 15)
                
                self.tvForList.addSubview(imgRange)
                
                //lbl for no pain Worst PAin
                
                let lblNoPain = UILabel()
                lblNoPain.text = Localization("PostQuestion_NoPain")
                lblNoPain.textColor = hexStringToUIColor(hex: "D75F00")
                lblNoPain.numberOfLines = 1
                lblNoPain.textAlignment = .left
                lblNoPain.font = UIFont(name: (lblNoPain.font.fontName), size: 12.0)
                
                lblNoPain.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: 100 , height: 20)
                self.tvForList.addSubview(lblNoPain)
                
                let lblWorstPain = UILabel()
                lblWorstPain.text = Localization("PostQuestion_WorstPain")
                lblWorstPain.textColor = hexStringToUIColor(hex: "D75F00")
                lblWorstPain.textAlignment = .right
                lblWorstPain.numberOfLines = 1
                lblWorstPain.font = UIFont(name: (lblWorstPain.font.fontName), size: 12.0)
                
                lblWorstPain.frame = CGRect(x:self.view.frame.width - 160, y: CGFloat(yAxix), width:150 , height: 20)
                yAxix = Double(CGFloat(yAxix) + lblWorstPain.frame.size.height  + 5)
                self.tvForList.addSubview(lblWorstPain)
                
                //--------Slider
                let sliderRange = UISlider()
                sliderRange.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 30 )
                sliderRange.thumbTintColor = hexStringToUIColor(hex: "D75F00")
                sliderRange.minimumTrackTintColor = hexStringToUIColor(hex: "D75F00")
                sliderRange.maximumTrackTintColor = UIColor.lightGray
                yAxix = Double(CGFloat(yAxix) + sliderRange.frame.size.height  + 20)
                sliderRange.maximumValue = 10.0
                sliderRange.minimumValue = 0.0
                
                sliderRange.tag = index
                  sliderRange.addTarget(self, action: #selector(pressSliderChange(slider:event:)), for: .touchUpInside)
                if strAnswer.count != 0{
                    let myStringArr = strAnswer.components(separatedBy: "::")
                    sliderRange.value = Float("\(myStringArr[0])")!
                    
                }else{
                    sliderRange.value = 0.0
                }
                
                self.tvForList.addSubview(sliderRange)
                
                let lbl = UILabel()
                let trackRect: CGRect = sliderRange.trackRect(forBounds: sliderRange.bounds)
                let thumbRect: CGRect = sliderRange.thumbRect(forBounds: sliderRange.bounds, trackRect: trackRect, value: sliderRange.value)
                lbl.frame = CGRect(x: thumbRect.origin.x + sliderRange.frame.origin.x - 12.0 , y: sliderRange.frame.origin.y + 30, width: 60.0, height: 20.0)
                lbl.textColor = PrimeryTheamColor
                lbl.text = "\(sliderRange.value)"
                lbl.font = UIFont(name: lbl.font.fontName, size: 12.0)
                lbl.textAlignment = .center
                self.tvForList.addSubview(lbl)
                yAxix = Double(CGFloat(yAxix) + 30.0)
                
                //--------Range 2  Image
                let lblExtra = UILabel()
                lblExtra.text = Localization("Pre-Treatment Analysis1_extraquestion")
                lblExtra.textColor = UIColor.black
                lblExtra.numberOfLines = 0
                lblExtra.frame = CGRect(x: 15.0, y: CGFloat(yAxix), width: self.view.frame.width - 30 , height: 30 )
                yAxix = Double(CGFloat(yAxix) + lblExtra.frame.size.height  + 15)
                self.tvForList.addSubview(lblExtra)
                
                let aryExtraButton = NSMutableArray()
                aryExtraButton.add(Localization("PostQuestion_None"))
                aryExtraButton.add(Localization("PostQuestion_Mild"))
                aryExtraButton.add(Localization("PostQuestion_Moderate"))
                aryExtraButton.add(Localization("PostQuestion_Severe"))
                var strWidth = CGFloat(10.0)
                for (_, item) in aryExtraButton.enumerated() {
                    let btn = UIButton()
                    btn.titleLabel?.font = UIFont(name: (btn.titleLabel?.font.fontName)!, size: 12.0)
                    btn.frame = CGRect(x:strWidth, y: CGFloat(yAxix), width: self.view.frame.width / 4 - 10 , height: 30 )
                    strWidth =  strWidth + btn.frame.size.width + 5.0
                    btn.setTitle("\(item)", for: .normal)
                    btn.tag = index
                    btn.backgroundColor = UIColor.white
                    btn.setTitleColor(UIColor.black, for: .normal)
                    btn.addTarget(self, action: #selector(pressExtraButton(_:)), for: .touchUpInside)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 1.0)
                    let answerArr = strAnswer.components(separatedBy: "::")
                    if answerArr.count == 2{
                        if answerArr[1] == btn.titleLabel?.text!{
                            btn.backgroundColor = hexStringToUIColor(hex: "BD4701")
                            btn.setTitleColor(UIColor.white, for: .normal)
                            makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 0.0)
                        }
                        
                    }
                    self.tvForList.addSubview(btn)
                }
                yAxix = Double(CGFloat(yAxix) + 40.0)
                
                //---------emojiImage
                let emojiImage = UIImageView()
                emojiImage.image = #imageLiteral(resourceName: "smiles")
                emojiImage.contentMode = .scaleAspectFill
                
                emojiImage.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 70 )
                //  yAxix = Double(CGFloat(yAxix) + emojiImage.frame.size.height  + 20)
                //  self.tvForList.addSubview(emojiImage)
                
                //---------UnderLine
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
            }
                
                
                //MARK:*****  DropDown*****
                
            else if(strType == "") && stroption_type == "2"{
                
                let multiple_choice = dictQuestion.value(forKey: "multiple_choice")as! NSArray
                if multiple_choice.count != 0{
                    let strType = (multiple_choice.object(at: 0)as! NSDictionary).value(forKey: "selected_option_type")as! String
                    
                    //MARK:--DropDown
                    if(strType == "single_select_drop_down"){
                        //---Question
                        let lblTitlesingle_single_select_drop_down = UILabel()
                        lblTitlesingle_single_select_drop_down.text = "\(dictQuestion.value(forKey: "title")!)"
                        lblTitlesingle_single_select_drop_down.textColor = UIColor.black
                        lblTitlesingle_single_select_drop_down.numberOfLines = 0
                        lblTitlesingle_single_select_drop_down.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitlesingle_single_select_drop_down.text!, view: self.view) )
                        
                        yAxix = Double(CGFloat(yAxix) + lblTitlesingle_single_select_drop_down.frame.size.height  + 05)
                        
                        self.tvForList.addSubview(lblTitlesingle_single_select_drop_down)
                        
                        //----Label
                        
                        let lbl1 = UILabel()
                        let lbl2 = UILabel()
                        let lbl3 = UILabel()
                        
                        lbl1.textColor = UIColor.black
                        lbl2.textColor = UIColor.black
                        lbl3.textColor = UIColor.black
                        lbl1.numberOfLines = 0
                        lbl2.numberOfLines = 0
                        lbl3.numberOfLines = 0
                        
                        lbl1.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width / 3 - 10 , height: 30 )
                        lbl2.frame = CGRect(x: lbl1.frame.maxX + 5, y: CGFloat(yAxix), width: self.view.frame.width / 3 - 10 , height: 30 )
                        lbl3.frame = CGRect(x: lbl2.frame.maxX + 5, y: CGFloat(yAxix), width: self.view.frame.width / 3 - 10 , height: 30 )
                        
                        lbl1.font = UIFont(name: (lbl1.font.fontName), size: 12.0)
                        lbl2.font = UIFont(name: (lbl1.font.fontName), size: 12.0)
                        lbl3.font = UIFont(name: (lbl1.font.fontName), size: 12.0)
                        
                        if multiple_choice.count == 3{
                            lbl1.text = "\((multiple_choice.object(at: 0)as!NSDictionary).value(forKey: "title")!)"
                            lbl2.text = "\((multiple_choice.object(at: 1)as!NSDictionary).value(forKey: "title")!)"
                            lbl3.text = "\((multiple_choice.object(at: 2)as!NSDictionary).value(forKey: "title")!)"
                        }
                        yAxix = Double(CGFloat(yAxix) + lbl1.frame.size.height )
                        
                        self.tvForList.addSubview(lbl1)
                        self.tvForList.addSubview(lbl2)
                        self.tvForList.addSubview(lbl3)
                        
                        //----Button
                        
                        let btn1 = UIButton()
                        let btn2 = UIButton()
                        let btn3 = UIButton()
                        
                        let lbltitle1 = UILabel()
                        let lbltitle2 = UILabel()
                        let lbltitle3 = UILabel()
                        
                        btn1.setBackgroundImage(#imageLiteral(resourceName: "dropdown_1"), for: .normal)
                        btn2.setBackgroundImage(#imageLiteral(resourceName: "dropdown_1"), for: .normal)
                        btn3.setBackgroundImage(#imageLiteral(resourceName: "dropdown_1"), for: .normal)
                        
                        btn1.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width / 3 - 10 , height: 30 )
                        btn2.frame = CGRect(x: btn1.frame.origin.x + btn1.frame.size.width + 5.0 , y: CGFloat(yAxix), width: self.view.frame.width / 3 - 10 , height: 30 )
                        btn3.frame = CGRect(x: btn2.frame.origin.x + btn2.frame.size.width + 5.0, y: CGFloat(yAxix), width: self.view.frame.width / 3 - 10 , height: 30 )
                        
                        lbltitle1.frame = CGRect(x: 15.0, y: CGFloat(yAxix), width: self.view.frame.width / 3 - 20 , height: 30 )
                        lbltitle2.frame = CGRect(x: btn1.frame.origin.x + btn1.frame.size.width + 10.0 , y: CGFloat(yAxix), width: self.view.frame.width / 3 - 20 , height: 30 )
                        lbltitle3.frame = CGRect(x: btn2.frame.origin.x + btn2.frame.size.width + 10.0, y: CGFloat(yAxix), width: self.view.frame.width / 3 - 20 , height: 30 )
                        
                        lbltitle1.font = UIFont(name: (lbl1.font.fontName), size: 12.0)
                        lbltitle2.font = UIFont(name: (lbl1.font.fontName), size: 12.0)
                        lbltitle3.font = UIFont(name: (lbl1.font.fontName), size: 12.0)
                        
                        btn1.tag = Int("1\(index)")!
                        btn2.tag = Int("2\(index)")!
                        btn3.tag = Int("3\(index)")!
                        
                        btn1.addTarget(self, action: #selector(pressButtonSinglDropDown(_:)), for: .touchUpInside)
                        btn2.addTarget(self, action: #selector(pressButtonSinglDropDown(_:)), for: .touchUpInside)
                        btn3.addTarget(self, action: #selector(pressButtonSinglDropDown(_:)), for: .touchUpInside)
                        
                        if strAnswer == ""{
                            lbltitle1.text = Localization("Language_Select")
                            lbltitle2.text = Localization("Language_Select")
                            lbltitle3.text = Localization("Language_Select")
                        }
                        else{
                            var strValue1 = ""
                            var strValue2 = ""
                            var strValue3 = ""
                            
                            if (strAnswer as String).count != 0 {
                                var myStringArr = strAnswer.components(separatedBy: "::")
                                strValue1 = myStringArr [0]
                                strValue2 = myStringArr [1]
                                strValue3 = myStringArr [2]
                            }
                            lbltitle1.text = strValue1
                            lbltitle2.text = strValue2
                            lbltitle3.text = strValue3
                            lbltitle1.textColor = UIColor.black
                            lbltitle2.textColor = UIColor.black
                            lbltitle3.textColor = UIColor.black
                            
                            if strValue1 == "" {
                                lbltitle1.text = Localization("Language_Select")
                                lbltitle1.textColor = UIColor.gray
                            }
                            if strValue2 == "" {
                                lbltitle2.text = Localization("Language_Select")
                                lbltitle2.textColor = UIColor.gray
                            }
                            if strValue3 == "" {
                                lbltitle3.text = Localization("Language_Select")
                                lbltitle3.textColor = UIColor.gray
                            }
                        }
                        yAxix = Double(CGFloat(yAxix) + btn1.frame.size.height  + 25)
                        self.tvForList.addSubview(btn1)
                        self.tvForList.addSubview(btn2)
                        self.tvForList.addSubview(btn3)
                        self.tvForList.addSubview(lbltitle1)
                        self.tvForList.addSubview(lbltitle2)
                        self.tvForList.addSubview(lbltitle3)
                        //----Underline
                        self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                        yAxix = Double(CGFloat(yAxix) + 5)
                    }
                        
                        
                        //MARK:***** Multi _Single_Select_Button_View*****
                    else if (strType == "single_select_button_view"){
                        
                        //-----Question
                        let lblTitlesingle_select_button_view = UILabel()
                        
                        lblTitlesingle_select_button_view.text = "\(dictQuestion.value(forKey: "title")!)"
                        lblTitlesingle_select_button_view.textColor = UIColor.black
                        lblTitlesingle_select_button_view.numberOfLines = 0
                        lblTitlesingle_select_button_view.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height:  estimatedHeightOfLabel(text: lblTitlesingle_select_button_view.text!, view: self.view))
                        yAxix = Double(CGFloat(yAxix) + lblTitlesingle_select_button_view.frame.size.height  + 15)
                        tvForList.addSubview(lblTitlesingle_select_button_view)
                        
                        
                        
                        
                        let ary_multiple_choice = dictQuestion.value(forKey: "multiple_choice")as! NSArray
                        for j in 0 ..< ary_multiple_choice.count {
                            
                            let dict = (ary_multiple_choice.object(at: j))as! NSDictionary
                            let lblTitle = UILabel()
                            
                            lblTitle.text = "\(dict.value(forKey: "title")!)"
                            lblTitle.textColor = UIColor.black
                            lblTitle.numberOfLines = 0
                            lblTitle.frame = CGRect(x: 15.0, y: CGFloat(yAxix), width: self.view.frame.width - 30 , height:  estimatedHeightOfLabel(text: lblTitle.text!, view: self.view))
                            yAxix = Double(CGFloat(yAxix) + lblTitle.frame.size.height  + 15)
                            
                            tvForList.addSubview(lblTitle)
                            
                            //----Button
                            
                            let aryMulti = dictData.value(forKey: "Multianswer")as! NSArray
                            let question_id = dict.value(forKey: "id")as! NSString
                            var answer = ""
                            for j in 0 ..< aryMulti.count {
                                
                                if question_id as String == (aryMulti.object(at: j)as! NSDictionary).value(forKey: "question_id")as! String{
                                    
                                    answer = (aryMulti.object(at: j)as! NSDictionary).value(forKey: "option_title")as! String
                                }
                            }
                            
                            let aryorOption = dict.value(forKey: "options_multiple")as! NSArray
                            if aryorOption.count != 0{
                                
                                let dict = aryorOption.object(at: 0)as! NSDictionary
                                let aryOptionlist = dict.value(forKey: "options_multi_list")as! NSArray
                                var strWidth = CGFloat(10.0)
                                
                                for (index1, item) in aryOptionlist.enumerated(){
                                    
                                    let btn = UIButton()
                                    btn.titleLabel?.tag = j
                                    btn.titleLabel?.font = UIFont(name: (btn.titleLabel?.font.fontName)!, size: 12.0)
                                    btn.frame = CGRect(x: strWidth, y: CGFloat(yAxix), width: self.view.frame.width / 3 - 10 , height: 30 )
                                    strWidth =  strWidth + btn.frame.size.width + 5.0
                                    
                                    btn.setTitle("\(item)", for: .normal)
                                    btn.tag = index
                                    btn.addTarget(self, action: #selector(pressButtonMultiButton(_:)), for: .touchUpInside)
                                    btn.backgroundColor = UIColor.white
                                    btn.setTitleColor(UIColor.black, for: .normal)
                                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 1.0)
                                    if answer == btn.titleLabel?.text!{
                                        btn.backgroundColor = PrimeryTheamColor
                                        btn.setTitleColor(UIColor.white, for: .normal)
                                        makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 0.0)
                                        
                                    }
                                    if ((index1 + 1) % 3 == 0){
                                        yAxix = yAxix + Double(btn.frame.size.height + 10.0)
                                    }
                                    self.tvForList.addSubview(btn)
                                }
                                
                                
                            }
                            
                        }
                        //----Underline
                        self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                        yAxix = Double(CGFloat(yAxix) + 5)
                        
                    }
                    
                    
                    
                    
                    
                }
                
                
            }
            
            
            
        }
        DispatchQueue.main.async {
            self.tvForList.contentSize = CGSize(width: self.view.frame.size.width, height: CGFloat(yAxix + 100.0))
            
        }
        
    }
    
    
    //MARK:
    //MARK: --------------Action's For Table Cell Button's---------------
    
    //MARK:*****Yes_No_Button_Slider_View*****
    
     @objc func pressYesNoSliderChange(slider: UISlider, event: UIEvent){
         if let touchEvent = event.allTouches?.first {
                          switch touchEvent.phase {
                          case .began: break
                              // handle drag began
                          case .moved: break
                              // handle drag moved
                          case .ended:
                     let dict = aryForTvList.object(at: slider.tag)as! NSDictionary
                    let dictData = dict.mutableCopy()as! NSMutableDictionary
                    let roundedStepValue = round(slider.value / 0.5) * 0.5
                    slider.value = roundedStepValue
                    let twoDecimalPlaces = String(format: "%.1f", slider.value)
                    let answer = dictData.value(forKey: "answer")as! String
                    var strAnswer = ""
                    if (answer as String).count != 0 {
                        let myStringArr = answer.components(separatedBy: "::")
                        if myStringArr.count == 2{
                            strAnswer = "\(myStringArr[0])::\(twoDecimalPlaces)"
                        }else{
                             strAnswer = "::\(twoDecimalPlaces)"
                        }
                    }else{
                        strAnswer = "::\(twoDecimalPlaces)"
                    }
                    dictData.setValue("\(strAnswer)", forKey: "answer")
                    aryForTvList.replaceObject(at: slider.tag, with: dictData)
                    genrateDynemicView(aryData: aryForTvList)
             default:
             break
             }
         }
        
         
     }
    
    @objc func pressButtonYESSlider(_ button: UIButton) {
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        let answer = dictData.value(forKey: "answer")as! String
        var strAnswer = ""
        if (answer as String).count != 0 {
            let myStringArr = answer.components(separatedBy: "::")
            if myStringArr.count == 2{
                
                strAnswer = "\(String(describing: (button.titleLabel?.text!)!))::\(myStringArr[1])"
            }
            
        }else{
            strAnswer = "\(String(describing: (button.titleLabel?.text!)!))::0.0"
        }
        
        dictData.setValue(strAnswer, forKey: "answer")
        aryForTvList.replaceObject(at: button.tag, with: dictData)
        genrateDynemicView(aryData: aryForTvList)
        
    }
    
    //MARK:***** SliderView*****
    
     @objc func pressSliderChange(slider: UISlider, event: UIEvent)
             {
                 if let touchEvent = event.allTouches?.first {
                    switch touchEvent.phase {
                    case .began: break
                        // handle drag began
                    case .moved: break
                        // handle drag moved
                    case .ended:
                        
                     
                      let dict = aryForTvList.object(at: slider.tag)as! NSDictionary
                      let dictData = dict.mutableCopy()as! NSMutableDictionary
                      let roundedStepValue = round(slider.value / 0.5) * 0.5
                      slider.value = roundedStepValue

                      let twoDecimalPlaces = String(format: "%.1f", slider.value)
                      let answer = dictData.value(forKey: "answer")as! String
                      var strAnswer = ""
                          
                      if (answer as String).count != 0 {
                            let myStringArr = answer.components(separatedBy: "::")
                            if myStringArr.count == 2{
                                if(twoDecimalPlaces == "0.0"){
                                    strAnswer = "\(twoDecimalPlaces)::"
                                }else{
                                    strAnswer = "\(twoDecimalPlaces)::\(myStringArr[1])"
                                }
                            }else{
                                strAnswer = "\(twoDecimalPlaces)::"
                            }
                        }else{
                            strAnswer = "\(twoDecimalPlaces)::"
                        }
                      
                                 dictData.setValue("\(strAnswer)", forKey: "answer")
                                 self.aryForTvList.replaceObject(at: slider.tag, with: dictData)
                                 self.genrateDynemicView(aryData: self.aryForTvList)
                    default:
                        break
                    }
                }
      
         }
    @objc func pressExtraButton(_ button: UIButton) {
        
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        let answer = dictData.value(forKey: "answer")as! String
        var strAnswer = ""
        if (answer as String).count != 0 {
            let myStringArr = answer.components(separatedBy: "::")
            if myStringArr.count == 2{
                
                strAnswer = "\(myStringArr[0])::\(String(describing: (button.titleLabel?.text!)!))"
            }
            
        }else{
            strAnswer = "0.0::\(String(describing: (button.titleLabel?.text!)!))"
        }
        
        dictData.setValue(strAnswer, forKey: "answer")
        aryForTvList.replaceObject(at: button.tag, with: dictData)
        genrateDynemicView(aryData: aryForTvList)
        
    }
    //MARK:***** Yes_No_Button_View*****
    
    @objc func pressButtonYES(_ button: UIButton) {
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        let dictQuestion = dictData.value(forKey: "question")as! NSDictionary
        let strQuestionID = "\(dictQuestion.value(forKey: "serial_no")!)"
        
        
        if strQuestionID == "3"{
            strBackPainTag = 1
        }
            
            // For If No then Gone Section Question
        else if(strQuestionID == "1"){
            for item in aryForPreTre_NotRelatedTooth{
                let dictData = (item as AnyObject)as! NSDictionary
                let dictQuestion = dictData.value(forKey: "question")as! NSDictionary
                let strQuestionID = "\(dictQuestion.value(forKey: "serial_no")!)"
                if strQuestionID == "2"{
                    let dictData = (aryForPreTre_NotRelatedTooth.object(at: 1)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    dictData.setValue("", forKey: "answer")
                    dictData.setValue(NSMutableArray(), forKey: "Multianswer")
                    if aryForTvList.contains(dictData){
                        aryForTvList.replaceObject(at: 1, with: dictData)
                    }else{
                        aryForTvList.insert(dictData, at: 1)
                    }
                    
                }
            }
        }
        
        
        dictData.setValue(button.titleLabel?.text!, forKey: "answer")
        aryForTvList.replaceObject(at: button.tag, with: dictData)
        genrateDynemicView(aryData: aryForTvList)
        
        
    }
    
    @objc func pressButtonNO(_ button: UIButton) {
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        
        let dictQuestion = dictData.value(forKey: "question")as! NSDictionary
        let strQuestionID = "\(dictQuestion.value(forKey: "serial_no")!)"
        
        // For Body Part Pain  Question
        
        if strQuestionID == "3"{
            strBackPainTag = 2
        }
            
            // For If No then Gone Section Question
        else if(strQuestionID == "1"){
            
            for item in aryForTvList{
                let dictData = (item as AnyObject)as! NSDictionary
                let dictQuestion = dictData.value(forKey: "question")as! NSDictionary
                let strQuestionID = "\(dictQuestion.value(forKey: "serial_no")!)"
                if strQuestionID == "2"{
                    aryForTvList.removeObject(at: 1)
                }
            }
        }
        dictData.setValue(button.titleLabel?.text!, forKey: "answer")
        aryForTvList.replaceObject(at: button.tag, with: dictData)
        genrateDynemicView(aryData: aryForTvList)
        
    }
    
    //MARK:***** Single_Select_Drop_Down*****
    
    @objc func pressButtonDropDown(_ button: UIButton) {
        tagForDropDown = button.tag
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        let dictQuestion = dictData.value(forKey: "question")as! NSDictionary
        
        let dictOption = ((dictQuestion.value(forKey: "options")as! NSArray).object(at: 0)as! NSDictionary)
        if (dictOption.value(forKey: "options_list") as! NSArray).count != 0{
            let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.handlePreTretmentQuestion1View = self
            vc.arytbl =  (dictOption.value(forKey: "options_list") as! NSArray).mutableCopy()as! NSMutableArray
            //vc.arytbl = aryselectedBodyParts
            vc.strTag = 24
            vc.strTitle =  Localization("Language_Select")
            self.present(vc, animated: false, completion: {})
            
        }
        
    }
    
    //MARK:***** Single_Select_Button_View*****
    
    @objc func pressButtonSinglButtonDown(_ button: UIButton) {
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        dictData.setValue(button.titleLabel?.text!, forKey: "answer")
        aryForTvList.replaceObject(at: button.tag, with: dictData)
        genrateDynemicView(aryData: aryForTvList)
        
    }
    
    //MARK:*****DropDown*****
    @objc func pressButtonSinglDropDown(_ button: UIButton) {
        let strTAg = "\(button.tag)"
        let BtnTag = Int("\(strTAg.first!)")
        let aryIndex = Int("\(strTAg.dropFirst())")
        tagForDropDown = aryIndex!
        let dict = aryForTvList.object(at: aryIndex!)as! NSDictionary
        
        var arySendData = NSMutableArray()
        
        let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
        
        
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        let dictQuestion = dictData.value(forKey: "question")as! NSDictionary
        let arymultiple_choice = (dictQuestion.value(forKey: "multiple_choice")as! NSArray)
        
        //Days
        if BtnTag == 1 {
            vc.strTag = 8
            
            if (arymultiple_choice.count != 0){
                
                let dict = arymultiple_choice.object(at: 0)as! NSDictionary
                vc.strTitle =  dict.value(forKey: "title")as! String
                
                let ary_options_multiple = dict.value(forKey: "options_multiple")as! NSArray
                if (ary_options_multiple.count != 0){
                    let aryFinal = (ary_options_multiple.object(at: 0)as! NSDictionary).value(forKey: "options_multi_list")as! NSArray
                    arySendData = aryFinal.mutableCopy()as! NSMutableArray
                }
                
            }
        }
            //Months
        else if(BtnTag == 2){
            vc.strTag = 9
            if (arymultiple_choice.count != 0){
                
                let dict = arymultiple_choice.object(at: 1)as! NSDictionary
                vc.strTitle =  dict.value(forKey: "title")as! String
                
                let ary_options_multiple = dict.value(forKey: "options_multiple")as! NSArray
                if (ary_options_multiple.count != 0){
                    let aryFinal = (ary_options_multiple.object(at: 0)as! NSDictionary).value(forKey: "options_multi_list")as! NSArray
                    arySendData = aryFinal.mutableCopy()as! NSMutableArray
                }
                
            }
            
        }
            //Year
        else if(BtnTag == 3){
            vc.strTag = 10
            if (arymultiple_choice.count != 0){
                
                let dict = arymultiple_choice.object(at: 2)as! NSDictionary
                vc.strTitle =  dict.value(forKey: "title")as! String
                
                let ary_options_multiple = dict.value(forKey: "options_multiple")as! NSArray
                if (ary_options_multiple.count != 0){
                    let aryFinal = (ary_options_multiple.object(at: 0)as! NSDictionary).value(forKey: "options_multi_list")as! NSArray
                    arySendData = aryFinal.mutableCopy()as! NSMutableArray
                }
                
            }
            
        }
        if arySendData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.handlePreTretmentQuestion1View = self
            vc.arytbl = arySendData
            self.present(vc, animated: false, completion: {})
            
        }
        
        
    }
    //MARK:***** Multi _Single_Select_Button_View*****
    
    @objc func pressButtonMultiButton(_ button: UIButton) {
        
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        dictData.setValue(button.titleLabel?.text!, forKey: "answer")
  
        //let strAnswer = dictData.value(forKey: "answer")as! NSString
        let aryMulti = (dictData.value(forKey: "Multianswer")as! NSArray).mutableCopy()as! NSMutableArray
        let dictQuestion = dictData.value(forKey: "question")as! NSDictionary
        let arymultiple_choice = (dictQuestion.value(forKey: "multiple_choice")as! NSArray)
        let question_id = (arymultiple_choice.object(at: (button.titleLabel?.tag)!)as! NSDictionary).value(forKey: "id")as! String
        let option_type = (arymultiple_choice.object(at:  (button.titleLabel?.tag)!)as! NSDictionary).value(forKey: "option_type")as! String
        let selected_option_type = (arymultiple_choice.object(at:  (button.titleLabel?.tag)!)as! NSDictionary).value(forKey: "selected_option_type")as! String
        let question_title = (arymultiple_choice.object(at:  (button.titleLabel?.tag)!)as! NSDictionary).value(forKey: "title")as! String
        let option_title = button.titleLabel?.text!
        
        let dict1  = NSMutableDictionary()
        dict1.setValue(question_id, forKey: "question_id")
        dict1.setValue(option_type, forKey: "option_type")
        dict1.setValue(selected_option_type, forKey: "selected_option_type")
        dict1.setValue(question_title, forKey: "question_title")
        dict1.setValue(option_title, forKey: "option_title")
        
        var temp  = ""
        var tempIndex  =  Int()
        
        for j in 0 ..< aryMulti.count {
            if question_id == (aryMulti.object(at: j)as! NSDictionary).value(forKey: "question_id")as! String{
                temp = "yes"
                tempIndex = j
            }
        }
        
        if temp == "yes"{
            aryMulti.replaceObject(at: tempIndex, with: dict1)
        }else{
            aryMulti.add(dict1)
        }

        dictData.setValue(button.titleLabel?.text!, forKey: "answer")
        dictData.setValue(aryMulti, forKey: "Multianswer")
        aryForTvList.replaceObject(at: button.tag, with: dictData)
        genrateDynemicView(aryData: aryForTvList)
        
    }
    //MARK:***** Single_Select_Redio_Button*****
    
    @objc func pressButtonRadio(_ button: UIButton) {
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        dictData.setValue(button.titleLabel?.text!, forKey: "answer")
        aryForTvList.replaceObject(at: button.tag, with: dictData)
        genrateDynemicView(aryData: aryForTvList)
    }
    //MARK:-- MultiCheckBox  VIEW
    @objc func pressButtonMultiCheckBox(_ button: UIButton) {
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        var answer = (dictData.value(forKey: "answer")as! NSString)
        let strCheck = "\(String(describing: (button.titleLabel?.text!)!))::"
        if answer.contains(strCheck) {
            let newString = answer.replacingOccurrences(of: "\(strCheck)", with:"")
            dictData.setValue(newString, forKey: "answer")
        }else{
            answer = "\(answer)\(strCheck)" as NSString
            dictData.setValue(answer, forKey: "answer")
            
        }
        
        aryForTvList.replaceObject(at: button.tag, with: dictData)
        genrateDynemicView(aryData: aryForTvList)
        
    }
    //MARK:
    //MARK: ---------------All IBAction-----------------
    
    @IBAction func actionOnContinue(_ sender: UIButton) {
        
        if aryForTvList.count != 0 {
            var temp = 0
            var indexTemp = 0
            
            for (index, item) in aryForTvList.enumerated() {
                let dictData = ((item as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                if (dictData.value(forKey: "answer")as! String).count  == 0{
                    temp = 1
                    indexTemp = index + 1
                    break
                }
            }
            
            if temp == 1{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage:"\(Localization("UTHC_QuestionAlert"))\(indexTemp) \(Localization("UTHC_QuestionAlert1"))" , viewcontrol: self)
            }else{
                
                let dictData1 = NSMutableDictionary()
                for item in aryForTvList{
                    let dictData = ((item as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    let questionDict = dictData.value(forKey: "question")as! NSDictionary
                    let aryoptions = questionDict.value(forKey: "options")as! NSArray
                    var dictOptionList = NSDictionary()
                    if (aryoptions.count != 0){
                        dictOptionList = aryoptions.object(at: 0)as! NSDictionary
                    }
                    
                    let option_type = "\(questionDict.value(forKey: "option_type")!)"
                    let question_id = "\(questionDict.value(forKey: "id")!)"
                    let question_title = "\(questionDict.value(forKey: "title")!)"
                    
                    let question_option_id = "\(dictOptionList.value(forKey: "id")!)"
                    let question_option_title = "\(dictData.value(forKey: "answer")as! String)"
                    let selected_option_type = "\(questionDict.value(forKey: "selected_option_type")!)"
                    
                    let dictSendData = NSMutableDictionary()
                    dictSendData.setValue(question_id, forKey: "question_id")
                    dictSendData.setValue(question_title, forKey: "question_title")
                    dictSendData.setValue(question_option_id, forKey: "question_option_id")
                    dictSendData.setValue(selected_option_type, forKey: "selected_option_type")
                    dictSendData.setValue(option_type, forKey: "option_type")
                    
                    let aryMulti = (dictData.value(forKey: "Multianswer")as! NSArray).mutableCopy()as! NSMutableArray
                    if (aryMulti.count != 0 ) {
                        dictSendData.setValue(aryMulti, forKey: "multiple")
                    }else{
                        dictSendData.setValue(question_option_title, forKey: "question_option_title")
                    }
                    dictData1.setValue(dictSendData, forKey:question_id)
                }
                self.API_Call_Save_PreTretmentQuestions(aryData: dictData1)
           }
        }
    }
}


//MARK:
//MARK: -----------Text Filed Delegate Method----------

extension PreTreatMentNotRelatedTooth_1VC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == " " {
            return false
        }
        if string == "" {
            return true
        }
        if (textField.text!.count > 25) {
            return false
        }
        return true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let dict = aryForTvList.object(at:textField.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        dictData.setValue(textField.text, forKey: "answer")
        aryForTvList.replaceObject(at: textField.tag, with: dictData)
        self.genrateDynemicView(aryData: aryForTvList)
    }
    
}


//MARK:-
//MARK:- ----------Refresh Delegate Methods----------

extension PreTreatMentNotRelatedTooth_1VC: refreshPre_TretmentQuestion1_Data {
    func refreshview(str: String, tag: Int) {
        
        if tag == 24 {
            let dict = aryForTvList.object(at: tagForDropDown)as! NSDictionary
            let dictData = dict.mutableCopy()as! NSMutableDictionary
            dictData.setValue(str, forKey: "answer")
            aryForTvList.replaceObject(at: tagForDropDown, with: dictData)
            self.genrateDynemicView(aryData: aryForTvList)
            
        }else{
            let dict = aryForTvList.object(at:tagForDropDown)as! NSDictionary
            let dictData = dict.mutableCopy()as! NSMutableDictionary
            let strAnswer = dictData.value(forKey: "answer")as! NSString
            let aryMulti = (dictData.value(forKey: "Multianswer")as! NSArray).mutableCopy()as! NSMutableArray
            var strValue1 = ""
            var strValue2 = ""
            var strValue3 = ""
            
            if (strAnswer as String).count != 0 {
                var myStringArr = strAnswer.components(separatedBy: "::")
                strValue1 = myStringArr [0]
                strValue2 = myStringArr [1]
                strValue3 = myStringArr [2]
            }
            let dictQuestion = dictData.value(forKey: "question")as! NSDictionary
            let arymultiple_choice = (dictQuestion.value(forKey: "multiple_choice")as! NSArray)
            if tag == 8 {
                strValue1 =  str
                let question_id = (arymultiple_choice.object(at: 0)as! NSDictionary).value(forKey: "id")as! String
                let option_type = (arymultiple_choice.object(at: 0)as! NSDictionary).value(forKey: "option_type")as! String
                let selected_option_type = (arymultiple_choice.object(at: 0)as! NSDictionary).value(forKey: "selected_option_type")as! String
                let question_title = (arymultiple_choice.object(at: 0)as! NSDictionary).value(forKey: "title")as! String
                let option_title = str
                
                let dict  = NSMutableDictionary()
                dict.setValue(question_id, forKey: "question_id")
                dict.setValue(option_type, forKey: "option_type")
                dict.setValue(selected_option_type, forKey: "selected_option_type")
                dict.setValue(question_title, forKey: "question_title")
                dict.setValue(option_title, forKey: "option_title")
                
                var temp  = ""
                var tempIndex  =  Int()
                
                for j in 0 ..< aryMulti.count {
                    if question_id == (aryMulti.object(at: j)as! NSDictionary).value(forKey: "question_id")as! String{
                        temp = "yes"
                        tempIndex = j
                    }
                }
                if temp == "yes"{
                    aryMulti.replaceObject(at: tempIndex, with: dict)
                }else{
                    aryMulti.add(dict)
                }
            }else if(tag == 9){
                strValue2 =  str
                let question_id = (arymultiple_choice.object(at: 1)as! NSDictionary).value(forKey: "id")as! String
                let option_type = (arymultiple_choice.object(at: 1)as! NSDictionary).value(forKey: "option_type")as! String
                let selected_option_type = (arymultiple_choice.object(at: 1)as! NSDictionary).value(forKey: "selected_option_type")as! String
                let question_title = (arymultiple_choice.object(at: 1)as! NSDictionary).value(forKey: "title")as! String
                let option_title = str
                let dict  = NSMutableDictionary()
                dict.setValue(question_id, forKey: "question_id")
                dict.setValue(option_type, forKey: "option_type")
                dict.setValue(selected_option_type, forKey: "selected_option_type")
                dict.setValue(question_title, forKey: "question_title")
                dict.setValue(option_title, forKey: "option_title")
                var temp  = ""
                var tempIndex  =  Int()
                
                for j in 0 ..< aryMulti.count {
                    if question_id == (aryMulti.object(at: j)as! NSDictionary).value(forKey: "question_id")as! String{
                        temp = "yes"
                        tempIndex = j
                    }
                }
                if temp == "yes"{
                    aryMulti.replaceObject(at: tempIndex, with: dict)
                }else{
                    aryMulti.add(dict)
                }
            }else if(tag == 10){
                strValue3 =  str
                let question_id = (arymultiple_choice.object(at: 2)as! NSDictionary).value(forKey: "id")as! String
                let option_type = (arymultiple_choice.object(at: 2)as! NSDictionary).value(forKey: "option_type")as! String
                let selected_option_type = (arymultiple_choice.object(at: 2)as! NSDictionary).value(forKey: "selected_option_type")as! String
                let question_title = (arymultiple_choice.object(at: 2)as! NSDictionary).value(forKey: "title")as! String
                let option_title = str
                
                let dict  = NSMutableDictionary()
                dict.setValue(question_id, forKey: "question_id")
                dict.setValue(option_type, forKey: "option_type")
                dict.setValue(selected_option_type, forKey: "selected_option_type")
                dict.setValue(question_title, forKey: "question_title")
                dict.setValue(option_title, forKey: "option_title")
                var temp  = ""
                var tempIndex  =  Int()
                
                for j in 0 ..< aryMulti.count {
                    if question_id == (aryMulti.object(at: j)as! NSDictionary).value(forKey: "question_id")as! String{
                        temp = "yes"
                        tempIndex = j
                    }
                }
                if temp == "yes"{
                    aryMulti.replaceObject(at: tempIndex, with: dict)
                }else{
                    aryMulti.add(dict)
                }
            }
            let strFinelAnswer = "\(strValue1)::\(strValue2)::\(strValue3)"
            dictData.setValue(strFinelAnswer, forKey: "answer")
            dictData.setValue(aryMulti, forKey: "Multianswer")
            aryForTvList.replaceObject(at: tagForDropDown, with: dictData)
            genrateDynemicView(aryData: aryForTvList)
        }
    }
}



