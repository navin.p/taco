//
//  CommonTableVC.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 2/16/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

// tag == 1 ,3 (Language)
// tag == 2,25 (SignUP)


// tag == 5,6,7,23(Pre_TretmentRelatedTooth)
// tag == 8,9,10,24 (Pre_TretmentQuestion1)
// tag == 4,11,12,13 (Pre_TretmentQuestion2)

//tag == 20  For Post Question
//tag == 21  For wellness Question
//tag == 22  For Post Question

import UIKit

class CommonTableVC: UIViewController {
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var tvForList: UITableView!
    @IBOutlet weak var height_ForTBLVIEW: NSLayoutConstraint!
    @IBOutlet weak var viewFortv: CardView!
 
    @IBOutlet weak var viewDatePicker: UIView!
    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOK: UIButton!
    
    var strTitle = String()
    var arytbl = NSMutableArray()
    var strTag = Int()
    
    weak var handlelanguageView: refreshLanguage_Data?
    weak var handleSignUpView: refreshSignUp_Data?
    weak var handlePreTretmentQuestion1View: refreshPre_TretmentQuestion1_Data?
    weak var handlePreTretmentQuestion2View: refreshPre_TretmentQuestion2_Data?
    
    weak var handlePre_TretmentRelatedTooth: refreshPre_TretmentRelatedTooth?
    weak var handlePostQuestion: refreshPost_Notification?

    weak var handleWellness: refreshWellness?
    weak var handlePost: refreshPostQuestion?


    //MARK:-
    //MARK:- ----------Life Cycle----------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvForList.estimatedRowHeight = 50.0
        btnTitle.setTitle(strTitle, for: .normal)
        print(arytbl)
        tvForList.tableFooterView = UIView()
        picker.maximumDate = Date()
        if strTag == 9999 {  //For Picker
            viewDatePicker.isHidden = false
        }else{
            viewDatePicker.isHidden = true
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillLayoutSubviews() {
        
        if strTag == 9999 {  //For Picker
            height_ForTBLVIEW.constant = 300
        }
        // for tabel
        else{
            
            height_ForTBLVIEW.constant = CGFloat((arytbl.count * 40 ) + 50 )
            if  height_ForTBLVIEW.constant > self.view.frame.size.height - 150  {
                height_ForTBLVIEW.constant = self.view.frame.size.height - 150
            }
        }
        viewFortv.center = self.view.center
    }
    
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.dismiss(animated: false, completion: {})
    }
    
    
    @IBAction func actionOnButtonPicker(_ sender: UIButton) {
        if sender.tag == 1 { //For Close
            self.dismiss(animated: false, completion: {})
            
        }else{ //For OK
            DispatchQueue.main.async {
                let dict = NSMutableDictionary()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let dateString = dateFormatter.string(from: self.picker.date)
                dict.setValue(dateString, forKey: "date")
                self.handleSignUpView?.refreshSignUpview(dict: dict, tag: self.strTag)
                self.dismiss(animated: false, completion: {})
            }
            
        }
        
    }
    
    
}
//MARK:-
//MARK:- ----------UITableView Delegate Methods----------

extension CommonTableVC : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arytbl.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arytbl.object(at: indexPath.row)as? NSDictionary
        if self.strTag == 1 {
            DispatchQueue.main.async {
                self.handlelanguageView?.refreshlanguageview(dict: dict!, tag: self.strTag)
                self.dismiss(animated: false, completion: {})
            }
            
        }
        else if(self.strTag == 2  ){
            DispatchQueue.main.async {
                self.handleSignUpView?.refreshSignUpview(dict: dict!, tag: self.strTag)
                self.dismiss(animated: false, completion: {})
            }
        }
        else if(self.strTag == 25  ){
            DispatchQueue.main.async {
                self.handleSignUpView?.refreshSignUpviewWithString(str: "\(self.arytbl.object(at: indexPath.row))", tag: self.strTag)
                self.dismiss(animated: false, completion: {})
            }
        }
        else if(self.strTag == 3){
            DispatchQueue.main.async {
                self.handlelanguageView?.refreshlanguageview(dict: dict!, tag: self.strTag)
                self.dismiss(animated: false, completion: {})
            }
        }
        else if(self.strTag == 4 || self.strTag == 11 || self.strTag == 12 || self.strTag == 13){
            DispatchQueue.main.async {
                self.handlePreTretmentQuestion2View?.refreshview(str: "\(self.arytbl.object(at: indexPath.row))", tag: self.strTag)
                self.dismiss(animated: false, completion: {})
            }
        }
       else if strTag == 5 || strTag == 6 || strTag == 7 || strTag == 23 {
            DispatchQueue.main.async {
                self.handlePre_TretmentRelatedTooth?.refreshview(str: "\(self.arytbl.object(at: indexPath.row))", tag: self.strTag)
                self.dismiss(animated: false, completion: {})
            }
        }
        else if strTag == 8 || strTag == 9 || strTag == 10 || strTag == 24{
            DispatchQueue.main.async {
                self.handlePreTretmentQuestion1View?.refreshview(str: "\(self.arytbl.object(at: indexPath.row))", tag: self.strTag)
                self.dismiss(animated: false, completion: {})
            }
        }
        else if strTag == 20 {
            DispatchQueue.main.async {
                
                self.handlePostQuestion?.refreshview(str: "\(self.arytbl.object(at: indexPath.row))", tag: self.strTag)
                self.dismiss(animated: false, completion: {})
            }
        }
        else if strTag == 21 {
            DispatchQueue.main.async {
                self.handleWellness?.refreshwellnessview(str: "\(self.arytbl.object(at: indexPath.row))", tag: self.strTag)
                self.dismiss(animated: false, completion: {})
            }
        }
        else if strTag == 22 {
            DispatchQueue.main.async {
                self.handlePost?.refreshPostQuestionview(str: "\(self.arytbl.object(at: indexPath.row))", tag: self.strTag)
                self.dismiss(animated: false, completion: {})
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvForList.dequeueReusableCell(withIdentifier: "StudyIDCell", for: indexPath as IndexPath) as! CommonTableCellVC
        let dict = arytbl.object(at: indexPath.row)as? NSDictionary
        
        //--------For Language Screen
        //1. StudyID
        if strTag == 1 {
            cell.language_lbl_StudyID.text = (dict?.value(forKey: "study_id")as? String)!
        }
            //--------For Signup Screen
            //1. DOB
        else if strTag == 2  {
            cell.language_lbl_StudyID.text = (dict?.value(forKey: "date")as? String)!
        }
          //----PreTretmentQuestion1
        else if strTag == 3  {
            cell.language_lbl_StudyID.text = (dict?.value(forKey: "location_prefix")as? String)!
        }
            //----PreTretmentQuestion2
        else if strTag == 4  {
            cell.language_lbl_StudyID.text = "\(arytbl.object(at: indexPath.row))"
        }
            //----PreTretmentRelatedTooth
        else if strTag == 5 || strTag == 6  || strTag == 7 || strTag == 8 || strTag == 9 || strTag == 10 || strTag == 11 || strTag == 12 || strTag == 13 || strTag == 23 || strTag == 24 {
            cell.language_lbl_StudyID.text = "\(arytbl.object(at: indexPath.row))"
        }
            //----Post Question // wellness // Post
        else if strTag == 20   ||  strTag == 21 || strTag == 22 || strTag == 25{
            cell.language_lbl_StudyID.text = "\(arytbl.object(at: indexPath.row))"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
