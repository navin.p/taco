//
//  UniqueIDVC.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 2/19/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class UniqueIDVC: UIViewController {
  
    //MARK:
    //MARK: ---------------IBOutlet-----------------
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var lblStaticUniqueID: UILabel!
    @IBOutlet weak var lblID: UILabel!
   
    var strpatient_id = String()
    
    //MARK:
    //MARK: --------------LifeCycle----------------
    override func viewDidLoad() {
        super.viewDidLoad()
        lblID.text = strpatient_id;
        configureViewFromLocalisation()
        
       
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillLayoutSubviews() {
        btnContinue.layer.cornerRadius = 20.0
    }
    //MARK:
    //MARK: -------------IBAction----------------
    @IBAction func actionOnContinue(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
  
    //MARK:
    //MARK: ---------------Extra Function's-----------------
    func configureViewFromLocalisation() {
    
       lblStaticUniqueID.text = Localization("UniqueID_Title")
        btnContinue.setTitle(Localization("UniqueID_Continue"), for: .normal)
    }
}
