//
//  NotificationQuestionVC.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 3/23/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import MessageUI


//MARK: ---------------Protocol-----------------
protocol refreshPost_Notification: class{
    func refreshview(str : String ,tag : Int)
    func refreshviewWithData(dict : NSDictionary ,tag : Int)
    
}

class NotificationQuestionVC: UIViewController {
    
    
    //MARK:-
    //MARK:- ----------IBOutlet----------
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var tvForList: UITableView!
 
    
    var btnTransprant = UIButton()
    var  aryForTvList = NSMutableArray()
    var  aryForPost = NSMutableArray()
    var dictNotiData = NSMutableDictionary()
    var strComeFrom = String()

    var tagForDropDown = Int()
    //Temp
    var callStatus = String()
    var apiCallStatus = "call"
    //MARK:-
    //MARK:- ----------LifeCycle----------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnTransprant.backgroundColor = UIColor.black
        btnTransprant.alpha = 0.5
        configureViewFromLocalisation()
        if isInternetAvailable() {
            callAPI_GetNotificationQuestion()
        }else{
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_NetworkError"), viewcontrol: self)
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
    }
    
    
    //MARK:-
    //MARK:- ---------All Button Action's----------

    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        if strComeFrom == "PUSH_NOTIFICATION" {
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "DashBoardVC")
            appDelegate.window?.rootViewController = mainVcIntial
          
        }else{
            self.btnTransprant.removeFromSuperview()
            self.navigationController?.popViewController(animated: true)
        }
    }
    

    
    //MARK:-
    //MARK:- ---------Extra Function----------
    func configureViewFromLocalisation() {

     lblHeaderTitle.text = Localization("UTHC_AppName")
    }
    
    func callPhone() {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        var strNumber = ""
        let dictAdmin = nsud.value(forKey: "AdminInfo")as! NSDictionary
        if (Int(dateString)! > 8 && Int(dateString)! < 17 ){
            strNumber = (dictAdmin.value(forKey: "phone_number_8_to_5")as?String)!
        }else{
            strNumber = (dictAdmin.value(forKey: "phone_number_after_hours")as?String)!
        }
        callingFunction(number: strNumber)
        if self.strComeFrom == "PUSH_NOTIFICATION" {
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "DashBoardVC")
            appDelegate.window?.rootViewController = mainVcIntial
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func callEmail() {
        if !MFMailComposeViewController.canSendMail() {
       
            let alertController = UIAlertController(title: Localization("UTHC_Alert"), message: "Mail services are not available", preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("UTHC_Cancel"), style: UIAlertAction.Style.default) {
                UIAlertAction in
                if self.strComeFrom == "PUSH_NOTIFICATION" {
                    let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "DashBoardVC")
                    appDelegate.window?.rootViewController = mainVcIntial
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            }
            alertController.addAction(okAction)
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            return
        }
        if(nsud.value(forKey: "AdminInfo") != nil){
            let dictAdmin = nsud.value(forKey: "AdminInfo")as! NSDictionary
            var lblToEmail = ""
            if (dictAdmin.count != 0){
                lblToEmail = (dictAdmin.value(forKey: "email")as?String)!
            }
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([lblToEmail])
            mail.setMessageBody(Localization("Email_Emergency"), isHTML: true)
            self.present(mail, animated: true)
        }
    }
    
    func setUpDataForSend()  {
        var temp = 0
        var indexTemp = 0
        for (index, item) in aryForTvList.enumerated() {
            let dictData = ((item as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
            if (dictData.value(forKey: "answer")as! String).count  == 0{
                temp = 1
                indexTemp = index + 1
                break
            }
        }
        
        if temp == 1{
            showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage:"\(Localization("UTHC_QuestionAlert"))\(indexTemp) \(Localization("UTHC_QuestionAlert1"))" , viewcontrol: self)
        }else{
            let dictData1 = NSMutableDictionary()
            
            for item in aryForTvList{
                let dictData = ((item as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                
                
                let questionDict = dictData.value(forKey: "question")as! NSDictionary
                let aryoptions = questionDict.value(forKey: "options")as! NSArray
                var dictOptionList = NSDictionary()
                if (aryoptions.count != 0){
                    dictOptionList = aryoptions.object(at: 0)as! NSDictionary
                }
                
                let option_type = "\(questionDict.value(forKey: "option_type")!)"
                let question_id = "\(questionDict.value(forKey: "id")!)"
                let question_title = "\(questionDict.value(forKey: "title")!)"
                
                let question_option_id = "\(dictOptionList.value(forKey: "id")!)"
                let question_option_title = "\(dictData.value(forKey: "answer")as! String)"
                let selected_option_type = "\(questionDict.value(forKey: "selected_option_type")!)"
                
                if(selected_option_type == "") && option_type == "1"{
                    if question_option_title.count != 0{
                        let myStringArr = question_option_title.components(separatedBy: "::")
                        var strscal = "7"
                        if(nsud.value(forKey: "AdminInfo") != nil){
                            let dictAdmin = nsud.value(forKey: "AdminInfo")as! NSDictionary
                            if (dictAdmin.count != 0){
                                strscal = (dictAdmin.value(forKey: "pain_scale_value")as?String)!
                            }
                        }
                        if (Float(myStringArr[0])!) >= Float(strscal)!{ // for call available or not
                            callStatus = "YES"
                        }
                    }
                }else if(selected_option_type == "yes_no_range_view") && option_type == "0"{
                    
                    if question_option_title.count != 0{
                        let myStringArr = question_option_title.components(separatedBy: "::")
                        var strscal = "7"
                        if(nsud.value(forKey: "AdminInfo") != nil){
                            let dictAdmin = nsud.value(forKey: "AdminInfo")as! NSDictionary
                            if (dictAdmin.count != 0){
                                strscal = (dictAdmin.value(forKey: "pain_scale_value")as?String)!
                            }
                        }
                        if (Float(myStringArr[1])!) > Float(strscal)!{ // for call available or not
                            callStatus = "YES"
                        }
                    }
                    
                    
                }
                            
                            
                let dictSendData = NSMutableDictionary()
                dictSendData.setValue(question_id, forKey: "question_id")
                dictSendData.setValue(question_title, forKey: "question_title")
                dictSendData.setValue(question_option_id, forKey: "question_option_id")
                dictSendData.setValue(selected_option_type, forKey: "selected_option_type")
                dictSendData.setValue(option_type, forKey: "option_type")
                
                let aryMultiple = dictData.value(forKey: "Multianswer")as! NSArray
                if (aryMultiple.count != 0 ) {
                    dictSendData.setValue(aryMultiple, forKey: "multiple")
                }else{
                    dictSendData.setValue(question_option_title, forKey: "question_option_title")
                }
                
                dictData1.setValue(dictSendData, forKey:question_id)
            }
            // showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: "Under Development", viewcontrol: self)
            self.API_Call_Save_PostNotification(aryData: dictData1)
        }
    }
    
    //MARK:
    //MARK: ---------------API's Calling--------------
    
    func API_Call_Save_PostNotification(aryData : NSMutableDictionary)  {
        let dict = NSMutableDictionary.init()
        if nsud.value(forKey: "AppLanguage") != nil{
            if nsud.value(forKey: "AppLanguage")as! String == "English"{
                 dict.setValue("1", forKey: "language_preference")
            }else{
                dict.setValue("2", forKey: "language_preference")
            }
        }else{
            dict.setValue("1", forKey: "language_preference")
        }
        
        let dictLogInData = nsud.value(forKey: "LogInData")as! NSDictionary
        var strPtientID = "\(String(describing: dictLogInData.value(forKey: "patient_id")!))"
        let fullNameArr = strPtientID.components(separatedBy: "-")
        strPtientID = fullNameArr[2]
        
        dict.setValue(strPtientID, forKey: "patient_id")
        dict.setValue("1", forKey: "is_post_analysis_complete")
     
        if strComeFrom == "PUSH_NOTIFICATION" {
            //var dictData = NSDictionary()
           // dictData = (dictNotiData.value(forKey: "aps")as!NSDictionary).value(forKey: "alert") as! NSDictionary
            dict.setValue("\(dictNotiData.value(forKey: "frequency_id")!)", forKey: "frequency_id") //2
        }else{
            dict.setValue("\(dictNotiData.value(forKey: "id")!)", forKey: "frequency_id") //2
        }
   
        dict.setValue("\(dictLogInData.value(forKey: "location_id")!)", forKey: "location_id")//4
        print(dict)
        print(aryData)
        dict.setValue(jsontoString(fromobject: aryData), forKey: "question")
        
        showLoader(strMsg: "", style: .dark)
        // API_Save_Post_Notification
        WebService.callAPI(parameter: dict, url: API_Save_Post_Notification) { (responce, status) in
            print(responce)
            DispatchQueue.main.async {
                dismissLoader()
                
            }
            if(status == "Suceess"){
                if(responce.value(forKey: "error")as! String == "1"){
                    if (self.callStatus == "YES"){
                        let str = (Localization("PostQuestion_AlertWhenCall"))
                    
                        let alertController = UIAlertController(title: Localization("UTHC_Alert"), message: str, preferredStyle: .alert)
                        // Create the actions
                        let call = UIAlertAction(title: Localization("PostQuestion_Call"), style: UIAlertAction.Style.default) {
                            UIAlertAction in
                             //self.navigationController?.popViewController(animated: true)
                             self.callPhone()
                    }
                        let email = UIAlertAction(title: Localization("PostQuestion_Email"), style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            self.callEmail()
                            //self.navigationController?.popViewController(animated: true)
                        }
                        let okAction = UIAlertAction(title: Localization("UTHC_Cancel"), style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            if self.strComeFrom == "PUSH_NOTIFICATION" {
                                let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "DashBoardVC")
                                appDelegate.window?.rootViewController = mainVcIntial
                            }else{
                                self.navigationController?.popViewController(animated: true)
                            }
                            
                        }
                        // Add the actions
                        alertController.addAction(call)
                        alertController.addAction(email)
                        alertController.addAction(okAction)
                        // Present the controller
                        self.present(alertController, animated: true, completion: nil)
                    }else{
                        let str = (Localization("PostQuestion_AlertWhenNotCall"))
                        let alertController = UIAlertController(title: Localization("UTHC_Alert"), message: str, preferredStyle: .alert)
                        // Create the actions
                        let okAction = UIAlertAction(title: Localization("UTHC_Ok"), style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            if self.strComeFrom == "PUSH_NOTIFICATION" {
                                let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "DashBoardVC")
                                appDelegate.window?.rootViewController = mainVcIntial
                            }else{
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                       // Add the actions
                        alertController.addAction(okAction)
                        // Present the controller
                        self.present(alertController, animated: true, completion: nil)
                    }
                } else if("\(String(describing: responce.value(forKey: "error")!))" == "0"){
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: responce.value(forKey: "data")as! String, viewcontrol: self)
                }
            }else{
                if self.apiCallStatus == "call"{
                    self.apiCallStatus = "Nocall"
                    self.setUpDataForSend()
                }else{
                         showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)
                }
            }
        }
    }
    
    @objc func callAPI_GetNotificationQuestion() {
        let dict = NSMutableDictionary.init()
        if nsud.value(forKey: "AppLanguage") != nil{
            if nsud.value(forKey: "AppLanguage")as! String == "English"{
                dict.setValue("1", forKey: "language_preference")
            }else{
                dict.setValue("2", forKey: "language_preference")
            }
        }else{
            dict.setValue("1", forKey: "language_preference")
        }
        
        let dictLogInData = nsud.value(forKey: "LogInData")as! NSDictionary
        dict.setValue("\(dictLogInData.value(forKey: "location_id")!)", forKey: "location_id")   //1
        if strComeFrom == "PUSH_NOTIFICATION" {
            //var dictData = NSDictionary()
           // dictData = (dictNotiData.value(forKey: "aps")as!NSDictionary).value(forKey: "alert") as! NSDictionary
            dict.setValue("\(dictNotiData.value(forKey: "frequency_id")!)", forKey: "frequency_id") //2
        }else{
            dict.setValue("\(dictNotiData.value(forKey: "id")!)", forKey: "frequency_id") //2
        }

        print(dict)
        showLoader(strMsg: "", style: .extraLight)
        WebService.callAPI(parameter: dict, url: API_post_survey_question) { (responce, status) in
            DispatchQueue.main.async {
                dismissLoader()
            }
            print(responce)
            if(status == "Suceess"){
                if(responce.value(forKey: "error")as! String == "1"){
                    self.aryForPost = NSMutableArray()
                    if (responce.value(forKey: "data")as! NSArray).count != 0{
                        self.aryForPost = (((responce.value(forKey: "data")as! NSArray).object(at: 0)) as! NSArray).mutableCopy()as! NSMutableArray
                        self.filterDataAccordingtoType()
                    }
                }
                else if(responce.value(forKey: "error")as! String == "0"){
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: responce.value(forKey: "data")as! String, viewcontrol: self)
                }else{
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)
                }
            }else{
                let alertController = UIAlertController(title: Localization("UTHC_Alert"), message: Localization("UTHC_OtherError"), preferredStyle: .alert)
                // Create the actions
                let okAction = UIAlertAction(title: Localization("UTHC_TryAgain"), style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    self.callAPI_GetNotificationQuestion()
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    //MARK:
    //MARK: ---------------Dynemic View-------------
    func genrateDynemicView( aryData : NSMutableArray)  {
        
        for view in self.tvForList.subviews {
            view.removeFromSuperview()
        }
        var yAxix = 0.0
        
        for (index, item) in aryData.enumerated() {
            let dict = (item as AnyObject)as! NSDictionary
            let dictData = dict.mutableCopy()as! NSMutableDictionary
            let dictQuestion = dictData.value(forKey: "question")as! NSDictionary
            let strAnswer = "\(dictData.value(forKey: "answer")!)"
            let strType = dictQuestion.value(forKey: "selected_option_type")as! String
            let stroption_type = dictQuestion.value(forKey: "option_type")as! String
           
            //let strQuestionID = dictQuestion.value(forKey: "id")as! String
            //let strAppLanguage = "\((nsud.value(forKey: "AppLanguage"))!)"
            //MARK:*****Yes_No_Button_View*****
            
            if(strType == "yes_no_button_view") && stroption_type == "0"{
                let lblTitleQuestionYesNo = UILabel()
                let btnYES = UIButton()
                let btnNO = UIButton()
                
                lblTitleQuestionYesNo.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitleQuestionYesNo.textColor = UIColor.black
                lblTitleQuestionYesNo.numberOfLines = 0
                lblTitleQuestionYesNo.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitleQuestionYesNo.text!, view: self.view) )
                
                
                yAxix = Double(CGFloat(yAxix) + lblTitleQuestionYesNo.frame.size.height  + 10)
                
                btnYES.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: 120 , height: 30)
                btnYES.layer.cornerRadius = 4.0
                btnYES.layer.borderWidth = 1.0
                btnYES.layer.borderColor = UIColor.gray.cgColor
                btnYES.layer.backgroundColor = UIColor.white.cgColor
                
                
                let dictOption = (dictQuestion.value(forKey: "options")as! NSArray).object(at: 0)as! NSDictionary
                if (dictOption.value(forKey: "options_list") as! NSArray).count == 2{
                    btnYES.setTitle((dictOption.value(forKey: "options_list") as! NSArray).object(at: 0) as? String, for: .normal)
                    btnNO.setTitle((dictOption.value(forKey: "options_list") as! NSArray).object(at: 1) as? String, for: .normal)
                }
                
                
                btnNO.frame = CGRect(x: btnYES.frame.origin.x + 125, y: btnYES.frame.origin.y, width: 120 , height: 30)
                
                
                if strAnswer == btnYES.titleLabel?.text!{
                    btnYES.backgroundColor = hexStringToUIColor(hex: "BD4701")
                    btnNO.backgroundColor = UIColor.white
                    btnYES.setTitleColor(UIColor.white, for: .normal)
                    btnNO.setTitleColor(UIColor.black, for: .normal)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btnYES, borderWidth: 0.0)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btnNO, borderWidth: 1.0)
                }else if(strAnswer == btnNO.titleLabel?.text!){
                    btnNO.backgroundColor = hexStringToUIColor(hex: "BD4701")
                    btnYES.backgroundColor = UIColor.white
                    btnNO.setTitleColor(UIColor.white, for: .normal)
                    btnYES.setTitleColor(UIColor.black, for: .normal)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btnYES, borderWidth: 1.0)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btnNO, borderWidth: 0.0)
                }else{
                    btnNO.backgroundColor = UIColor.white
                    btnYES.backgroundColor = UIColor.white
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btnYES, borderWidth: 1.0)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btnNO, borderWidth: 1.0)
                    btnYES.setTitleColor(UIColor.black, for: .normal)
                    btnNO.setTitleColor(UIColor.black, for: .normal)
                }
                
                btnNO.tag = index
                btnYES.tag = index
                btnNO.addTarget(self, action: #selector(pressButtonYES_NO_ButtonView(_:)), for: .touchUpInside)
                btnYES.addTarget(self, action: #selector(pressButtonYES_NO_ButtonView(_:)), for: .touchUpInside)
                self.tvForList.addSubview(lblTitleQuestionYesNo)
                self.tvForList.addSubview(btnYES)
                self.tvForList.addSubview(btnNO)
                yAxix = Double(CGFloat(yAxix) +  btnNO.frame.size.height + 25.0)
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width ), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
            }
                
                //MARK:*****Yes No Range View*****
                
            else if(strType == "yes_no_range_view") && stroption_type == "0"{
                //--------Question
                let lblTitleQuestionSlider = UILabel()
                lblTitleQuestionSlider.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitleQuestionSlider.textColor = UIColor.black
                lblTitleQuestionSlider.numberOfLines = 0
                lblTitleQuestionSlider.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitleQuestionSlider.text!, view: self.view))
                yAxix = Double(CGFloat(yAxix) + lblTitleQuestionSlider.frame.size.height  + 20)
                self.tvForList.addSubview(lblTitleQuestionSlider)
         
                let dictOption = (dictQuestion.value(forKey: "options")as! NSArray).object(at: 0)as! NSDictionary
    
                var strWidth = CGFloat(10.0)
                for (_, item) in (dictOption.value(forKey: "options_list") as! NSArray).enumerated() {
                    let btn = UIButton()
                    btn.titleLabel?.font = UIFont(name: (btn.titleLabel?.font.fontName)!, size: 12.0)
                    btn.frame = CGRect(x:strWidth, y: CGFloat(yAxix), width: 80, height: 30 )
                    strWidth =  strWidth + btn.frame.size.width + 5.0
                    btn.setTitle("\(item)", for: .normal)
                    btn.tag = index
                    btn.backgroundColor = UIColor.white
                    btn.setTitleColor(UIColor.black, for: .normal)
                    btn.addTarget(self, action: #selector(pressButtonYESSlider(_:)), for: .touchUpInside)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 1.0)
                    
                    let answerArr = strAnswer.components(separatedBy: "::")
                    if answerArr.count == 2{
                        if answerArr[0] == btn.titleLabel?.text!{
                            btn.backgroundColor = hexStringToUIColor(hex: "BD4701")
                            btn.setTitleColor(UIColor.white, for: .normal)
                            makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 0.0)
                        }
                    }
                    self.tvForList.addSubview(btn)
                }
                yAxix = Double(CGFloat(yAxix) + 40.0)
                
                //--------Range Image
                let imgRange = UIImageView()
                imgRange.image = #imageLiteral(resourceName: "scale")
                imgRange.contentMode = .scaleToFill
                imgRange.frame = CGRect(x: 15.0, y: CGFloat(yAxix), width: self.view.frame.width - 30 , height: 30 )
                yAxix = Double(CGFloat(yAxix) + imgRange.frame.size.height  + 15)
                self.tvForList.addSubview(imgRange)
                
                //--------Slider
                let sliderRange = UISlider()
                sliderRange.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 30 )
                sliderRange.thumbTintColor = hexStringToUIColor(hex: "D75F00")
                sliderRange.minimumTrackTintColor = hexStringToUIColor(hex: "D75F00")
                sliderRange.maximumTrackTintColor = UIColor.lightGray
                yAxix = Double(CGFloat(yAxix) + sliderRange.frame.size.height  + 20)
                sliderRange.maximumValue = 10.0
                sliderRange.minimumValue = 0.0
                sliderRange.tag = index
                  
                sliderRange.addTarget(self, action: #selector(pressYesNoSliderChange(slider:event:)), for: .valueChanged)
                
                if strAnswer.count != 0{
                    let myStringArr = strAnswer.components(separatedBy: "::")
                    sliderRange.value = Float("\(myStringArr[1])")!
                }else{
                    sliderRange.value = 0.0
                }
                
                self.tvForList.addSubview(sliderRange)
                let lbl = UILabel()
                let trackRect: CGRect = sliderRange.trackRect(forBounds: sliderRange.bounds)
                let thumbRect: CGRect = sliderRange.thumbRect(forBounds: sliderRange.bounds, trackRect: trackRect, value: sliderRange.value)
                lbl.frame = CGRect(x: thumbRect.origin.x + sliderRange.frame.origin.x - 12.0 , y: sliderRange.frame.origin.y + 30, width: 60.0, height: 20.0)
                lbl.textColor = hexStringToUIColor(hex: "BD4701")
                lbl.text = "\(sliderRange.value)"
                lbl.font = UIFont(name: lbl.font.fontName, size: 12.0)
                lbl.textAlignment = .center
                self.tvForList.addSubview(lbl)
                yAxix = Double(CGFloat(yAxix) + 10.0)
                //---------emojiImage
                let emojiImage = UIImageView()
                emojiImage.image = #imageLiteral(resourceName: "smiles")
                emojiImage.contentMode = .scaleAspectFill
                
                emojiImage.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 70 )
                //    yAxix = Double(CGFloat(yAxix) + emojiImage.frame.size.height  + 20)
                //   self.tvForList.addSubview(emojiImage)
                
                //---------UnderLine
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
            }
                
                //MARK:*****Single_Select_Drop_Down*****
                
            else if(strType == "single_select_drop_down") && stroption_type == "0"{
                
                let lblTitlesingle_select_drop_down = UILabel()
                lblTitlesingle_select_drop_down.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitlesingle_select_drop_down.textColor = UIColor.black
                lblTitlesingle_select_drop_down.numberOfLines = 0
                lblTitlesingle_select_drop_down.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitlesingle_select_drop_down.text!, view: self.view) )
                
                yAxix = Double(CGFloat(yAxix) + lblTitlesingle_select_drop_down.frame.size.height  + 15)
                
                let lblSubTitle_select_drop_down = UILabel()
                lblSubTitle_select_drop_down.text = "\(dictQuestion.value(forKey: "title")!)"
                lblSubTitle_select_drop_down.textColor = UIColor.black
                lblSubTitle_select_drop_down.numberOfLines = 0
                lblSubTitle_select_drop_down.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 20 )
                lblSubTitle_select_drop_down.text = Localization("Pre-Treatment Analysis2_Select_Body_Part")
                lblSubTitle_select_drop_down.font = UIFont(name: (lblSubTitle_select_drop_down.font?.fontName)!, size: 14)
                
                yAxix = Double(CGFloat(yAxix) + lblSubTitle_select_drop_down.frame.size.height + 10)
                
                
                let btnDrop = UIButton()
                btnDrop.setBackgroundImage(#imageLiteral(resourceName: "dropdown_2"), for: .normal)
                btnDrop.addTarget(self, action: #selector(pressButtonDropDown(_:)), for: .touchUpInside)
                btnDrop.tag = index
                btnDrop.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 35 )
                
                let lblselectedText = UILabel()
                lblselectedText.textColor = UIColor.black
                lblselectedText.numberOfLines = 0
                lblselectedText.textAlignment = .center
                lblselectedText.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 40 , height: 30 )
                lblselectedText.font = UIFont(name: (lblselectedText.font?.fontName)!, size: 14)
                
                if strAnswer.count != 0{
                    lblselectedText.text = strAnswer
                    lblselectedText.textColor = UIColor.black
                }else{
                    lblselectedText.textColor = UIColor.darkGray
                    lblselectedText.text = Localization("Pre-Treatment Analysis2_Select")
                }
                yAxix = Double(CGFloat(yAxix) + btnDrop.frame.size.height + 25)
                
                self.tvForList.addSubview(lblTitlesingle_select_drop_down)
                self.tvForList.addSubview(lblSubTitle_select_drop_down)
                self.tvForList.addSubview(lblselectedText)
                self.tvForList.addSubview(btnDrop)
                
                
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
            }
                
                //MARK:*****Single_Select_Redio_Button*****
                
            else if(strType == "single_select_redio_button") && stroption_type == "0"{
                
                //---Question
                let lblTitleRadio = UILabel()
                lblTitleRadio.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitleRadio.textColor = UIColor.black
                lblTitleRadio.numberOfLines = 0
                lblTitleRadio.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitleRadio.text!, view: self.view) )
                
                yAxix = Double(CGFloat(yAxix) + lblTitleRadio.frame.size.height  + 03)
                
                self.tvForList.addSubview(lblTitleRadio)
                
                //CheckBox
                
                let aryorOption = dictQuestion.value(forKey: "options")as! NSArray
                if aryorOption.count != 0{
                    
                    let dict = aryorOption.object(at: 0)as! NSDictionary
                    let aryOptionlist = dict.value(forKey: "options_list")as! NSArray
                    if aryOptionlist.count != 0{
                        for item in aryOptionlist{
                            let lblTitile = UILabel()
                            let btnImg  = UIButton()
                            btnImg.tag = index
                            btnImg.addTarget(self, action: #selector(pressButtonRadio(_:)), for: .touchDown)
                            lblTitile.textColor = UIColor.black
                            lblTitile.text = (item as AnyObject)as? String
                            btnImg.setTitle(lblTitile.text, for: .normal)
                            lblTitile.frame = CGRect(x: lblTitleRadio.frame.origin.x+30.0, y: CGFloat(yAxix), width: self.view.frame.width - 50 , height: 30)
                            btnImg.frame = CGRect(x:  lblTitleRadio.frame.origin.x, y: CGFloat(yAxix + 3), width: 30 , height: 30)
                            btnImg.setImage(#imageLiteral(resourceName: "pending"), for: .normal)
                            if (strAnswer as String).count != 0 {
                                if (btnImg.titleLabel?.text! ==  strAnswer){
                                    btnImg.setImage(#imageLiteral(resourceName: "redio_2"), for: .normal)
                                }
                            }
                            yAxix = yAxix + 40.0
                            self.tvForList.addSubview(lblTitile)
                            self.tvForList.addSubview(btnImg)
                        }
                    }
                }
                //---------UnderLine
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
            }
                
                //MARK:*****Single_Select_Button_View*****
                
            else if(strType == "single_select_button_view") && stroption_type == "0"{
                //---Question
                let lblTitlesingle_single_select_button_view = UILabel()
                lblTitlesingle_single_select_button_view.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitlesingle_single_select_button_view.textColor = UIColor.black
                lblTitlesingle_single_select_button_view.numberOfLines = 0
                lblTitlesingle_single_select_button_view.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitlesingle_single_select_button_view.text!, view: self.view) )
                
                yAxix = Double(CGFloat(yAxix) + lblTitlesingle_single_select_button_view.frame.size.height  + 15)
                
                self.tvForList.addSubview(lblTitlesingle_single_select_button_view)
                
                //----Button
                let aryorOption = dictQuestion.value(forKey: "options")as! NSArray
                
                if aryorOption.count != 0{
                    let dict = aryorOption.object(at: 0)as! NSDictionary
                    let aryOptionlist = dict.value(forKey: "options_list")as! NSArray
                    if aryOptionlist.count != 0{
                        var strWidth = CGFloat(10.0)
                        
                        for (index1, item) in aryOptionlist.enumerated() {
                            let btn = UIButton()
                            btn.titleLabel?.font = UIFont(name: (btn.titleLabel?.font.fontName)!, size: 12.0)
                            btn.frame = CGRect(x:strWidth, y: CGFloat(yAxix), width: self.view.frame.width / 3 - 10 , height: 30 )
                            strWidth =  strWidth + btn.frame.size.width + 5.0
                            btn.setTitle("\(item)", for: .normal)
                            btn.tag = index
                            btn.addTarget(self, action: #selector(pressButtonSinglButtonDown(_:)), for: .touchUpInside)
                            btn.backgroundColor = UIColor.white
                            btn.setTitleColor(UIColor.black, for: .normal)
                            makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 1.0)
                            if strAnswer == btn.titleLabel?.text!{
                                btn.backgroundColor = hexStringToUIColor(hex: "BD4701")
                                btn.setTitleColor(UIColor.white, for: .normal)
                                makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 0.0)
                                
                            }
                            if ((index1 + 1) % 3 == 0){
                                yAxix = yAxix + Double(btn.frame.size.height + 10.0)
                                strWidth = CGFloat(10.0)

                            }
                            self.tvForList.addSubview(btn)
                        }
                        //yAxix = yAxix + Double(40.0)
                    }
                }
                
                //----Underline
                yAxix = Double(CGFloat(yAxix) + 40)
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) + 5)
                
            }
                //MARK:*****Multi_Select_Check_Box*****
            else if(strType == "multi_select_check_box") && stroption_type == "0"{
                
                //----Question
                let lblTitlemulti_select_check_box = UILabel()
                lblTitlemulti_select_check_box.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitlemulti_select_check_box.textColor = UIColor.black
                lblTitlemulti_select_check_box.numberOfLines = 0
                lblTitlemulti_select_check_box.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitlemulti_select_check_box.text!, view: self.view))
                yAxix = Double(CGFloat(yAxix) + lblTitlemulti_select_check_box.frame.size.height  + 20)
                self.tvForList.addSubview(lblTitlemulti_select_check_box)
                
                //CheckBox
                
                let aryorOption = dictQuestion.value(forKey: "options")as! NSArray
                if aryorOption.count != 0{
                    
                    let dict = aryorOption.object(at: 0)as! NSDictionary
                    let aryOptionlist = dict.value(forKey: "options_list")as! NSArray
                    if aryOptionlist.count != 0{
                        for item in aryOptionlist{
                            let lblTitile = UILabel()
                            let btnImg  = UIButton()
                            btnImg.tag = index
                            btnImg.addTarget(self, action: #selector(pressButtonMultiCheckBox(_:)), for: .touchDown)
                            lblTitile.textColor = UIColor.black
                            lblTitile.text = (item as AnyObject)as? String
                            btnImg.setTitle(lblTitile.text, for: .normal)
                            lblTitile.frame = CGRect(x: lblTitlemulti_select_check_box.frame.origin.x+30.0, y: CGFloat(yAxix), width: self.view.frame.width - 50 , height: 30)
                            btnImg.frame = CGRect(x:  lblTitlemulti_select_check_box.frame.origin.x, y: CGFloat(yAxix), width: 30 , height: 30)
                            btnImg.setImage(#imageLiteral(resourceName: "check_box_1"), for: .normal)
                            
                            if (strAnswer as String).count != 0 {
                                let myStringArr = strAnswer.components(separatedBy: "::")
                                btnImg.setImage(#imageLiteral(resourceName: "check_box_1"), for: .normal)
                                for item in myStringArr{
                                    if (btnImg.titleLabel?.text! ==  item){
                                        btnImg.setImage(#imageLiteral(resourceName: "check_box_2"), for: .normal)
                                    }
                                }
                            }
                            
                            
                            yAxix = yAxix + 40.0
                            self.tvForList.addSubview(lblTitile)
                            self.tvForList.addSubview(btnImg)
                        }
                    }
                }
                //---------UnderLine
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
            }
                //MARK:*****Yes_No_Radio_Button*****
                
            else if(strType == "yes_no_radio_button") && stroption_type == "0"{
                let lblTitleQuestionYesNo = UILabel()
                lblTitleQuestionYesNo.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitleQuestionYesNo.textColor = UIColor.black
                lblTitleQuestionYesNo.numberOfLines = 0
                lblTitleQuestionYesNo.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitleQuestionYesNo.text!, view: self.view) )
                yAxix = Double(CGFloat(yAxix) + lblTitleQuestionYesNo.frame.size.height  + 10)
                self.tvForList.addSubview(lblTitleQuestionYesNo)
                
                //----Button
                let aryorOption = dictQuestion.value(forKey: "options")as! NSArray
                
                if aryorOption.count != 0{
                    let dict = aryorOption.object(at: 0)as! NSDictionary
                    let aryOptionlist = dict.value(forKey: "options_list")as! NSArray
                    if aryOptionlist.count != 0{
                        var strWidth = CGFloat(10.0)
                        
                        for (index1, item) in aryOptionlist.enumerated() {
                            let btn = UIButton()
                            btn.frame = CGRect(x:strWidth, y: CGFloat(yAxix), width: self.view.frame.width / 4 - 10 , height: 30 )
                            strWidth =  strWidth + btn.frame.size.width + 5.0
                            btn.setTitle("\(item)", for: .normal)
                            btn.tag = index
                            btn.addTarget(self, action: #selector(pressButtonSinglButtonDown(_:)), for: .touchUpInside)
                            btn.backgroundColor = UIColor.white
                            btn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left;
                            btn.setTitleColor(UIColor.black, for: .normal)
                            btn.setImage(#imageLiteral(resourceName: "redio_1"), for: .normal)
                            if strAnswer == btn.titleLabel?.text!{
                                btn.setImage(#imageLiteral(resourceName: "redio_2"), for: .normal)
                            }
                            if ((index1 + 1) % 4 == 0){
                                yAxix = yAxix + Double(btn.frame.size.height + 10.0)
                            }
                            let spacing = 15; // the amount of spacing to appear between image and title
                            
                            let insetAmount = spacing / 2
                            btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(-insetAmount), bottom: 0, right: CGFloat(insetAmount))
                            btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(insetAmount), bottom: 0, right: CGFloat(-insetAmount))
                            btn.contentEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(insetAmount), bottom: 0, right: CGFloat(insetAmount))
                            
                            self.tvForList.addSubview(btn)
                        }
                        yAxix = yAxix + Double(40.0)
                        
                    }
                }
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width ), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
                
            }
              
                //MARK:*****Textbox View*****
                
            else if(strType == "textbox") && stroption_type == "0"{
                let lblTitleQuestionYesNo = UILabel()
                lblTitleQuestionYesNo.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitleQuestionYesNo.textColor = UIColor.black
                lblTitleQuestionYesNo.numberOfLines = 0
                lblTitleQuestionYesNo.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitleQuestionYesNo.text!, view: self.view) )
                yAxix = Double(CGFloat(yAxix) + lblTitleQuestionYesNo.frame.size.height  + 10)
                self.tvForList.addSubview(lblTitleQuestionYesNo)
                
                //----Text
                let txt = UITextField()
                txt.layer.cornerRadius = 4.0
                txt.layer.borderColor = UIColor.gray.cgColor
                txt.layer.borderWidth = 1.0
                txt.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 35)
                txt.delegate = self
                txt.tag = index
                txt.text = strAnswer
                txt.placeholder = ""
                yAxix = Double(CGFloat(yAxix) + txt.frame.size.height  + 10)
                self.tvForList.addSubview(txt)
                
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width ), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
                
            }
                
                
                
                
                //MARK:*****SLIDER*****
                
            else if(strType == "") && stroption_type == "1"{
                
                //--------Question
                
                let lblTitleQuestionSlider = UILabel()
                lblTitleQuestionSlider.text = "\(dictQuestion.value(forKey: "title")!)"
                lblTitleQuestionSlider.textColor = UIColor.black
                lblTitleQuestionSlider.numberOfLines = 0
                lblTitleQuestionSlider.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: estimatedHeightOfLabel(text: lblTitleQuestionSlider.text!, view: self.view))
                yAxix = Double(CGFloat(yAxix) + lblTitleQuestionSlider.frame.size.height  + 20)
                self.tvForList.addSubview(lblTitleQuestionSlider)
                
                //--------Range Image
                let imgRange = UIImageView()
                imgRange.image = #imageLiteral(resourceName: "scale")
                imgRange.contentMode = .scaleToFill
                imgRange.frame = CGRect(x: 15.0, y: CGFloat(yAxix), width: self.view.frame.width - 30 , height: 30 )
                yAxix = Double(CGFloat(yAxix) + imgRange.frame.size.height  + 15)
                self.tvForList.addSubview(imgRange)
                
                
                //lbl for no pain Worst PAin
            
                let lblNoPain = UILabel()
                lblNoPain.text = Localization("PostQuestion_NoPain")
                lblNoPain.textColor = hexStringToUIColor(hex: "D75F00")
                lblNoPain.numberOfLines = 1
                lblNoPain.textAlignment = .left
                lblNoPain.font = UIFont(name: (lblNoPain.font.fontName), size: 12.0)

                lblNoPain.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: 100 , height: 20)
                self.tvForList.addSubview(lblNoPain)
            
                let lblWorstPain = UILabel()
                lblWorstPain.text = Localization("PostQuestion_WorstPain")
                lblWorstPain.textColor = hexStringToUIColor(hex: "D75F00")
                lblWorstPain.textAlignment = .right
                lblWorstPain.numberOfLines = 1
               lblWorstPain.font = UIFont(name: (lblWorstPain.font.fontName), size: 12.0)

                lblWorstPain.frame = CGRect(x:self.view.frame.width - 160, y: CGFloat(yAxix), width:150 , height: 20)
                yAxix = Double(CGFloat(yAxix) + lblWorstPain.frame.size.height  + 5)
                self.tvForList.addSubview(lblWorstPain)
                
                //--------Slider
                let sliderRange = UISlider()
                sliderRange.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 30 )
                sliderRange.thumbTintColor = hexStringToUIColor(hex: "D75F00")
                sliderRange.minimumTrackTintColor = hexStringToUIColor(hex: "D75F00")
                sliderRange.maximumTrackTintColor = UIColor.lightGray
                
                yAxix = Double(CGFloat(yAxix) + sliderRange.frame.size.height  + 20)
                sliderRange.maximumValue = 10.0
                sliderRange.minimumValue = 0.0
                sliderRange.tag = index
                
                

               sliderRange.addTarget(self, action: #selector(pressSliderChange(slider:event:)), for: .touchUpInside)
                if strAnswer.count != 0{
                    let myStringArr = strAnswer.components(separatedBy: "::")
                    sliderRange.value = Float("\(myStringArr[0])")!
                }else{
                    sliderRange.value = 0.0
                }
                self.tvForList.addSubview(sliderRange)
                let lbl = UILabel()
                let trackRect: CGRect = sliderRange.trackRect(forBounds: sliderRange.bounds)
                let thumbRect: CGRect = sliderRange.thumbRect(forBounds: sliderRange.bounds, trackRect: trackRect, value: sliderRange.value)
                lbl.frame = CGRect(x: thumbRect.origin.x + sliderRange.frame.origin.x - 12.0 , y: sliderRange.frame.origin.y + 30, width: 60.0, height: 20.0)
                lbl.textColor = hexStringToUIColor(hex: "BD4701")
                lbl.text = "\(sliderRange.value)"
                lbl.font = UIFont(name: lbl.font.fontName, size: 12.0)
                lbl.textAlignment = .center
                self.tvForList.addSubview(lbl)
                yAxix = Double(CGFloat(yAxix) + 10.0)

                //--------Range 2  Image
                let lblExtra = UILabel()
                lblExtra.text = Localization("Pre-Treatment Analysis1_extraquestion")
                lblExtra.textColor = UIColor.black
                lblExtra.numberOfLines = 0
                lblExtra.frame = CGRect(x: 15.0, y: CGFloat(yAxix), width: self.view.frame.width - 30 , height: 30 )
                yAxix = Double(CGFloat(yAxix) + lblExtra.frame.size.height  + 15)
                self.tvForList.addSubview(lblExtra)
              
                let aryExtraButton = NSMutableArray()
       
                aryExtraButton.add(Localization("PostQuestion_None"))
                aryExtraButton.add(Localization("PostQuestion_Mild"))
                aryExtraButton.add(Localization("PostQuestion_Moderate"))
                aryExtraButton.add(Localization("PostQuestion_Severe"))
               var strWidth = CGFloat(10.0)
                for (_, item) in aryExtraButton.enumerated() {
                    let btn = UIButton()
                    btn.titleLabel?.font = UIFont(name: (btn.titleLabel?.font.fontName)!, size: 12.0)
                    btn.frame = CGRect(x:strWidth, y: CGFloat(yAxix), width: self.view.frame.width / 4 - 10 , height: 30 )
                    strWidth =  strWidth + btn.frame.size.width + 5.0
                    btn.setTitle("\(item)", for: .normal)
                    btn.tag = index
                    btn.backgroundColor = UIColor.white
                    btn.setTitleColor(UIColor.black, for: .normal)
                    btn.addTarget(self, action: #selector(pressExtraButton(_:)), for: .touchUpInside)
                    makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 1.0)
                    
                let answerArr = strAnswer.components(separatedBy: "::")
                    if answerArr.count == 2{
                    if answerArr[1] == btn.titleLabel?.text!{
                        btn.backgroundColor = hexStringToUIColor(hex: "BD4701")
                        btn.setTitleColor(UIColor.white, for: .normal)
                        makeButtonCorner(radius: 6.0, color: UIColor.gray, btn: btn, borderWidth: 0.0)
                        }
                    }
                    self.tvForList.addSubview(btn)
                }
                yAxix = Double(CGFloat(yAxix) + 40.0)

                
                //---------emojiImage
                let emojiImage = UIImageView()
                emojiImage.image = #imageLiteral(resourceName: "smiles")
                emojiImage.contentMode = .scaleAspectFill
                
                emojiImage.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 70 )
                //  yAxix = Double(CGFloat(yAxix) + emojiImage.frame.size.height  + 20)
                //  self.tvForList.addSubview(emojiImage)
                
                //---------UnderLine
                self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width), height: 1.0)))
                yAxix = Double(CGFloat(yAxix) +  5.0)
                
            }
            
        }
        
        if aryData.count != 0 {
            let btnContinue = UIButton()
            btnContinue.titleLabel?.font = UIFont(name: (btnContinue.titleLabel?.font.fontName)!, size: 20.0)
            btnContinue.frame = CGRect(x: 10.0, y: CGFloat(yAxix + 20.0), width: self.view.frame.width  - 20 , height: 40 )
            
            btnContinue.setTitle(Localization("DashBoard_Submit"), for: .normal)
            btnContinue.addTarget(self, action: #selector(pressButtonContinue(_:)), for: .touchUpInside)
            makeButtonCorner(radius: 15.0, color: UIColor.gray, btn: btnContinue, borderWidth: 0.0)
            btnContinue.backgroundColor = hexStringToUIColor(hex: "BD4701")
            tvForList.addSubview(btnContinue)
        }
        DispatchQueue.main.async {
            self.tvForList.contentSize = CGSize(width: self.view.frame.size.width, height: CGFloat(yAxix + 100.0))
        }
    }
    
    func underlineCreat(ract:CGRect) -> UILabel {
        let lbl = UILabel()
        lbl.frame = ract
        lbl.backgroundColor = UIColor.lightGray
        return lbl
    }
    
    
    func filterDataAccordingtoType()  {
        aryForTvList = NSMutableArray()
        for item in aryForPost {
            let dictData = ((item as AnyObject)as! NSDictionary).mutableCopy()as! NSMutableDictionary
            let dictQuestion = dictData.value(forKey: "question")as! NSDictionary
            //--Set default value on slider question
            let strType = dictQuestion.value(forKey: "selected_option_type")as! String
            let stroption_type = dictQuestion.value(forKey: "option_type")as! String
            if(strType == "") && stroption_type == "1"{
                dictData.setValue("0.0", forKey: "answer")
            }else{
                dictData.setValue("", forKey: "answer")
            }
            
            dictData.setValue(NSMutableArray(), forKey: "Multianswer")
            
            aryForTvList.add(dictData)
            
            
            
        }
        genrateDynemicView(aryData: aryForTvList)
        print(aryForTvList)
    }
    
    
    //MARK:
    //MARK: --------------Action's For Table Cell Button's---------------
  
    
    
    
    //MARK:*****Yes_No_Button_Slider_View*****
    
      @objc func pressYesNoSliderChange(slider: UISlider, event: UIEvent){
          if let touchEvent = event.allTouches?.first {
                           switch touchEvent.phase {
                           case .began: break
                               // handle drag began
                           case .moved: break
                               // handle drag moved
                           case .ended:
                      let dict = aryForTvList.object(at: slider.tag)as! NSDictionary
                     let dictData = dict.mutableCopy()as! NSMutableDictionary
                     let roundedStepValue = round(slider.value / 0.5) * 0.5
                     slider.value = roundedStepValue
                     let twoDecimalPlaces = String(format: "%.1f", slider.value)
                     let answer = dictData.value(forKey: "answer")as! String
                     var strAnswer = ""
                     if (answer as String).count != 0 {
                         let myStringArr = answer.components(separatedBy: "::")
                         if myStringArr.count == 2{
                             strAnswer = "\(myStringArr[0])::\(twoDecimalPlaces)"
                         }else{
                              strAnswer = "::\(twoDecimalPlaces)"
                         }
                     }else{
                         strAnswer = "::\(twoDecimalPlaces)"
                     }
                     dictData.setValue("\(strAnswer)", forKey: "answer")
                     aryForTvList.replaceObject(at: slider.tag, with: dictData)
                     genrateDynemicView(aryData: aryForTvList)
              default:
              break
              }
          }
         
          
      }
    
    @objc func pressButtonYESSlider(_ button: UIButton) {
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        let answer = dictData.value(forKey: "answer")as! String
        var strAnswer = ""
        if (answer as String).count != 0 {
            let myStringArr = answer.components(separatedBy: "::")
            if myStringArr.count == 2{
                
                strAnswer = "\(String(describing: (button.titleLabel?.text!)!))::\(myStringArr[1])"
            }
            
        }else{
            strAnswer = "\(String(describing: (button.titleLabel?.text!)!))::0.0"
        }
        
        dictData.setValue(strAnswer, forKey: "answer")
        aryForTvList.replaceObject(at: button.tag, with: dictData)
        genrateDynemicView(aryData: aryForTvList)
        
    }
   
    //MARK:*****Yes_No_Button_View*****
    @objc func pressButtonYES_NO_ButtonView(_ button: UIButton) {
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        dictData.setValue(button.titleLabel?.text!, forKey: "answer")
        aryForTvList.replaceObject(at: button.tag, with: dictData)
        genrateDynemicView(aryData: aryForTvList)
    }
   
    //MARK:*****Single_Select_Redio_Button*****
    
    @objc func pressButtonRadio(_ button: UIButton) {
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        dictData.setValue(button.titleLabel?.text!, forKey: "answer")
        aryForTvList.replaceObject(at: button.tag, with: dictData)
        genrateDynemicView(aryData: aryForTvList)
    }
    
    
    //MARK:*****SLIDER*****

        @objc func pressSliderChange(slider: UISlider, event: UIEvent)
        {
            if let touchEvent = event.allTouches?.first {
               switch touchEvent.phase {
               case .began: break
                   // handle drag began
               case .moved: break
                   // handle drag moved
               case .ended:
                   
                
                 let dict = aryForTvList.object(at: slider.tag)as! NSDictionary
                 let dictData = dict.mutableCopy()as! NSMutableDictionary
                 let roundedStepValue = round(slider.value / 0.5) * 0.5
                 slider.value = roundedStepValue

                 let twoDecimalPlaces = String(format: "%.1f", slider.value)
                 let answer = dictData.value(forKey: "answer")as! String
                 var strAnswer = ""
                     
                 if (answer as String).count != 0 {
                       let myStringArr = answer.components(separatedBy: "::")
                       if myStringArr.count == 2{
                           if(twoDecimalPlaces == "0.0"){
                               strAnswer = "\(twoDecimalPlaces)::"
                           }else{
                               strAnswer = "\(twoDecimalPlaces)::\(myStringArr[1])"
                           }
                       }else{
                           strAnswer = "\(twoDecimalPlaces)::"
                       }
                   }else{
                       strAnswer = "\(twoDecimalPlaces)::"
                   }
                 
                            dictData.setValue("\(strAnswer)", forKey: "answer")
                            self.aryForTvList.replaceObject(at: slider.tag, with: dictData)
                            self.genrateDynemicView(aryData: self.aryForTvList)
               default:
                   break
               }
           }
 
    }
    
    
    @objc func pressExtraButton(_ button: UIButton) {
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        let answer = dictData.value(forKey: "answer")as! String
        var strAnswer = ""
        if (answer as String).count != 0 {
            let myStringArr = answer.components(separatedBy: "::")
            if myStringArr.count == 2{
                
                strAnswer = "\(myStringArr[0])::\(String(describing: (button.titleLabel?.text!)!))"
            }
            
        }else{
            strAnswer = "0.0::\(String(describing: (button.titleLabel?.text!)!))"
        }
        
        if(button.titleLabel?.text == "Severe" || button.titleLabel?.text == "Grave"){
            let myStringArr = answer.components(separatedBy: "::")
            if myStringArr.count > 0{
                if("\(myStringArr[0])" == "0.0" || "\(myStringArr[0])" == ""){
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage:Localization("PostQuestion_Severe_Alert") , viewcontrol: self)
                }else{
                    dictData.setValue(strAnswer, forKey: "answer")
                    aryForTvList.replaceObject(at: button.tag, with: dictData)
                    genrateDynemicView(aryData: aryForTvList)
                }
            }
        }else{
            dictData.setValue(strAnswer, forKey: "answer")
            aryForTvList.replaceObject(at: button.tag, with: dictData)
            genrateDynemicView(aryData: aryForTvList)
        }
    }
    
    //MARK:*****Single_Select_Drop_Down*****
    
    @objc func pressButtonDropDown(_ button: UIButton) {
        tagForDropDown = button.tag
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        let dictQuestion = dictData.value(forKey: "question")as! NSDictionary
        
        let dictOption = ((dictQuestion.value(forKey: "options")as! NSArray).object(at: 0)as! NSDictionary)
        if (dictOption.value(forKey: "options_list") as! NSArray).count != 0{
            let vc: CommonTableVC = self.storyboard!.instantiateViewController(withIdentifier: "CommonTableVC") as! CommonTableVC
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.handlePostQuestion = self
            vc.arytbl =  (dictOption.value(forKey: "options_list") as! NSArray).mutableCopy()as! NSMutableArray
            //vc.arytbl = aryselectedBodyParts
            vc.strTag = 20
            vc.strTitle =  Localization("Pre-Treatment Analysis2_Select_Body_Part")
            self.present(vc, animated: false, completion: {})
            
        }
        
    }
    
    //MARK:*****Single_Select_Button_View && YES NO Redio Button *****
    
    @objc func pressButtonSinglButtonDown(_ button: UIButton) {
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        dictData.setValue(button.titleLabel?.text!, forKey: "answer")
        aryForTvList.replaceObject(at: button.tag, with: dictData)
        genrateDynemicView(aryData: aryForTvList)
        
    }
    
    //MARK:*****Multi_Select_Check_Box*****
    
    @objc func pressButtonMultiCheckBox(_ button: UIButton) {
        let dict = aryForTvList.object(at: button.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        var answer = (dictData.value(forKey: "answer")as! NSString)
        let strCheck = "\(String(describing: (button.titleLabel?.text!)!))::"
        if answer.contains(strCheck) {
            let newString = answer.replacingOccurrences(of: "\(strCheck)", with:"")
            dictData.setValue(newString, forKey: "answer")
        }else{
            answer = "\(answer)\(strCheck)" as NSString
            dictData.setValue(answer, forKey: "answer")
        }
        
        aryForTvList.replaceObject(at: button.tag, with: dictData)
        genrateDynemicView(aryData: aryForTvList)
        
    }
    //MARK:*****Continue*****
    @objc func pressButtonContinue(_ button: UIButton) {
        setUpDataForSend()
    }
    
    
    
    
}
//MARK:
//MARK: -----------Text Filed Delegate Method----------

extension NotificationQuestionVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        if string == " " {
            return false
        }
        if string == "" {
            return true
        }
        if (textField.text!.count > 25) {
            return false
        }
        return true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let dict = aryForTvList.object(at:textField.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        dictData.setValue(textField.text, forKey: "answer")
        aryForTvList.replaceObject(at: textField.tag, with: dictData)
        self.genrateDynemicView(aryData: aryForTvList)
    }
}


//MARK:-
//MARK:- ----------Refresh Delegate Methods----------


extension NotificationQuestionVC: refreshPost_Notification {
    
    
    
    func refreshviewWithData(dict: NSDictionary, tag: Int) {
        
    }
    
    func refreshview(str: String, tag: Int) {
        // For Singl DropDown
        if tag == 20 {
            
            let dict = aryForTvList.object(at: tagForDropDown)as! NSDictionary
            let dictData = dict.mutableCopy()as! NSMutableDictionary
            dictData.setValue(str, forKey: "answer")
            aryForTvList.replaceObject(at: tagForDropDown, with: dictData)
            self.genrateDynemicView(aryData: aryForTvList)
            
        }
    }
    
    
}
//MARK:-
//MARK:- ----------MFMailComposeViewControllerDelegate Methods----------


extension NotificationQuestionVC: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        if self.strComeFrom == "PUSH_NOTIFICATION" {
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "DashBoardVC")
            appDelegate.window?.rootViewController = mainVcIntial
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        controller.dismiss(animated: true, completion: nil)
    }
}

