//
//  AppDelegate.swift
//  UTHC_APP
//
//  Created by Navin Patidar on 2/12/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications

let kConstantObj = kConstant()
let appd = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    //MARK:
    //MARK: --------------Life Cycle-------------
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.sharedManager().enable = true
        
        //-----For Push Notification---
        self.registerForRemoteNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        FTIndicator.setIndicatorStyle(UIBlurEffect.Style.dark)
        manageViewcontrollerWhenUserlogin()
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        self.saveContext()
    }
    
    
    
    // MARK: ---------Register RemoteNotifications Func----------
    // MARK: -
    
    func registerForRemoteNotifications()  {
        FirebaseApp.configure()
        if #available(iOS 10, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                if granted == true
                {
                    print("Notification Allow")
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                    Messaging.messaging().delegate = self
                }
                else
                {
                    print("Notification Don't Allow")
                }
            }
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            UIApplication.shared.applicationIconBadgeNumber = 1
        }
    }
    
    func getNotificationSettings()  {
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings(){ (setttings) in
                
                switch setttings.soundSetting{
                case .enabled:
                    print("Enabled notification sound setting")
                    
                case .disabled:
                    print("Notification setting has been disabled")
                    
                case .notSupported:
                    print("something vital went wrong here")
                }
            }
        } else {
            
        }
    }
    
    
    // MARK: ---------GET TOKEN Func----------
    // MARK: -
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            nsud.setValue(uuid, forKey: "DEVICE_ID")
            print("DEVICE_ID-------------------- : \(uuid)")
        }
        if let token = InstanceID.instanceID().token() {
            nsud.setValue("\(token)", forKey: "FCM_Token")
            nsud.synchronize()
            print("FCM_Token----------------- : \(String(describing: token))")
            
        }
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        nsud.setValue("5437788560965635874098656heyfget87", forKey: "FCM_Token")
        nsud.synchronize()
    }
    
    
    @objc func tokenRefreshNotification(notification: NSNotification) {
        if let token = InstanceID.instanceID().token() {
            nsud.setValue("\(token)", forKey: "FCM_Token")
            nsud.synchronize()
        }
    }
    
    
    // MARK: --------UNNotification/ Messaging Delegate Method----------
    // MARK: -
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        hendelNotidata(userInfo: userInfo as NSDictionary)
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let dictApsData : NSDictionary = response.notification.request.content.userInfo as NSDictionary
        hendelNotidata(userInfo: dictApsData)
    }
    
    func hendelNotidata(userInfo :NSDictionary){
        
        
        let state = UIApplication.shared.applicationState
        
        if state == .background ||  state == .inactive {
            
            
            if ((nsud.value(forKey: "LogInData")) != nil){
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let obj_ISVC = storyboard.instantiateViewController(withIdentifier: "NotificationQuestionVC") as! NotificationQuestionVC
                obj_ISVC.dictNotiData = userInfo.mutableCopy()as! NSMutableDictionary
                obj_ISVC.strComeFrom = "PUSH_NOTIFICATION"
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                rootViewController(nav: navigationController)
            }
        }
        else if state == .active {
            //   showToastMessage(strMsg: "notfication", style: .dark)
            //   FTIndicator.setIndicatorStyle(.prominent)
            // FTIndicator.showInfo(withMessage: "notification")
            //  showToastMessage(strMsg: (userInfo.value(forKey: "title")as!String), style: .dark)
        }
        
    }
    
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        
    }
    
    
    
    //MARK:
    //MARK: --------------Extra Function's-----------------
    func rootViewController(nav : UINavigationController)  {
        self.window!.rootViewController = nav
        self.window!.makeKeyAndVisible()
        nav.setNavigationBarHidden(true, animated: true)
    }
    
    
    func manageViewcontrollerWhenUserlogin()  {
        let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
        
        if ((nsud.value(forKey: "AppLanguage")) != nil){
            
            let strAppLanguage = (nsud.value(forKey: "AppLanguage"))!
            
            if (strAppLanguage as! String  == "English"){
                if SetLanguage(arrayLanguages[1]) {
                    
                }
            }else if(strAppLanguage as! String  == "Spanish"){
                if SetLanguage(arrayLanguages[0]) {
                }
            }
        }else{
            nsud.setValue("English", forKey: "AppLanguage")
            if SetLanguage(arrayLanguages[1]) {
            }
        }
        // For Check LoginStatus
        if ((nsud.value(forKey: "LoginStatus")) != nil){
            if (nsud.value(forKey: "LoginStatus")!)as! String == "YES" {
                // For Check RememberMeStatus
                if  (nsud.value(forKey: "RememberMeStatus")!)as! String == "YES"{
                    // For Check Answer Status And Redirect on Screen
                    //1 for Body Screen
                    // 2 For Paion ralated tooth
                    //3 For WellNess
                    // 4 For PostAnalysis
                    // 5 For Dashboard
                    if ((nsud.value(forKey: "AnswerStatus")) != nil){
                        let str = nsud.value(forKey: "AnswerStatus")as! String
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        if str == "1"{
                            let obj_ISVC = storyboard.instantiateViewController(withIdentifier: "BodyPartVC") as! BodyPartVC
                            let navigationController = UINavigationController(rootViewController: obj_ISVC)
                            rootViewController(nav: navigationController)
                        }else if(str == "2"){
                            let obj_ISVC = storyboard.instantiateViewController(withIdentifier: "PreTreatMentRelatedToothVC") as! PreTreatMentRelatedToothVC
                            let navigationController = UINavigationController(rootViewController: obj_ISVC)
                            rootViewController(nav: navigationController)
                        }
                        else if(str == "3"){
                            let obj_ISVC = storyboard.instantiateViewController(withIdentifier: "WellnessQuestionVC") as! WellnessQuestionVC
                            let navigationController = UINavigationController(rootViewController: obj_ISVC)
                            rootViewController(nav: navigationController)
                        }
                        else if(str == "4"){
                            let obj_ISVC = storyboard.instantiateViewController(withIdentifier: "PostQuestionVC") as! PostQuestionVC
                            let navigationController = UINavigationController(rootViewController: obj_ISVC)
                            rootViewController(nav: navigationController)
                        }  else if(str == "5"){
                            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "DashBoardVC")
                            appd.window?.rootViewController = mainVcIntial
                        }else{
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let obj_ISVC = storyboard.instantiateViewController(withIdentifier: "PreTreatMentNotRelatedTooth_1VC") as! PreTreatMentNotRelatedTooth_1VC
                            let navigationController = UINavigationController(rootViewController: obj_ISVC)
                            rootViewController(nav: navigationController)
                        }
                    }else{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let obj_ISVC = storyboard.instantiateViewController(withIdentifier: "PreTreatMentNotRelatedTooth_1VC") as! PreTreatMentNotRelatedTooth_1VC
                        let navigationController = UINavigationController(rootViewController: obj_ISVC)
                        rootViewController(nav: navigationController)
                    }
                }else{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let obj_ISVC : LoginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    let navigationController = UINavigationController(rootViewController: obj_ISVC)
                    rootViewController(nav: navigationController)
                }
            }
                
            else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let obj_ISVC : LoginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                rootViewController(nav: navigationController)
            }
        }
            
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let obj_ISVC : LoginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let navigationController = UINavigationController(rootViewController: obj_ISVC)
            rootViewController(nav: navigationController)
        }
    }
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "UTHC_APP")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    class func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
}

