//
//  WebService.swift
//  AlamoFire_WebService
//
//  Created by admin on 22/12/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit
import Alamofire


class WebService: NSObject,NSURLConnectionDelegate {
    class func callAPI(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        request(strUrlwithString!, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value
                    {
                        OnResultBlock(data as! NSDictionary,"Suceess")
                    }
                    break
                case .failure(_):
                    print(response.result.error!)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("Connection Time Out ", forKey: "message")
                    OnResultBlock(dic,"failure")
                    break
                }
            }
    }
    
    //MARK:
    //MARK: API UPLOAD IMAGE
    
    class func callAPIWithImageOnlyOne(parameter:NSDictionary,url:String,image:UIImage,imageName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        if isInternetAvailable() {
            upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
                multiPartFormData.append(image.jpegData(compressionQuality: 0.1)!, withName: "photo_path", fileName: "\(imageName)", mimeType: "image/jpeg")
                for (key, value) in parameter {
                    multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                }
                
            }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
                
                switch (encodingResult){
                    
                case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                    upload.responseJSON { response in
//                        print(response.request)  // original URL request
//                        print(response.response) // URL response
//                        print(response.data)     // server data
//                        print(response.result)   // result of response serialization
                        let statusCode = response.response?.statusCode
                        if statusCode == 200
                        {
                            let dic = NSMutableDictionary.init()
                            dic.setValue("\(response.result)", forKey: "message")
                            OnResultBlock(dic,"Suceess")
                        }else{
                            let dic = NSMutableDictionary.init()
                            dic.setValue("FAILURE", forKey: "message")
                            OnResultBlock(dic,"failure")
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    let dic = NSMutableDictionary.init()
                    dic.setValue("Connection Time Out", forKey: "message")
                    OnResultBlock(dic,"failure")
                    
                }
                
            }
            
        }else{
            
            let dic = NSMutableDictionary.init()
            dic.setValue(Localization("UTHC_NetworkError"), forKey: "message")
            dic.setValue("false", forKey: "status")
            OnResultBlock(dic,"failure")
        }
        
    }
    
    

    
    //MARK:
    //MARK: API Without Secureity Parameter
    class func callAPIBYGET(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
            request(url, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value
                    {
                       // print(data)
                        let dictdata = NSMutableDictionary()
                        dictdata.setValue(data, forKey: "data")
                        OnResultBlock((dictdata) ,"Suceess")
                    }
                    break
                case .failure(_):
                    print(response.result.error ?? 0)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("Connection Time Out ", forKey: "message")
                    OnResultBlock(dic,"failure")
                    break
                }
        }
    }
    class func callAPIBYPOST(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        request(url, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value
                    {
                        print(data)
                        let dictdata = NSMutableDictionary()
                        dictdata.setValue(data, forKey: "data")
                        OnResultBlock((dictdata) ,"Suceess")
                    }
                    break
                case .failure(_):
                    print(response.result.error ?? 0)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("Connection Time Out ", forKey: "message")
                    OnResultBlock(dic,"failure")
                    break
                }
            }
    }
    
    class func callAPIWithImage(parameter:NSDictionary,url:String,image:UIImage,imageName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
            upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
                if  let imageData = image.jpegData(compressionQuality: 0.5) {
                    multiPartFormData.append(imageData, withName: imageName, fileName: imageName, mimeType: "image/png")
                }
                for (key, value) in parameter {
                    multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                }
            }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
                switch (encodingResult){
                case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                    upload.responseJSON { response in
                        print(response.request ?? 0)  // original URL request
                        print(response.response ?? 0) // URL response
                        print(response.data ?? 0)     // server data
                        print(response.result)   // result of response serialization
                        
                        if let JSON = response.result.value
                        {
                            OnResultBlock(JSON as! NSDictionary,"Suceess")
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    let dic = NSMutableDictionary.init()
                    dic.setValue("Connection Time Out ", forKey: "message")
                    OnResultBlock(dic,"failure")
                }
            }
        }
    
    //MARK:
    //MARK: API With Secureity Parameter
    
    
    func callSecurityAPI(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {

            let finalParameter = self.addSecurity_Parameter(parameter: parameter as! NSMutableDictionary)
            request(url, method: .post, parameters: finalParameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value{
                        OnResultBlock(data as! NSDictionary,"Suceess")
                    }
                    break
                    
                case .failure(_):
                    print(response.result.error ?? 0)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("Connection Time Out ", forKey: "message")
                    OnResultBlock(dic,"failure")
                    break
                }
            }

    }
    
    func callAPIWithSecurity_Image(parameter:NSDictionary,url:String,image:UIImage,imageName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
      
            let finalParameter = self.addSecurity_Parameter(parameter: parameter as! NSMutableDictionary)
            
            upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
                
                if  let imageData = image.jpegData(compressionQuality: 0.5) {
                    let fileName = String(format: "%f.jpeg", NSDate.init().timeIntervalSince1970)
                    multiPartFormData.append(imageData, withName: imageName, fileName: fileName, mimeType: "image/jpeg")
                }
                
                for (key, value) in finalParameter {
                    multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                }
                
            }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
                
                switch (encodingResult){
                    
                case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                    upload.responseJSON { response in
                        
                        print(response.request ?? 0)  // original URL request
                        print(response.response ?? 0) // URL response
                        print(response.data ?? 0)     // server data
                        print(response.result)   // result of response serialization
                        
                        if let JSON = response.result.value
                        {
                            OnResultBlock(JSON as! NSDictionary,"Suceess")
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("Connection Time Out ", forKey: "message")
                    OnResultBlock(dic,"failure")
                }
        }
    }
    
    //MARK:
    //MARK: Get Device ID
    func get_DeviceID() -> String {
        let device = UIDevice.current
        let str_deviceID = device.identifierForVendor?.uuidString
        return str_deviceID!
    }
    
    //MARK:
    //MARK: Get Current Time
    func get_currentTime() -> String
    {
        let timeFormat = DateFormatter.init()
        timeFormat.dateFormat = "mmddHHmmss"
        return timeFormat.string(from: Date.init())
        
    }
    
    //MARK:
    //MARK: Get Current Time
    func addSecurity_Parameter(parameter:NSMutableDictionary) -> NSDictionary
    {
        let strTimeStamp = self.get_currentTime()
        let strDeviceType = "2"
        let str_Token = String.init(format: "%@%@%@%@", "masterkey","123456789",strDeviceType,strTimeStamp)
        let str_md5 = str_Token.md5
        
        parameter.setValue("123456789", forKey: "user_device_token")
        parameter.setValue(strDeviceType, forKey: "user_device_type")
        parameter.setValue(strTimeStamp, forKey: "timestamp")
        parameter.setValue(str_md5, forKey: "md5_key")
        
        return parameter
    }
    // MARK: - Convert String To Dictionary Function
    
    class func convertToDictionary(text: String) -> NSDictionary {
        do {
            
            let data  = try JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: .allowFragments) as? Dictionary<String, Any>
            // print(data)
            let firstElement: NSDictionary = data! as NSDictionary
            return firstElement
        } catch {
            print(error.localizedDescription)
        }
        return NSDictionary()
    }
    
    
    class func convertToArray(text: String) ->NSArray {
        do {
            let data  = try JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: .allowFragments) as? Array<Dictionary<String, Any>>
            //print(data)
            let firstElement: NSArray = data! as NSArray
            return firstElement
        }
        catch{
            print ("Handle error")
        }
        return NSArray()
    }
    
}

//MARK:
//MARK: MD5 Generate
extension String  {
    var md5: String! {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CC_LONG(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        
        CC_MD5(str!, strLen, result)
        
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        result.deinitialize(count: digestLen)
        
        return String(format: hash as String)
    }
}
