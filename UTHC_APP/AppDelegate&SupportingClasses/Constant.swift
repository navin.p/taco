//
//  Constant.swift
//  Petnod
//
//  Created by admin on 2/7/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import CoreTelephony
import MessageUI
import CoreData
import MapKit
import SystemConfiguration


//MARK:
var nsud = UserDefaults.standard
let appDelegate = UIApplication.shared.delegate as! AppDelegate
var mainStoryboard : UIStoryboard = UIStoryboard()


var toolbar_Button_Orange_color = "D75F00"
var toolbar_Gray_color = "F6F6F6"
var txt_Black_color = "282828"
var PrimeryTheamColor =  hexStringToUIColor(hex: "BD4701")
var strApp_platform = "iPhone"
var FrequencyTime = 90
var tagAPI = 0
var app_BuildVersion = "1.0.7"


//MARK:
//MARK: IPHONE SCREEN WIDTH AND HEIGHT BOUND CONDITION

let kIS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
let kIS_IPHONE = UIDevice.current.userInterfaceIdiom == .phone
let kIS_RETINA = UIScreen.main.scale >= 2.0
let kSYSTEM_VERSION = Float(UIDevice.current.systemVersion)
let kSCREEN_X = UIScreen.main.bounds.origin.x
let kSCREEN_Y = UIScreen.main.bounds.origin.y
let kSCREEN_WIDTH = UIScreen.main.bounds.size.width
let kSCREEN_HEIGHT = UIScreen.main.bounds.size.height

let kSCREEN_MAX_LENGTH = max(kSCREEN_WIDTH, kSCREEN_HEIGHT)
let kSCREEN_MIN_LENGTH = min(kSCREEN_WIDTH, kSCREEN_HEIGHT)

let kIS_IPHONE_4_OR_LESS = (kIS_IPHONE && kSCREEN_MAX_LENGTH < 568.0)
let kIS_IPHONE_5 = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 568.0)
let kIS_IPHONE_6_OR_7 = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 667.0)
let kIS_IPHONE_6P_OR_7P = (kIS_IPHONE && kSCREEN_MAX_LENGTH == 736.0)

////MARK:
//MARK:  Message
var kClientId = "155262836863-vv38ojgodr5sb3crh280bcrq1ahlp4ks.apps.googleusercontent.com"

////MARK:
//MARK:  WEB SERVICES URL

//--------Project

//var BaseURL :String = "http://192.168.0.22/UTHSC/webservices/webservices/"
//var BaseURL :String = "http://uttaco.stagingsoftware.com/webservices/webservices/"

// Product

//var BaseURL :String = "http://uttaco.stagingsoftware.com/uthsc_product/webservices/webservices/"
//var BaseURL :String = "http://192.168.0.191/uthsc_product/webservices/webservices/"
var BaseURL :String = "https://endotacoapp.uthscsa.edu/index.php/webservices/webservices/"
var API_Terms_Condition = "\(BaseURL)terms_conditions"
var API_AboutApp = "\(BaseURL)about_uthsc"
var API_BodyPartList = "\(BaseURL)body_parts_list"
var API_StudyID = "\(BaseURL)study_id_list"
var API_Registration = "\(BaseURL)registration"
var API_Login = "\(BaseURL)login"
var API_ForgetPassword = "\(BaseURL)forgot_password"
var API_AddDevice = "\(BaseURL)add_device"
var API_Pre_Survey_Question = "\(BaseURL)pre_survey_question_new"
var API_post_survey_question = "\(BaseURL)post_survey_question"
var API_Save_Pre_Analysis = "\(BaseURL)save_pre_analysis"
var API_Patient_contact_email = "\(BaseURL)patient_contact_email"
var API_get_change_password = "\(BaseURL)change_password"
var API_change_language_preference = "\(BaseURL)change_language_preference"
var API_Admin_location_list = "\(BaseURL)admin_location_list"
var API_get_survey_notification = "\(BaseURL)get_survey_notification"
var API_Admin_Information = "\(BaseURL)admin_information"
var API_Save_Post_Notification = "\(BaseURL)save_post_analysis"
var API_Logout = "\(BaseURL)logout"
var API_Insert_request_data = "\(BaseURL)insert_request_data"


//MARK:
//MARK: ScreenSize&DeviceType
struct ScreenSize{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width;
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height;
    static let SCREEN_MAX_LENGTH  = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
}

struct DeviceType{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 480.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6__6s_7_8          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P_7P_8P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH >= 1024.0
}

//MARK:
//MARK: OTHER FUNCTION
func makeButtonCorner(radius : CGFloat , color : UIColor , btn : UIButton ,borderWidth :CGFloat)  {
    btn.layer.cornerRadius = radius
    btn.layer.borderColor = color.cgColor
    btn.layer.borderWidth = borderWidth
}

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func jsontoString(fromobject:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: fromobject, options: []) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}

//MARK:
//MARK: Local Directory

func getImagefromDirectory(strname : String) -> UIImage{
    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
    let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
    if let dirPath          = paths.first
    {
        if strname == ""{
            return UIImage()
        }else{
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(strname)
            return  UIImage(contentsOfFile: imageURL.path)!
        }
    }
    return UIImage()
}

func saveImageOnDocument(image : UIImage , strname : String)  {
    let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    // create a name for your image
    let fileURL = documentsDirectoryURL.appendingPathComponent("\(strname)")
    if !FileManager.default.fileExists(atPath: fileURL.path) {
        do {
            try  image.jpegData(compressionQuality: 0.0)!.write(to: fileURL)
            print("Image Added Successfully = \(strname)" )
        } catch {
            print(error)
        }
    } else {
        print("Image Not Added")
    }
}
func removeImageFromDirectory(itemName:String) {
    let fileManager = FileManager.default
    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
    let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
    guard let dirPath = paths.first else {
        return
    }
    let filePath = "\(dirPath)/\(itemName)"
    print("Delete file Name : \(itemName)")
    do {
        try fileManager.removeItem(atPath: filePath)
    } catch let error as NSError {
        print(error.debugDescription)
    }
}
func estimatedHeightOfLabel(text: String , view : UIView) -> CGFloat {
    
    let size = CGSize(width: view.frame.width - 20 , height: 1000)
    
    let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
    
    let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)]
    
    let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
    
    return rectangleHeight
}
//MARK:
//MARK: Related to date formet

func dateTimeConvertor(str: String , formet : String) -> String {
    var fullNameArr = str.components(separatedBy: ".")
    let strFirst = fullNameArr[0] // First
    
    let dateFormatter = DateFormatter()
    let tempLocale = dateFormatter.locale
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    let date = dateFormatter.date(from: strFirst)!
    dateFormatter.dateFormat = "dd-MM-yyyy"
    dateFormatter.locale = tempLocale
    let dateString1 = dateFormatter.string(from: date)
    dateFormatter.dateFormat = "hh:mm a"
    dateFormatter.locale = tempLocale
    let dateString2 = dateFormatter.string(from: date)
    let finlString = "\(dateString1)  |  \(dateString2)"
    return finlString
    
}

func dateStringToFormatedDateString(dateToConvert: String, dateFormat: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let myDate = dateFormatter.date(from: dateToConvert)!
    dateFormatter.dateFormat = dateFormat
    let dateString = dateFormatter.string(from: myDate)
    return dateString
}

//MARK:
//MARK: Animation

func ShakeAnimation(textfiled: UITextField) {
    let shake = CAKeyframeAnimation(keyPath: "transform.translation.x")
    shake.duration = 0.1
    shake.repeatCount = 3
    shake.autoreverses = true
    shake.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
    textfiled.layer.add(shake, forKey: "shake")
}



func validateEmail(email: String) -> Bool{
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
}


//MARK:
//MARK : Show toast & loader

func showToastMessage(strMsg: String , style : UIBlurEffect.Style )  {
    FTIndicator.showToastMessage(strMsg)
    FTIndicator.setIndicatorStyle(style)
}

func showLoader(strMsg: String ,style : UIBlurEffect.Style)  {
    FTIndicator.setIndicatorStyle(.prominent)
    FTIndicator.showProgress(withMessage:strMsg, userInteractionEnable: false)
}

func dismissLoader()  {
    FTIndicator.dismissProgress()
}


//MARK:
//MARK:  Internet validation

func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
}



func calculateSizeFromString(message : String ,view : UIView) -> CGFloat{
    let myString: NSString = message as NSString
    let size: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18.0)])
    return size.height * 2
}

func showAlertWithoutAnyAction(strtitle : String , strMessage : String ,viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: strtitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
    
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: Localization("UTHC_Ok"), style: .cancel, handler: { (nil) in
         }))
    viewcontrol.present(alert, animated: true, completion: nil)
}

//MARK:
//MARK:  Calling & Message & OpenMap & Attribute string

func setAttributeText(lblText : UILabel) {
    let lblPocCell = NSAttributedString(string: lblText.text!,attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
    lblText.attributedText = lblPocCell
}

func checkNullValue(str : String) -> String! {
    var strValue = String()
    if str == "nil" || str == "<null>" {
        strValue = ""
    }else{
        strValue = "\(str)"
    }
    return strValue
    
}



func callingFunction(number : String){
    if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    else {
        showToastMessage(strMsg: "Your device doesn't support this feature.", style: .dark)
    }
}


func openMap(strTitle : String , strlat : String , strLong : String) {
    let latitude: CLLocationDegrees = Double(strlat)!
    let longitude: CLLocationDegrees = Double(strLong)!
    let regionDistance:CLLocationDistance = 10000
    let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
    let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
    let options = [
        MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
        MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
    ]
    let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
    let mapItem = MKMapItem(placemark: placemark)
    mapItem.name = strTitle
    mapItem.openInMaps(launchOptions: options)

}


//MARK:
//MARK:  CordDATA

func saveDataInLocalDictionary(strEntity: String , strKey : String , data : NSMutableDictionary)  {
    let context = AppDelegate.getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}



func saveDataInLocalArray(strEntity: String , strKey : String , data : NSMutableArray)  {
    let context = AppDelegate.getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}


func getDataFromLocalUsingPredicate(strEntity: String ,pedicate : NSPredicate)-> NSArray {
    
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
    fetchRequest.predicate = pedicate
    do {
        return (try AppDelegate.getContext().fetch(fetchRequest) as NSArray)
        
    } catch
    {
        let fetchError = error as NSError
        print(fetchError)
    }
    return NSArray()
}

func getDataFromLocal(strEntity: String , strkey : String )-> NSArray {
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")

    do {
        return (try AppDelegate.getContext().fetch(fetchRequest) as NSArray)
        
    } catch
    {
        let fetchError = error as NSError
        print(fetchError)
    }
    return NSArray()
}

func getDataFromLocalUsingAntity(entityName : String) -> NSMutableArray {
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
    do {
        //go get the results
        return  (try AppDelegate.getContext().fetch(fetchRequest) as NSArray).mutableCopy() as! NSMutableArray
        
    } catch {
        print("Error with request: \(error)")
    }
    return NSMutableArray()
}



func deleteAllRecords(strEntity: String ) {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let context = delegate.persistentContainer.viewContext
    
    let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\(strEntity)")
    let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
    
    do {
        try context.execute(deleteRequest)
        try context.save()
        
    } catch {
        print ("There was an error")
    }
}
func OpenActionSheet ( strMessage : String , strTitle : String, vc : UIViewController){
    let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: strTitle, message: strMessage, preferredStyle: .alert)
    let cancelActionButton = UIAlertAction(title: Localization("UTHC_Ok"), style: .cancel) { _ in
        print("Cancel")
    }
    actionSheetControllerIOS8.addAction(cancelActionButton)
    vc.present(actionSheetControllerIOS8, animated: true, completion: nil)
}


func txtFiledValidation(textField : UITextField , string : String , returnOnly : String , limitValue : Int) -> Bool {
    
    let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
    let stringFromTextField = NSCharacterSet.init(charactersIn: string)
    let strValid = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    
    if returnOnly == "NUMBER" {
        if strValid == false{
            return false
        }
    }
    
    if returnOnly == "CHAR" {
        if strValid == true{
            return false
        }
    }
    
    
    
    
    
    if ((textField.text?.count)! > 0) {
        
        
        if ((textField.text?.count)! > limitValue) {
            if (string == "")
            {
                return true;
            }
            return false;
        }
        return true;
    }
    if (string == " ") {
        return false;
    }
    return true;
}
//MARK: UIDevice Model Name

public enum Model : String {
    case simulator     = "simulator/sandbox",
    //iPod
    iPod1              = "iPod 1",
    iPod2              = "iPod 2",
    iPod3              = "iPod 3",
    iPod4              = "iPod 4",
    iPod5              = "iPod 5",
    //iPad
    iPad2              = "iPad 2",
    iPad3              = "iPad 3",
    iPad4              = "iPad 4",
    iPadAir            = "iPad Air ",
    iPadAir2           = "iPad Air 2",
    iPad5              = "iPad 5", //aka iPad 2017
    iPad6              = "iPad 6", //aka iPad 2018
    //iPad mini
    iPadMini           = "iPad Mini",
    iPadMini2          = "iPad Mini 2",
    iPadMini3          = "iPad Mini 3",
    iPadMini4          = "iPad Mini 4",
    //iPad pro
    iPadPro9_7         = "iPad Pro 9.7\"",
    iPadPro10_5        = "iPad Pro 10.5\"",
    iPadPro12_9        = "iPad Pro 12.9\"",
    iPadPro2_12_9      = "iPad Pro 2 12.9\"",
    //iPhone
    iPhone4            = "iPhone 4",
    iPhone4S           = "iPhone 4S",
    iPhone5            = "iPhone 5",
    iPhone5S           = "iPhone 5S",
    iPhone5C           = "iPhone 5C",
    iPhone6            = "iPhone 6",
    iPhone6plus        = "iPhone 6 Plus",
    iPhone6S           = "iPhone 6S",
    iPhone6Splus       = "iPhone 6S Plus",
    iPhoneSE           = "iPhone SE",
    iPhone7            = "iPhone 7",
    iPhone7plus        = "iPhone 7 Plus",
    iPhone8            = "iPhone 8",
    iPhone8plus        = "iPhone 8 Plus",
    iPhoneX            = "iPhone X",
    iPhoneXS           = "iPhone XS",
    iPhoneXSMax        = "iPhone XS Max",
    iPhoneXR           = "iPhone XR",
    //Apple TV
    AppleTV            = "Apple TV",
    AppleTV_4K         = "Apple TV 4K",
    unrecognized       = "?unrecognized?"
}

// #-#-#-#-#-#-#-#-#-#-#-#-#-#-#
//MARK: UIDevice extensions
// #-#-#-#-#-#-#-#-#-#-#-#-#-#-#

public extension UIDevice {
    public var type: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafePointer(to: &systemInfo.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String.init(validatingUTF8: ptr)
                
            }
        }
        var modelMap : [ String : Model ] = [
            "i386"      : .simulator,
            "x86_64"    : .simulator,
            //iPod
            "iPod1,1"   : .iPod1,
            "iPod2,1"   : .iPod2,
            "iPod3,1"   : .iPod3,
            "iPod4,1"   : .iPod4,
            "iPod5,1"   : .iPod5,
            //iPad
            "iPad2,1"   : .iPad2,
            "iPad2,2"   : .iPad2,
            "iPad2,3"   : .iPad2,
            "iPad2,4"   : .iPad2,
            "iPad3,1"   : .iPad3,
            "iPad3,2"   : .iPad3,
            "iPad3,3"   : .iPad3,
            "iPad3,4"   : .iPad4,
            "iPad3,5"   : .iPad4,
            "iPad3,6"   : .iPad4,
            "iPad4,1"   : .iPadAir,
            "iPad4,2"   : .iPadAir,
            "iPad4,3"   : .iPadAir,
            "iPad5,3"   : .iPadAir2,
            "iPad5,4"   : .iPadAir2,
            "iPad6,11"  : .iPad5, //aka iPad 2017
            "iPad6,12"  : .iPad5,
            "iPad7,5"   : .iPad6, //aka iPad 2018
            "iPad7,6"   : .iPad6,
            //iPad mini
            "iPad2,5"   : .iPadMini,
            "iPad2,6"   : .iPadMini,
            "iPad2,7"   : .iPadMini,
            "iPad4,4"   : .iPadMini2,
            "iPad4,5"   : .iPadMini2,
            "iPad4,6"   : .iPadMini2,
            "iPad4,7"   : .iPadMini3,
            "iPad4,8"   : .iPadMini3,
            "iPad4,9"   : .iPadMini3,
            "iPad5,1"   : .iPadMini4,
            "iPad5,2"   : .iPadMini4,
            //iPad pro
            "iPad6,3"   : .iPadPro9_7,
            "iPad6,4"   : .iPadPro9_7,
            "iPad7,3"   : .iPadPro10_5,
            "iPad7,4"   : .iPadPro10_5,
            "iPad6,7"   : .iPadPro12_9,
            "iPad6,8"   : .iPadPro12_9,
            "iPad7,1"   : .iPadPro2_12_9,
            "iPad7,2"   : .iPadPro2_12_9,
            //iPhone
            "iPhone3,1" : .iPhone4,
            "iPhone3,2" : .iPhone4,
            "iPhone3,3" : .iPhone4,
            "iPhone4,1" : .iPhone4S,
            "iPhone5,1" : .iPhone5,
            "iPhone5,2" : .iPhone5,
            "iPhone5,3" : .iPhone5C,
            "iPhone5,4" : .iPhone5C,
            "iPhone6,1" : .iPhone5S,
            "iPhone6,2" : .iPhone5S,
            "iPhone7,1" : .iPhone6plus,
            "iPhone7,2" : .iPhone6,
            "iPhone8,1" : .iPhone6S,
            "iPhone8,2" : .iPhone6Splus,
            "iPhone8,4" : .iPhoneSE,
            "iPhone9,1" : .iPhone7,
            "iPhone9,3" : .iPhone7,
            "iPhone9,2" : .iPhone7plus,
            "iPhone9,4" : .iPhone7plus,
            "iPhone10,1" : .iPhone8,
            "iPhone10,4" : .iPhone8,
            "iPhone10,2" : .iPhone8plus,
            "iPhone10,5" : .iPhone8plus,
            "iPhone10,3" : .iPhoneX,
            "iPhone10,6" : .iPhoneX,
            "iPhone11,2" : .iPhoneXS,
            "iPhone11,4" : .iPhoneXSMax,
            "iPhone11,6" : .iPhoneXSMax,
            "iPhone11,8" : .iPhoneXR,
            //AppleTV
            "AppleTV5,3" : .AppleTV,
            "AppleTV6,2" : .AppleTV_4K
        ]
        
        if let model = modelMap[String.init(validatingUTF8: modelCode!)!] {
            if model == .simulator {
                if let simModelCode = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
                    if let simModel = modelMap[String.init(validatingUTF8: simModelCode)!] {
                        return simModel
                    }
                }
            }
            return model
        }
        return Model.unrecognized
    }
}
