//
//  ViewController.swift
//  WireFraming
//
//  Created by admin on 26/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    var dicoLocalisation = NSDictionary()
    var strTag : Int?
    
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var lblVersion: UILabel!

    var aryMenuItems = NSMutableArray()
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(userLoggedIn), name: Notification.Name("UserLoggedIn"), object: nil)

        tv.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        lblVersion.text = "Version \(app_BuildVersion)"
        aryMenuItems = NSMutableArray()

        aryMenuItems.add(Localization("Drawer_Home"))
        aryMenuItems.add(Localization("Drawer_ChangeLanguage"))
        aryMenuItems.add(Localization("Drawer_ChangePassword"))
        aryMenuItems.add(Localization("Drawer_Aboutus"))
        aryMenuItems.add(Localization("Drawer_Logout"))

        tv.delegate = self
        tv.dataSource = self
        tv.tableFooterView = UIView()
        
        self.view.backgroundColor =  hexStringToUIColor(hex: "BD4701")
        self.tv.backgroundColor = hexStringToUIColor(hex: "BD4701")
        tv.reloadData()

    }
    
    @objc func userLoggedIn()  {
        aryMenuItems = NSMutableArray()
        aryMenuItems.add(Localization("Drawer_Home"))
        aryMenuItems.add(Localization("Drawer_ChangeLanguage"))
        aryMenuItems.add(Localization("Drawer_ChangePassword"))
        aryMenuItems.add(Localization("Drawer_Aboutus"))
        aryMenuItems.add(Localization("Drawer_Logout"))
        
        
        tv.delegate = self
        tv.dataSource = self
        tv.tableFooterView = UIView()
        self.view.backgroundColor =  hexStringToUIColor(hex: "BD4701")
        tv.reloadData()

        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "UserLoggedIn"), object: nil)

    }
    
    //MARK:- LogOUT API
    //MARK:-
    func API_CallLogout()  {
        let dict  = NSMutableDictionary()
     
        let dictLogInData = nsud.value(forKey: "LogInData")as! NSDictionary
        var strPtientID = "\(String(describing: dictLogInData.value(forKey: "patient_id")!))"
        let fullNameArr = strPtientID.components(separatedBy: "-")
        strPtientID = fullNameArr[2]
        dict.setValue(strPtientID, forKey: "patient_id")
        dict.setValue("\(dictLogInData.value(forKey: "location_id")!)", forKey: "location_id")//4
        
        showLoader(strMsg: "", style: .dark)
        WebService.callAPIBYPOST(parameter: dict, url: API_Logout) { (responce, status) in
            DispatchQueue.main.async {
                dismissLoader()
            }
            if(status == "Suceess"){
                let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()

                let dictResponce = responce.value(forKey: "data")as! NSDictionary
                if("\(String(describing: dictResponce.value(forKey: "error")!))" == "1"){
                    let p_ID = "\(String(describing: nsud.value(forKey: "patient_id")!))"
                    UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                    nsud.setValue(p_ID, forKey: "patient_id")
                    if SetLanguage(arrayLanguages[1]) {
                        nsud.setValue("English", forKey: "AppLanguage")
                    }
                    nsud.setValue("English", forKey: "AppLanguage")
                    let mainVcIntial = kConstantObj.SetMainViewController(aStoryBoardID: "LoginVC")
                    appDelegate.window?.rootViewController = mainVcIntial
                }
                else if("\(String(describing: dictResponce.value(forKey: "error")!))" == "0"){
                    showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: dictResponce.value(forKey: "data")as! String, viewcontrol: self)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: Localization("UTHC_Alert"), strMessage: Localization("UTHC_OtherError"), viewcontrol: self)
            }
        }
    }
    
    
    
    //MARK:- UITableView Delegate Methods
    //MARK:-
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        }else{
            return aryMenuItems.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section != 0 {
            let cell = tv.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath as IndexPath) as! DrawerCellTableViewCell
            cell.lbltitle.text = aryMenuItems[indexPath.row]as? String
            return cell
        }else{
            let cell = tv.dequeueReusableCell(withIdentifier: "MenuCellStatic", for: indexPath as IndexPath) as! DrawerCellTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 90
        }else{
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {

        if indexPath.row == 0 {
            let mainVcIntial = kConstantObj.SetMainViewController(aStoryBoardID: "DashBoardVC")
            appDelegate.window?.rootViewController = mainVcIntial
        }
        else  if indexPath.row == 1 {
        let mainVcIntial = kConstantObj.SetMainViewController(aStoryBoardID: "LanguageSelectionVC")
        appDelegate.window?.rootViewController = mainVcIntial
        }
        else  if indexPath.row == 2 {
            
            let mainVcIntial = kConstantObj.SetMainViewController(aStoryBoardID: "ForgetPasswordVC")
            appDelegate.window?.rootViewController = mainVcIntial
            
        }
       else  if indexPath.row == 3 {
            
            let mainVcIntial = kConstantObj.SetMainViewController(aStoryBoardID: "TermsAndConditionVC")
            appDelegate.window?.rootViewController = mainVcIntial
            
        }else if(indexPath.row == 4){
          
            let alert = UIAlertController(title: Localization("UTHC_Alert"), message:  Localization("Drawer_Logout1"), preferredStyle: UIAlertController.Style.alert)
            // add the actions (buttons)
            alert.addAction(UIAlertAction (title: Localization("Drawer_No"), style: .default, handler: { (nil) in
            }))
            alert.addAction(UIAlertAction (title: Localization("Drawer_Yes"), style: .default, handler: { (nil) in
              
                self.API_CallLogout()
                
                
             
            }))
            self.present(alert, animated: true, completion: nil)
        }
   }
}

